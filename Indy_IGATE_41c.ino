//********************************Last Update 23.30   24/11/19   HS3LSE******************//
//********************************Last Update 02.45   9/03/19   HS6TUX******************//
// Test1
//*** CODE    > HS3LSE , HS6TUX
//*** Symbol  > E27ASY
//*** Test&MOD    > HS3NOQ HS3UZM HS2QJJ HS6TUX
//***KISS
//***TNC2
//***password WIFI 19 digit
//***Change baud rate.
//***1.5.4 change delay client print beacon-comment 500=>2000
//***1.5.4 show version in command line
//***1.5.5 fix bug KISS skip after 0x03 is worng data
//*** (for arduino TNC arduino_tnc_014_w_tx KI4CW)
//*** 1.6.1 fix bug KISS send to APRSIS [add println("")]
//*** 1.6.1 fix bug Check ':' before Send TNC2 to IS
//*** 1.6.2 ?DISP display all config
//*** 1.6.2 SendKISS INET TO RF 20/06/16
//*** 1.6.2 Reject TCP qA RFONLY NOGATE LOOP To IS
//*** 1.6.2 Add TLM Rx Tx Drop WiFi
//*** 1.6.2 Auto Send out KISS or TNC2 check from Rx PKT
//*** 1.7.0 Add more buffer for JAVA Filter EEPROM
//*** 1.7.0 Renew solution for decode KISS
//*** 1.7.0 Renew for printf fuction all make fast up
//*** 1.7.0 Renew solution for decode SSID with &0x7F before >>1
//*** 1.7.0 get Repearted SSID only Path by &0x80F for check first bit for "1" as Repeated
//*** 1.7.1 Change pattern The WifiStart and APRSIS_connect
//*** 1.7.1 Change pattern decode KISS
//*** 1.7.1 Add Path for IGATE Tx to RF*****
//*** 1.7.1 Add Digipeater Function*****
//*** 1.7.2 Add LED online
//*** 1.7.2 Add Reset pin for Reset TNC
//*** 1.8.0 Add WiFi Config
//*** 1.8.0 Call sign SSID
//*** 1.8.0 APRSDroid avaliable connect PKT Send to APRS-IS (use PATH WIDE1-1 if want send to RF [use sendDIGI code])
//*** 1.8.0 add KISS CMD Txdelay
//*** 1.8.1 Config by WEB Browser
//*** 1.8.1 Add Reset pin for Reset TNC // 420sec
//*** 1.8.2 Update Firmware via web browser ESP-12E/F
//*** 2.0 Change Pin LED Status
//*** 2.0 Cancel RST to TNC
//*** 2.0 add WX for DHT22 / BME280 / LCD 20x4 I2C8574 scan Address
//*** 2.0 chage EQNS wifi -db +100
//*** 2.0 add Temp/LDR in web browser
//*** 2.0 DHT22-GPIO12 / LDR-GPIO14 > ADC / Rain-GPIO16 > ADC
//*** 2.0 Swap symbol by LDR Value <4 (or not connectLDR) = Wx Symbol
//*** 2.0 Swap symbol by LDR Value <500  = /) Cloudy Symbol
//*** 2.0 Swap symbol by LDR Value >500  = /U Sunny Symbol
//*** 2.0 renew code for fast browser config
//*** 2.0 add DHCP if set local IP = 0
//*** 2.0 กำลัง เขียน passcode**ยังไม่เสร็จ
//*** For OTA Open Broswer to 192.168.4.1:81/update or local_ip:81/update
//*** 2.1 OLED 1.3 add 0X3C SH1106
//*** 2.1 WiFi ON/OFF For Digi
//*** 3.0c add restart button
//*** 3.0d fix DIGI Destination error (add var DIGIKISS)
//*** 3.0e fix DIGI not work from IS>RF
//*** 3.0e fix Destination APESPG3-0
//*** 3.0e renew RAW Display
//*** 3.0e renew ack MSG
//*** 3.0f Remote Config via RF & Net
//*** 3.0f add T= H= P= on the RF Beacon
//*** 3.0g fix display raw not in case
//*** 3.0h fix digi delay 1 is not delay
//*** 3.1a add page send beacon for test
//*** 3.1b Auto Restart on time to set
//*** 3.1c fix bug SSID with Call sign wrong get into EEPROM with code 10 & 13
//*** 3.1d add technical command
//*** 3.1d fix dup show Display from the NOGATE via IGATE
//*** 3.1d add Page MSG , renew webconfig
//*** 3.1d add beacon for Tx test mode
//*** 3.1e bug fix eeprom 505-506
//*** 3.1e Support 2 Color OLED
//*** 3.1e add temp hum pressure Icon in Wx Page
//*** 3.1e add Symbol S_tower S-istar
//*** 3.1e LED Satus ON Digo Mode (WiFi OFF)
//*** 3.1e Block Rev in My IGate to sent to RF
//*** 3.1e Add Dx in Status
//*** 3.1e Add MSG Symbol
//*** 3.1e Add Display Object and ITEM
//*** 3.1e maxlength textbox setup page
//*** 3.1e Set AP Access point OFF in IGATE Mode
//*** 3.1e Show Status page Last 5 Packet From RF
//*** 4.0 toUpperCase() for MyCallsign Config , PATH
//*** 4.0 Upgrade ESP8266 community board to 2.4.2
//*** 4.0 recode all config to CMD()
//*** 4.0 use ESP8266WebServer core and update at port 80
//*** 4.0 add ITEM
//*** 4.0 LOGO APRS at home page
//*** 4.0 Send Rawpacket in message Page
//*** 4.0 Only one SAVE button for config page
//*** 4.0 Gen passcode
//*** 4.0 Add UP time in Status
//*** 4.0 Line Notification MSG , Rx SAT , Digi from SAT , IGATE online
//*** 4.0 Attack Beacon when rcv Packet or Digi from YB0X , ARISS ,RS0ISS ,W3ADO ,PCSAT ,PSAT,PSAT2
//*** 4.0 fix worng telemetry RSSI
//*** 4.0 fix worng Temp>38
//*** 4.0 fix worng SSID -15
//*** 4.0 filter direct ARISS path eject
//*** 4.0 Library NTP => https://github.com/stelgenhof/NTPClient  / zip => https://github.com/stelgenhof/NTPClient/archive/master.zip
//*** 4.0 Add WiFi.disconnect() in Web config func when WiFi not connect
//*** ADC for 220k / 47k Voltage
//*** 4.0j สามารถ ปิดการทำงาน IGATE ได้โดยที่ WiFi ยังเชื่อมต่อกับ AP ภายในบ้านอยู่ได้ กรณี เจอ AP ที่ตั้งค่าไว้แล้ว
//*** 4.0j แก้ไขตัวแปรที่ อ่านค่าทศนิยม ให้เป็นจำนวนเต็มที่แม่นยำถูกต้องมาขึ้น
//*** 4.0j เพิ่มเมนู ปิด/เปิด AP / IGATE ให้สามารถ ปิด-เปิด ได้ในขณะกำลังทำงาน
//*** 4.0j เพิ่มการส่งค่า มาตรฐาน Wx ที่ต้องบอก วันเวลาUTC ลงใน PKT / แต่การแสดงผลเวลาที่หน้าจอยังเป็น Loc Time
//*** 4.0k เพิ่มการตั้งค่า default อัตโนมัติ เมื่อลง FW ครั้งแรก
//*** 4.1a มี Factory reset เพื่อตั้งค่าในeeprom กรณีลง FW ครั้งแรก หรือต้องการกลับสู่ค่าโรงงาน
//*** 4.1b จัดการแสดงผลOLED หน้า WiFiStart / APRSIS Connect
//*** 4.1b ปรับการสั่ง Line notify ใหม่โดย  สั่ง notify เมื่อ AP เป็น OFF เท่านั้น
//*** 4.1b ย้ายการส่ง Line online ไปที่ เงื่อนไข TLM ==1
//*** 4.1b เพิ่มการรับ PATH USNAP1-1 ดาวเทียม BRICSAT
//*** 4.1c เปลี่ยน Format Wx กรณีไม่ต่อเซนเซอร์แสง ส่ง c...s...g... เป็น 000/000g000

#include "buzzer.h"
#include <SoftwareSerial.h>
#include <ESP8266WiFi.h>
#include "EEPROM.h"
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServer.h>
//********************Config Here*****************//
//"n3L5Z9g3zzk09VnDGHmiQoMwG8mnFH4XDHzQZi2SJ43" //"XkxbukEjmVQfWsfqcN5DByVT5Od6ufGYGNU7QqMAI32" //
//#define Developer                                   //  ห้ามเปิด ใช้สำหรับ นักพัฒนา เท่านั้น
#define LINE_Notify
#define LINE_MSG_RCV
#define LINE_PKT_SAT_RCV
#define LINE_SAT_RCV
#define LINE_DX_NEW

#define MODEL_INDY // or // Standard ESP Pin ใส่คอมเม้น เมื่อต้องการใช้กับบอร์ด ที่มีจอOLEDติดมาด้วย ใช้ขา I2C มาตรฐาน D1D2
#define OLED
#define inch13 // 1.3 or 0.96 ใส่คอมเม้นเมื่อใช้กับจอ0.96นิ้ว
//#define Two_color                             //  เอาคอมเม้นออกเมื่อใช้กับจอ 2สี
#define IGATE_Ver "ESP8266GATE 4.1c"
#define FW_V "4.1c"
#define FW_dest "AESPG41"
#define FW_destNET "AESPG4"
#define BME
#define D_H_T
#define LDR
#define Rain_s
#define ANS_MSG
#define TLM
#define AP_ON //เมื่อต้องการเปิดโหมด Acess point ตลอดเวลา
#define HOME_LOGO
//#define SHOW_SCAN
#define SHOW_SSID_BEACON
#define NTP_Serv
#define To_RF
#define To_BeaconINET
#define FW_THAILAND //สำหรับ ใช้ในไทย จะใช้งานฟังชั่น UTC+7 / Filter g/HS*/E2*
//***********************************************//
#define OLED_ADD 0x3c
#define BME_address1 0x76
#define BME_address2 0x77
//***********************************************//

//********ชุดนี้ไม่ต้องแก้ไข ให้เลือกการทำงานที่ Config Here ข้างบน**************//
#ifdef MODEL_INDY //กรณีอินดี้ ต่อจอ ที่ขา GPIO 12-13 และ TNC GPIO 5-4
#define Tx_Serial 5
#define Rx_Serial 4
#define SCA 13
#define SCL 12
#else // Standard ESP Pin     //กรณีบอร์ดมาตรฐาน ต่อจอ ที่ขา GPIO 5-4 หรือ D1-D2 และ TNC GPIO 13-12
#define Tx_Serial 13
#define Rx_Serial 12
#define SCA 5
#define SCL 4
#endif

#ifdef BME or OLED
#include <Wire.h>
#endif
#ifdef OLED //กำหนดค่าเลือกใช้จอ 1.3 และ 0.96
#include "symbol.h"

#ifdef inch13
#include "SH1106.h" // 1.3
SH1106 display(OLED_ADD, SCA, SCL);
#else
#include "SSD1306.h" //0.96
SSD1306 display(OLED_ADD, SCA, SCL);
#endif

#endif
//***********************************************************************//

//*********PIN Hardware******//
#define Buzzer 16
#define Tx_Now 0 // Switch Send Beacon

//*********Screen Area*******//
#define SWAP_delay 30 //ระยะเวลา วินาที การสลับหน้าจอ เวลาแสดงข้อมูล PKT สลับไปหน้าจอ แสดงวันเวลาปัจจุบัน
#ifdef Two_color
#define X_hor 0    //จุดลากเส้นแนวนอน จากX0 ซ้ายสุด แบ่งข้อมูลในหน้าจอ OLED
#define Y_hor 25   //จุดลากเส้นแนวนอน จาก จากบรรทัดที่ 17 แบ่งข้อมูลในหน้าจอ OLED //HS6TUX**************//
#define X_ver 35   //จุดลากเส้นแนวตั้ง จากขอบซ้าย มา 38 Px แบ่งข้อมูลในหน้าจอ OLED //HS6TUX**************//
#define Y_ver 25   //จุดลากเส้นแนวตั้ง จากขอบบน 17 Px แบ่งข้อมูลในหน้าจอ OLED //HS6TUX**************//
#define S_width 25 //ขนาดกว้างของรูปสัญลักษณ์
#define S_hight 25 //ขนาดสูงของรูปสัญลักษณ์
#define First_Line 10
#define X_symbol 103 //ตำแหน่งของรูปสัญลักษณ์ กรณีใช้จอ 2สี  ย้ายไปแสดง ในช่อง ข้อมูล //HS6TUX**************//
#define Y_symbol 16  //ตำแหน่งของรูปสัญลักษณ์ กรณีใช้จอ 2สี  ย้ายไปแสดง ในช่อง ข้อมูล //HS6TUX_23092561-1019//
#else
#define X_hor 0    //จุดลากเส้นแนวนอน จากX0 ซ้ายสุด แบ่งข้อมูลในหน้าจอ OLED
#define Y_hor 26   //จุดลากเส้นแนวนอน จาก จากบรรทัดที่ 26 แบ่งข้อมูลในหน้าจอ OLED
#define X_ver 38   //จุดลากเส้นแนวตั้ง จากขอบซ้าย มา 38 Px แบ่งข้อมูลในหน้าจอ OLED
#define Y_ver 26   //จุดลากเส้นแนวตั้ง จากขอบบน 26 Px แบ่งข้อมูลในหน้าจอ OLED
#define S_width 25 //ขนาดกว้างของรูปสัญลักษณ์
#define S_hight 25 //ขนาดสูงของรูปสัญลักษณ์
#define First_Line 10
#define X_symbol 100 //ตำแหน่งของรูปสัญลักษณ์ กรณีใช้จอ 1 สี  ตำแหน่งมุมบนขวาสุดของจอ
#define Y_symbol 0   //ตำแหน่งของรูปสัญลักษณ์ กรณีใช้จอ 1 สี  ตำแหน่งมุมบนขวาสุดของจอ
#endif
//*****************************//

#ifdef BME
#include "BlueDot_BME280.h"
BlueDot_BME280 bme280 = BlueDot_BME280();
#endif

#ifdef D_H_T
#include "DHT.h"
#endif

#ifdef NTP_Serv
#include "NTPClient.h"
#endif

#define MAX_SRV_CLIENTS 1
const char *ssid_s = "IndyIGATE_Config";
const char *password_s = "123456789";
ESP8266WebServer httpServer(80);
ESP8266HTTPUpdateServer httpUpdater;
WiFiServer server(23);
WiFiClient serverClients[MAX_SRV_CLIENTS];
WiFiClient client;
//int status = WL_IDLE_STATUS;                     // the Wifi radio's status
#ifdef D_H_T
//----------DHT Config-------------
#define DHTPIN 10     // what pin we're connected to
#define DHTTYPE DHT22 // DHT 22  (AM2302)
DHT dht(DHTPIN, DHTTYPE);
//----------Wx Config--------------
#endif
#ifdef LDR
const int sensorPin = A0;
#endif
#define change_symbol_LDR 500
int LDR_value = 0;
const int Rain_Pin = 14;
//---------------------------------

//**************ข้างล่างนี้ไม่ต้องแก้ไข เพราะไม่มีผล เนื่องจาก การตั้งค่าจะอยู่ใน EEPROM ตรงนี้เป็นแค่ค่าเริ่มต้นเฉยๆ*********/
char APRSIS[30] = "aprsth.nakhonthai.net";
//SoftwareSerial TNC_Serial = SoftwareSerial(Rx_Serial,Tx_Serial,false, 256);
SoftwareSerial TNC_Serial(Rx_Serial, Tx_Serial, false, 256);
IPAddress ip(192, 168, 4, 1);
IPAddress netmask(255, 255, 255, 0);
IPAddress APRSIS_IP = {192, 168, 1, 6};
unsigned long timeref1 = 0, timedigi = 0, timeIS = 0, timerecount = 0;
unsigned long Main_Time_Sec = 0, Sub_Time_Sec = 0, Run_Time_Sec = 0;
boolean Beacon_Beacon = true, Beacon_Wx = false, Beacon_Status = false, Beacon_Telemetry = false;
char MyCallSign[11] = "NOCALL";
char MyRawCall[20] = "";
String ITEM = "";
String BUFF = "";
String BUF_via = "";
String NTP_DATETIME = "";
String NTP_DATETIME_sec = "";
String NTP_MDHM = ""; //สำหรับ WX
String NTP_TIME = ""; //17.45   8/03/19   HS6TUX
String Time_RX = "";  //17.45   8/03/19   HS6TUX
String LINE_TOKEN = "";
char MYDEST[8] = FW_dest;
char is_passcode[6] = "00000";
char JavaFilter[60] = "m/60";
char ssid[20] = "MYWIFI";
char password[20] = "1234";
char Beacon[21] = "!1455.03N/10329.84E&";
char comment[30] = "ESP8266 IGATE";
char VIAPATH[20] = "WIDE1-1";
char Answer[15] = "MYCALL";
unsigned int DigiDelay = 0;
byte local_ip[] = {192, 168, 1, 200};
byte gateway[] = {192, 168, 1, 1};
byte dnsip[] = {8, 8, 8, 8};
byte subnet[] = {255, 255, 255, 0};
unsigned int interval = 20;
long Brate = 9600;
unsigned int IS_port = 14580;
unsigned int wifi_loss = 0, Inet_loss = 0;
unsigned int Page = 0;
unsigned const int frq = 1000; //Specified in Hz
unsigned const int timeOn = 50;
int Loop_connect_IS = 0;
unsigned int Read_ResetOnTime = 0;
unsigned int My_MSG = 0;
const unsigned int buffsize = 250;
char RawSerial[buffsize] = "";
char RawServer[buffsize] = "";
char RawCommand[buffsize] = "";
char CRate[10];
byte KISS[buffsize];
byte DIGIKISS[17];
String K_SSID = "", BUF_display = "";
int Old_ack = 0;
unsigned int Read_PWR = 0;
unsigned int State_IGATE = 1; //สถานะ IGATE
boolean CHK_CONFIGKISS = true;
unsigned int StartK = 0;
int indexcon = 0, indexRK = 0;
unsigned int indexSerial = 0, indexServer = 0, indexCommand = 0, Tx_cnt = 0, Rx_cnt = 0, Dg_cnt = 0;
boolean CMD_debug = false, CMD_config = false, CHK_Repair = false, Last_buttom = false, CMD_REMOTE = false;
boolean B_B = false, B_W = false, B_S = false;
boolean Config_Interrupt = false;
boolean Target_wifi_Start = false; //เชคตอนเปิดเครื่องว่ามีสัญญาณ SSID WiFi ที่ต้องการหรือไม่
char const thirdparty = 0x7d;
unsigned int indexkiss = 0;
unsigned int STARTKISS = 0;
unsigned int counter = 0;
unsigned int Tcount = 0, TLM_T = 0, TLM_R = 0, TLM_D = 0;
//--------Wx---------------//
float Show_t = 0.0, Show_h = 0.0, Show_p = 0.0;

//-------------------------//
boolean KISSOK = false, KISS_Frame = false, CHK_KISS = false, REPEATED = false, CHK_UI = false, CHK_UI_INFO = false, TxMODEKISS = false;
boolean Real_PKT1 = false, Real_PKT2 = false, CHK_colon = false, DIGI_ON = false, Inet = false, DigiP = false;
boolean CHK_Upfirmware = false;
boolean CHK_NTP = false, CHK_MSG = false;
unsigned int C_UI = 0, C_UI_I, C_S = 0, CT = 0;
const int LED_online = 2;
boolean PWR_WiFi = false;
String incomeMSG[5] = "";
String Dx_Call = "";
String Dx_Callx = "";  //17.45   8/03/19   HS6TUX
String Last_Call = ""; //17.45   8/03/19   HS6TUX
String Last_Packet[5] = "";
int num_last_p = 0, num_last_msg = 0;
unsigned int Dx_km = 0, Dx_br = 0, Last_KM = 0;
unsigned int Run_Time_SWAP_A = 0, Run_Time_SWAP_B = 0;
boolean IGATE_ON = false, FWD_RF = true;
byte Ad_BME;
byte Ad_OLED;
String message = "";
const String Label_config[] = {
    "",              //0
    "a_IP Address ", //1
    "b_Subnet ",
    "c_Gateway ",
    "d_DNS ",
    "e_SSID ",
    "f_Password wifi ",
    "g_APRS-IS ",
    "h_Server port ",
    "i_MyCall ",
    "j_Passcode ",
    "k_Filter ",
    "l_Location-Symbol ",
    "m_Comment ",
    "n_Interval ",
    "o_TNCBaudrate ",
    "q_DIGI delay ", //16
    "p_IGATE Via ",  //17
    "r_Restart every ",
    "t_Line_Token "};
//const int EEprom_Add[31]= {0,1,21,41,61,81,101,121,151,161,181,291,211,241,271,276,361,381,400,430,450,460,470,480,490,496,506,507,508,509,520};
const int EEprom_Add[41] = {0, 1, 21, 41, 61, 81, 101, 121, 151, 161, 181, 291, 211, 241, 271, 276,
                            361, 381, 400, 430, 450, 460, 470, 480, 490, 496, 506, 507, 508, 509, 510, 511, 512, 513, 514, 515, 516, 517, 518, 519, 520};
/*{0,1,21,41,61,81,101,121,151,161,181,291,211,241,271,276,361,381,400,430,450,460,470,480,490,496,506,507,508,509};
0 *******
1  local_ip,1
2  subnet,21
3  gateway,41
4  dnsip,61
5  ssid,81
6  password,101
7  APRSIS,121
8  IS_port,151
9  MyCallSign,161     a
10  is_passcode,181
11  JavaFilter,291
12  Beacon,211
13  comment,241
14  interval,271
15  Brate,276         j
16  DigiDelay,361
17  VIAPATH,381       d
18  Profile1,400      b
19  Profile2,430      c
20  Fastspd,450       e
21  Fastrate,460      f
22  Slowspd,470       g
23  Slowrate,480      h
24  mice,490          i
25  Grate 496         k
26  Read_ResetOnTime 506
27  Vin 507 // 0/1 false/true
28  Inet_toRF_KM 508
29  V_offset 509
40  Line_Token,520  t
*/
#ifdef NTP_Serv
void onSTAGotIP(WiFiEventStationModeGotIP event)
{
    //Serial.printf("Got IP: %s\n", event.ip.toString().c_str());
    NTP.init((char *)"th.pool.ntp.org", UTC); //UTC0700  // UTC (for UTC)
    NTP.setPollingInterval(3900);             // Poll sec
}

// Event Handler when WiFi is disconnected
void onSTADisconnected(WiFiEventStationModeDisconnected event)
{
    //Serial.printf("WiFi connection (%s) dropped.\n", event.ssid.c_str());
    // Serial.printf("Reason: %d\n", event.reason);
}
#endif

void setup()
{
    unsigned int WIFI_TX_POWER = 82; //กำลังส่งของ WiFi 0-82

    pinMode(Tx_Now, INPUT_PULLUP);

#ifdef NTP_Serv
    static WiFiEventHandler gotIpEventHandler, disconnectedEventHandler;
    NTP.onSyncEvent([](NTPSyncEvent_t ntpEvent) {
        switch (ntpEvent)
        {
        case NTP_EVENT_INIT:
            break;
        case NTP_EVENT_STOP:
            break;
        case NTP_EVENT_NO_RESPONSE:
            Serial.printf("> NTP server not reachable.\n");
            break;
        case NTP_EVENT_SYNCHRONIZED:
            Serial.printf("> Got NTP time: %s\n", NTP.getTimeDate(NTP.getLastSync()));
            break;
        }
    });
    gotIpEventHandler = WiFi.onStationModeGotIP(onSTAGotIP);
    disconnectedEventHandler = WiFi.onStationModeDisconnected(onSTADisconnected);
#endif

    //-------------------
    EEPROM.begin(1024); //กำหนดแอดเดรส EEPROM 1024ไบต์ แอดเดรส 0-1023
    WIFI_TX_POWER = EEPROM.read(510);
    State_IGATE = EEPROM.read(511);
    if (WIFI_TX_POWER > 82)
    {
        WIFI_TX_POWER = 82;
    }

    WiFi.persistent(false);
    WiFi.mode(WIFI_OFF);
    WiFi.mode(WIFI_AP_STA);             //กำหนดการทำงาน ของESP เป็นโหมด Acesspoint  และ Station
    WiFi.setOutputPower(WIFI_TX_POWER); //กำหนด กำลังส่ง WiFi ตามตัวแปร WIFI_TX_POWER ให้ย้อนไปดูข้างบนที่ #define  ครูน้อยขอให้ส่งต่ำๆ เพราะจะได้ไม่กวนเสียงของTNC ส่งสูงมีเสียงน้ำเดือด
    WiFi.disconnect();
    //------------------

    Serial.begin(9600); //กำหนดความเร็ว พอร์ท TxRx สำหรับการใช้งานตั้งค่า ต่างๆ ความเร็ว 9600 คงที่ ตลอดการใช้งาน
    delay(200);
    Serial.printf("\r\n\r\n> WiFi Tx PWR %d\r\n", WIFI_TX_POWER);
    Serial.printf("> State_IGATE = %d\r\n", State_IGATE);
#ifdef MODEL_INDY
    Serial.println("> I2C Indy Model");
#else
    Serial.println("> I2C Standard Model");
#endif

#ifdef BME
    Wire.begin(SCA, SCL); //SCA,SCL       //เตรียมพอร์ท I2C และขาใช้งาน
    //----------------Scan Address I2C----------
    for (byte i = 1; i < 120; i++) //สแกนพอร์ท I2C เพื่อตรวจสอบว่าอุปกรณ์อะไรต่ออยู่บ้าง ตรงกับแอดเดรสที่กำหนดหรือไม่
    {
        Wire.beginTransmission(i);
        if (Wire.endTransmission() == 0)
        {
            delay(1);
            if (i == BME_address1)
            {
                Ad_BME = i;
                Serial.println("> BME found 0x76");
            }
            if (i == BME_address2)
            {
                Ad_BME = i;
                Serial.println("> BME found 0x77");
            }
        }
    }

    bme280.parameter.communication = 0;          //Set to 0 for I2C (default value)
    bme280.parameter.I2CAddress = Ad_BME;        //Default value
    bme280.parameter.SPI_cs = 10;                //Chip Select Pin used for both Software and Hardware SPI
    bme280.parameter.sensorMode = 0b11;          //In normal mode the sensor measures continually (default value)
    bme280.parameter.IIRfilter = 0b101;          //factor 16 (default value)
    bme280.parameter.humidOversampling = 0b101;  //factor 16 (default value)
    bme280.parameter.tempOversampling = 0b101;   //factor 16 (default value)
    bme280.parameter.pressOversampling = 0b101;  //factor 16 (default value)
    bme280.parameter.pressureSeaLevel = 1013.25; //default value of 1013.25 hPa
    bme280.parameter.tempOutsideCelsius = 15;    //default value of 15°C
#endif

#ifdef OLED
    Wire.begin(SCA, SCL); //SCA,SCL       //เตรียมพอร์ท I2C และขาใช้งาน
    //----------------Scan Address I2C----------
    for (byte i = 1; i < 120; i++) //สแกนพอร์ท I2C เพื่อตรวจสอบว่าอุปกรณ์อะไรต่ออยู่บ้าง ตรงกับแอดเดรสที่กำหนดหรือไม่
    {
        Wire.beginTransmission(i);
        if (Wire.endTransmission() == 0)
        {
            delay(1);
            if (i == byte(OLED_ADD))
            {
                Ad_OLED = i;
                Serial.println("> OLED found 0x3c");
            }
        }
    }
    if (byte(OLED_ADD) == Ad_OLED)
    {
#ifdef inch13
        Serial.println("> Set Display size 1.3 inch");
#else
        Serial.println("> Set Display size 0.96 inch");
#endif
    }
    else
    {
        Serial.println("> OLED Not found 0x3c");
    }

    display.init();
    display.clear();
    display.flipScreenVertically();

#ifdef Two_color //****************จอ 2 สี*******************//
    Serial.println("> Set Display 2 Color");
    display.setTextAlignment(TEXT_ALIGN_CENTER); //HS6TUX_22092561-1335//
    display.setFont(ArialMT_Plain_16);
    display.drawString(64, 0, "WELCOME TO");
    display.setFont(ArialMT_Plain_16);
    display.drawString(64, 26, "Loading....");
    display.drawString(64, 45, "Config");
#else //******************************************//
    Serial.println("> Set Display mono Color");
    display.setFont(ArialMT_Plain_24);
    display.drawString(0, 0, "WELCOME TO");
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.setFont(ArialMT_Plain_16);
    display.drawString(64, 30, "Loading....");
    display.drawString(64, 45, "Config");
#endif

    display.display();
#endif

#ifdef Rain_s
    pinMode(Rain_Pin, INPUT_PULLUP); //กำหนดขา ต่อเซนเซอร์ ตรวจการตกของฝน
#endif
    pinMode(LED_online, OUTPUT);   //กำหนดขา LED แสดงผลการทำงานของ Igate
    digitalWrite(LED_online, LOW); //เมื่อเริ่มเปิดเครื่อง สั่งให้ไฟติดค้าง ไว้ก่อน เพื่อให้รู้ว่าไฟเข้าแล้ว

    ReadEEPROMlng_config(Brate, 276);
    delay(50); //อ่าน EEPROM ตัวแปร Brate ค่าความเร็วในการติดต่อ TNC
    Serial.println(Brate);
    delay(1000);
    if (Brate == 1200)
    {
        TNC_Serial.begin(1200);
    }
    else if (Brate == 2400)
    {
        TNC_Serial.begin(2400);
    }
    else if (Brate == 4800)
    {
        TNC_Serial.begin(4800);
    }
    else if (Brate == 9600)
    {
        TNC_Serial.begin(9600);
    }
    else if (Brate == 14400)
    {
        TNC_Serial.begin(14400);
    }
    else if (Brate == 19200)
    {
        TNC_Serial.begin(19200);
    }
    else if (Brate == 28800)
    {
        TNC_Serial.begin(28800);
    }
    else if (Brate == 38400)
    {
        TNC_Serial.begin(38400);
    }
    else if (Brate == 57600)
    {
        TNC_Serial.begin(57600);
    }
    else if (Brate == 115200)
    {
        TNC_Serial.begin(115200);
    }
    else
    {
        TNC_Serial.begin(9600);
    }

    Serial.printf("\r\n\r\n Type ? for Config or Type # for Debug v%s\r\n", FW_V); //ส่งข้อความ ถาม ? หรือ # เพื่อการตั้งค่า
#ifdef OLED

#ifdef Two_color //****************จอ 2 สี*******************//
    while (counter < 200)
    { // 3.1d 500 to 200
        display.clear();
        /* display.setFont(ArialMT_Plain_24);                       //HS6TUX***************
display.setTextAlignment(TEXT_ALIGN_LEFT);
display.drawString(0, 0, "WELCOME TO");  */
        display.setTextAlignment(TEXT_ALIGN_LEFT);
        //display.setFont(ArialMT_Plain_16);                       //HS6TUX_23092561-1021//
        display.drawXbm(0, 24, 128, 40, homelogo);
        display.setTextAlignment(TEXT_ALIGN_CENTER);
        display.setFont(ArialMT_Plain_16);
        //display.drawString(64, 26, "Loading....");
        int progress = (counter / 2) % 100;               // 3.1d 5 to 2
        display.drawProgressBar(0, 2, 120, 10, progress); //HS6TUX_29092561-2332//
        display.setFont(ArialMT_Plain_10);
        display.setTextAlignment(TEXT_ALIGN_CENTER);
        display.drawString(64, 14, String(progress) + "%"); //HS6TUX_29092561-2332//
        display.display();
        counter++;
        delay(2);
    }
#else            //****************** สีเดียว **********************//
#ifdef HOME_LOGO //ถ้าหากกำหนดว่า ให้หน้าแรกแสดง โลโก้ APRS
    while (counter < 200)
    { // 3.1d 500 to 200
        display.clear();
        //display.setFont(ArialMT_Plain_24);
        display.setTextAlignment(TEXT_ALIGN_LEFT);
        display.drawXbm(0, 24, 128, 40, homelogo);
        display.setTextAlignment(TEXT_ALIGN_CENTER);
        display.setFont(ArialMT_Plain_16);
        //display.drawString(64, 30, "Loading....");
        int progress = (counter / 2) % 100;
        display.drawProgressBar(0, 2, 120, 10, progress);
        display.setFont(ArialMT_Plain_10);
        display.setTextAlignment(TEXT_ALIGN_CENTER);
        display.drawString(64, 12, String(progress) + "%");
        display.display();
        counter++;
        delay(10); // 3.1d 5 ช้า  to 2 เร็ว
    }
#else            //ถ้าหากไม่กำหนด  ให้แสดงเป็นอักษรแทน
    while (counter < 200)
    { // 3.1d 500 to 200
        display.setFont(ArialMT_Plain_24);
        display.setTextAlignment(TEXT_ALIGN_LEFT);
        display.drawString(0, 0, "WELCOME TO");
        display.setTextAlignment(TEXT_ALIGN_CENTER);
        display.setFont(ArialMT_Plain_16);
        display.drawString(64, 30, "Loading....");
        int progress = (counter / 2) % 100;
        display.drawProgressBar(0, 54, 120, 10, progress);
        display.setFont(ArialMT_Plain_10);
        display.setTextAlignment(TEXT_ALIGN_CENTER);
        display.drawString(64, 44, String(progress) + "%");
        display.display();
        counter++;
        delay(2); // 3.1d 5 ช้า  to 2 เร็ว
    }
#endif
#endif

#endif
    delay(1000);
    if (digitalRead(Tx_Now) == 0)
    { //***********เชคปุ่มกดว่า ถ้าถูกกดค้างไว้อยู่ ให้ Factory reset
        factory_reset();
        ESP.restart();
    }

    char RD = Serial.read(); //รอรับค่าจากผู้ใช้ พิมพ์เข้าทาง UART
    if (RD == '#')
    {
        CMD_debug = true;
        Serial.print("\r\nDEBUG....\r\nWiFi for Config: ");
    } //หากพิมพ์ # ให้เข้าโหมด Debug
    if (RD == 'r')
    {
        CMD_debug = true;
        CHK_Repair = true;
        Serial.println("\r\nRepair....");
    } //หากพิมพ์ r เข้าโหมด ทดสอบ
    if (RD == '?')
    { //ถ้าผู้ใช้ พิมพ์ ? เข้ามา  ให้เข้าสู่โหมดการตั้งค่า
        CMD_config = true;
        CMD_debug = true;
        Serial.println("\r\n> Please Config : "); //ส่งข้อความแนะนำการตั้งค่าออกทาง UART
        Serial.println("Example command type ?wa?192.168.1.22 and enter");
        Serial.println("--------- ตัวอย่าง ---------");
        Serial.println("?wa?192.168.1.22 [Static IP Igate]");
        Serial.println("?wb?255.255.255.0 [Subnet]");
        Serial.println("?wc?192.168.1.1 [Gateway]");
        Serial.println("?wd?8.8.8.8 [DNS]");
        Serial.println("?we?MyhomeWIFI [SSID WIFI]");
        Serial.println("?wf?123456 [Password WIFI]");
        Serial.println("?wg?aprsth.nakhonthai.net [APRS Server]");
        Serial.println("?wh?14580 [APRSIS Port]");
        Serial.println("?wi?HS3LSE-1 [Mycall]");
        //Serial.println("?wj?000000 [Passcode]");
        Serial.println("?wk?m/1 [filter]");
        Serial.println("?wl?!1455.03N/10329.84E& [beacon Location table and symbol only]");
        Serial.println("?wm?MyIgate [comment]");
        Serial.println("?wn?15 [Interval beacon minute]");
        Serial.println("?wo?9600 [Baud Rate]");
        Serial.println("?wp?WIDE1-1 [IGATE Via PATH]");
        Serial.println("?wq?0 [DIGIPEATER]");
        Serial.println("?wr?0 [Restart every]");
        Serial.println("?wt? LINE TOKEN");
        Serial.println("***************************");
        Serial.println("?na? [Lookup IP]");
        Serial.println("?nb? [Scan Network]");
        Serial.println("?nd? [Connect APRS Serv]");
        Serial.println("***************************");
        Serial.println("?restart [Restart System]");
        Serial.println("***************************");
        Serial.println("For read your Config  ?ra? - ?ro?  or ?DISP for read all  ");
#ifdef OLED
        display.clear();
        display.setTextAlignment(TEXT_ALIGN_CENTER);
        display.setFont(ArialMT_Plain_16);
        display.drawString(64, 26, "Config mode");
        display.display();
#endif
    }
    else if (EEPROM.read(0) != '?')
    { //หรือเชค ที่EEPROM แอดเดรส 0 ว่าเคยมีการบันทึกค่ามาก่อนหรือไม่ ถ้าไม่เคยมี
        factory_reset();
    }
    else
    {

        InitEEPROM(); //***ไปทำงานในฟังก์ชั่น อ่านค่าที่ตั้งไว้จาก EEPROM

        //WiFi.softAPConfig(ip, ip, netmask); // configure ip address for softAP //ตั้งค่าIP Subnet Gate way ประจำตัว ให้ ESP
        if (MyCallSign[0] > 64)
        {
            WiFi.softAP(MyCallSign, password_s, 1, false);
        }
        else
        {
            WiFi.softAP(ssid_s, password_s, 1, false);
        }

        delay(1);
        if (local_ip[0] != 0)
        {
            Serial.print(Label_config[1]);
            Serial.printf("%d.%d.%d.%d\r\n", local_ip[0], local_ip[1], local_ip[2], local_ip[3]);
            Serial.print(Label_config[2]);
            Serial.printf("%d.%d.%d.%d\r\n", subnet[0], subnet[1], subnet[2], subnet[3]);
            Serial.print(Label_config[3]);
            Serial.printf("%d.%d.%d.%d\r\n", gateway[0], gateway[1], gateway[2], gateway[3]);
            Serial.print(Label_config[4]);
            Serial.printf("%d.%d.%d.%d\r\n", dnsip[0], dnsip[1], dnsip[2], dnsip[3]);
            WiFi.config(local_ip, gateway, subnet, dnsip); //ตั้ง IP / Gateway / Subnet กรณี ไม่ใช้ DHCP   //HS3LSE 23.47  20/10/18
        }

        server.begin();
        delay(1);
        server.setNoDelay(true);
        delay(1);
        httpUpdater.setup(&httpServer);
        delay(1);
        httpServer.on("/", handleRoot);
        delay(1);
        httpServer.on("/config", handleConfigArg);
        delay(1);
        httpServer.on("/status", handleStatusArg);
        delay(1);
        httpServer.on("/call", handleCallArg);
        delay(1);
        httpServer.begin();
        delay(1);
        Serial.println("> Start server , httpUpdater , httpServer ");

        ////////////////////////////////////////////////////////////-------CHECK AP ON / OFF--------//////////////////////////////////////////////////////
        Read_PWR = EEPROM.read(505); //อ่านการตั้งค่าในEEPROM แอดเดรส 505 ว่าเคยตั้งค่าเป็น AP ON หรือ OFF
        if (Read_PWR == 0)
        {                                    //ถ้า Read_PWR==0
            APOFF_Beep(Buzzer, frq, timeOn); ////************HS6TUX-16.00-23/07/19
            Serial.printf("> AP OFF / Please press Button for switch ON \r\n");
            display.clear();
            display.setFont(ArialMT_Plain_24);
            display.setTextAlignment(TEXT_ALIGN_CENTER);
            display.drawString(64, 20, "AP OFF"); //ให้แสดงผลที่ OLED ว่า OFF
            display.display();
        }
        else
        {                                   //ถ้า Read_PWR==1
            APON_Beep(Buzzer, frq, timeOn); ////************HS6TUX-16.00-23/07/19
            Serial.printf("> AP ON / Please press Button for switch OFF\r\n");
            display.clear();
            display.setFont(ArialMT_Plain_24);
            display.setTextAlignment(TEXT_ALIGN_CENTER);
            display.drawString(64, 20, "AP ON"); //ให้แสดงผลที่ OLED ว่า ON
            display.display();
        }
        delay(2000);

        if (digitalRead(Tx_Now) == 0)
        { //***********เชคปุ่มกดว่า ถ้าถูกกดค้างไว้อยู่ ให้ สั่งตรวจการควบคุม AP ON หรือ OFF ทางตัวแปร
            if (Read_PWR == 0)
            { //ถ้าเคยตั้งเป็น OFF
                Serial.printf("> AP ON / Please press Button for switch OFF\r\n");
                delay(50);
                EEPROM.write(505, 1);
                EEPROM.commit();
                delay(50); //บันทึก ค่าใหม่ลงไป เป็น 1  แสดงว่าสั่งให้ON
                display.clear();
                display.setFont(ArialMT_Plain_24);
                display.setTextAlignment(TEXT_ALIGN_CENTER);
                display.drawString(64, 20, "AP ON"); //ให้แสดงผลที่ OLED ว่า ON
                display.display();
                Read_PWR = 1;                   //เก็บค่า 1 ลงใน Read_PWR
                APON_Beep(Buzzer, frq, timeOn); ////************HS6TUX-16.00-23/07/19
                delay(2000);
            }
            else
            { //ถ้าเคยตั้งเป็น ON
                Serial.printf("> AP OFF / Please press Button for switch ON\r\n");
                EEPROM.write(505, 0);
                EEPROM.commit();
                delay(50); //บันทึก ค่าใหม่ลงไป เป็น 0  แสดงว่าสั่งให้OFF
                display.clear();
                display.setFont(ArialMT_Plain_24);
                display.setTextAlignment(TEXT_ALIGN_CENTER);
                display.drawString(64, 20, "AP OFF"); //ให้แสดงผลที่ OLED ว่า OFF
                display.display();
                if (WIFI_TX_POWER == 0)
                {
                    WiFi.disconnect();      //สั่งปลดการเชื่อมต่อ
                    WiFi.forceSleepBegin(); //สั่ง sleep หรือ ปิด WiFi
                }
                Read_PWR = 0;
                APOFF_Beep(Buzzer, frq, timeOn); ////************HS6TUX-16.00-23/07/19
                delay(2000);
            }
        }
        if (Read_PWR == 0)
        {
            /* Serial.println("> WiFi Sleep");
WiFi.disconnect();
WiFi.forceSleepBegin();//สั่ง sleep หรือ ปิด WiFi     */
            Serial.println("> WiFi Stop");
            WiFi.softAPdisconnect(true);
        }

        ////////////////////////////////////////////////////////////-----------------------------------------//////////////////////////////////////////////////////

#ifdef OLED
        display.clear();
#ifdef Two_color //****************จอ 2 สี*******************//
        display.setFont(ArialMT_Plain_16);
        display.drawString(64, 0, MyCallSign);
        display.drawString(64, 18, "IndyIGATE v." + String(FW_V));
#else //*****************************************//
        display.setFont(ArialMT_Plain_16);
        display.drawString(64, 0, MyCallSign);
        display.drawString(64, 18, "IndyIGATE v." + String(FW_V));
#endif
        display.display();
#endif
#ifdef OLED
#ifdef BME
        if (bme280.init() == 0x60)
        {
            display.setFont(ArialMT_Plain_10);
            display.drawString(64, 36, "BME280 Found");
            display.display();
            Serial.println("> BME280 Found");
        }
#endif
        if (Ad_OLED != byte(OLED_ADD))
        {
            display.setFont(ArialMT_Plain_10);
            display.drawString(64, 36, String(Ad_OLED) + String(OLED_ADD));
            display.drawString(64, 48, "                 ");
            display.drawString(64, 48, "OLED notFound");
            display.display();
        }
        else
        {
            display.setFont(ArialMT_Plain_10);
            display.drawString(64, 48, "                      ");
            display.drawString(64, 48, "OLED Found");
            display.display();
        }

        delay(2000);
#endif

        ScanNetwork();
    }

    ////////////////////////////////////////////////////////////-------CHECK IGATE ON / OFF--------//////////////////////////////////////////////////////

    State_IGATE = EEPROM.read(511); //อ่านการตั้งค่าในEEPROM แอดเดรส 511 ว่าเคยตั้งค่าเป็น IGATE ON หรือ OFF
    if (State_IGATE == 0)
    {                                    //ถ้า ==0
        IGOFF_Beep(Buzzer, frq, timeOn); ////************HS6TUX-16.00-23/07/19
        Serial.printf("> IGATE OFF / Please press Button for switch ON\r\n");
        display.clear();
        display.setFont(ArialMT_Plain_24);
        display.setTextAlignment(TEXT_ALIGN_CENTER);
        display.drawString(64, 20, "IGATEOFF"); //ให้แสดงผลที่ OLED ว่า OFF
        display.display();
        IGATE_ON = false;
        delay(2000);
    }
    else
    {                                   //ถ้า ==1
        IGON_Beep(Buzzer, frq, timeOn); ////************HS6TUX-16.00-23/07/19
        Serial.printf("> IGATE ON / Please press Button for switch OFF\r\n");
        IGATE_ON = true;
        display.clear();
        display.setFont(ArialMT_Plain_24);
        display.setTextAlignment(TEXT_ALIGN_CENTER);
        display.drawString(64, 20, "IGATE ON"); //ให้แสดงผลที่ OLED ว่า ON
        display.display();
        delay(2000);
    }

    if (digitalRead(Tx_Now) == 0)
    { //***********เชคปุ่มกดว่า ถ้าถูกกดค้างไว้อยู่ ให้ สั่งตรวจการควบคุม IGATE On หรือ OFF
        if (State_IGATE == 0)
        { //ถ้าเคยตั้งเป็น OFF
            Serial.printf("> IGATE ON / Please press Button for switch OFF\r\n");
            delay(50);
            EEPROM.write(511, 1);
            EEPROM.commit();
            delay(50); //บันทึก ค่าใหม่ลงไป เป็น 1  แสดงว่าสั่งให้ON
            display.clear();
            display.setFont(ArialMT_Plain_24);
            display.setTextAlignment(TEXT_ALIGN_CENTER);
            display.drawString(64, 20, "IGATE ON"); //ให้แสดงผลที่ OLED ว่า ON
            display.display();
            IGATE_ON = true;
            IGON_Beep(Buzzer, frq, timeOn); ////************HS6TUX-16.00-23/07/19
            delay(2000);
        }
        else
        { //ถ้าเคยตั้งเป็น ON
            Serial.printf("> IGATE OFF / Please press Button for switch ON\r\n");
            EEPROM.write(511, 0);
            EEPROM.commit();
            delay(50); //บันทึก ค่าใหม่ลงไป เป็น 0  แสดงว่าสั่งให้OFF
            display.clear();
            display.setFont(ArialMT_Plain_24);
            display.setTextAlignment(TEXT_ALIGN_CENTER);
            display.drawString(64, 20, "IGATEOFF"); //ให้แสดงผลที่ OLED ว่า OFF
            display.display();
            IGATE_ON = false;
            IGOFF_Beep(Buzzer, frq, timeOn); ////************HS6TUX-16.00-23/07/19
            delay(2000);
        }
    }

    if (local_ip[0] != 0 && IGATE_ON == true)
    {
        WiFi.config(local_ip, gateway, subnet, dnsip); //ตั้ง IP / Gateway / Subnet กรณี ไม่ใช้ DHCP  //HS3LSE 23.47  20/10/18
    }

    digitalWrite(LED_online, HIGH);
    Serial.println("> Send Command TNC init");
    CMD_KISS_Parameter();

    if (IGATE_ON == false)
    {
        delay(500);
        if (CMD_config == false)
        {
            Run_Time_Sec = interval * 60;
            digitalWrite(LED_online, LOW); // 09.19 141018 HS3LSE
#ifdef OLED
            display.clear();
#ifdef Two_color //****************จอ 2 สี ***************************//
            display.setTextAlignment(TEXT_ALIGN_LEFT);
            display.setFont(ArialMT_Plain_16); //HS6TUX***************//
            display.drawString(0, 0, ">Beacon");
            display.setFont(ArialMT_Plain_10);
            String BB = String(MyCallSign) + ">" + FW_destNET + ":" + String(Beacon) + String(comment);
            // display.drawStringMaxWidth(0, 14, 128,BB);
            display.drawString(0, 14, BB.substring(0, 20));
            display.drawString(0, 24, BB.substring(20, 40));
            display.drawString(0, 34, BB.substring(40, 60));
            display.drawString(0, 44, BB.substring(60, 80));  //HS6TUX-21.32-11.10.18//
            display.drawString(0, 54, BB.substring(80, 100)); //HS6TUX 01.18 12/10/18
#else                                                         //**************************************************//
            display.setTextAlignment(TEXT_ALIGN_LEFT);
            display.setFont(ArialMT_Plain_24);
            display.drawString(0, 0, ">Beacon");
            display.setFont(ArialMT_Plain_10);
            String BB = String(MyCallSign) + ">" + FW_destNET + ":" + String(Beacon) + String(comment);
            display.drawStringMaxWidth(0, 25, 128, BB);
#endif

            display.display();
#endif
        }
    }
    else
    {
        wifistart(); //************************ Prepare to Start Wifi connect***********************************************//
    }

#ifdef D_H_T
    dht.begin();
#endif
}

/*"(---------------------------------------------------------------------)"*/
/*"(-                           Read EEprom and Convert Value           -)"*/
/*"(---------------------------------------------------------------------)"*/
String EP_write_char(int EP_data1, String RawCMD)
{
    int i = 0;
    int len = RawCMD.length();
    String RawData = RawCMD.substring(4, len);
    len = RawData.length();
    for (i = 0; i <= len; i++)
    {
        if (RawData.charAt(i) > 31)
        {
            EEPROM.write(i + EP_data1, RawData.charAt(i));
            EEPROM.commit();
            delay(20);
        }
        else
        {
            EEPROM.write(i + EP_data1, '\0');
            EEPROM.commit();
            delay(20);
        }
    }
    EEPROM.write(0, '?');
    EEPROM.commit();
    delay(20);
    return RawData;
}

String EP_read_char(int EP_data)
{
    int i = 0;
    String R_data = "";
    while (EEPROM.read(EP_data) != 0)
    {
        char R_EP = EEPROM.read(EP_data);
        if (R_EP < 32 or R_EP > 127)
        {
            return R_data;
        }
        if (R_EP != '?')
        {
            R_data = R_data + char(R_EP);
        }
        EP_data++;
        delay(10);
    }
    return R_data;
}

/*"(---------------------------------------------------------------------)"*/
/*"(-                           Read EEprom and Convert Value           -)"*/
/*"(---------------------------------------------------------------------)"*/

void ReadEEPROMlng_config(long &val, int str)
{
    char buff[10];
    char *Atolng = buff;
    int repm;
    int sb = 0;
    int str_ini = str;
    char lp_rd = EEPROM.read(str);
    for (; lp_rd > 47 && lp_rd < 58; str++)
    {
        CRate[sb] = buff[sb] = lp_rd = EEPROM.read(str);
        sb++;
        if (sb > 9)
        {
            buff[0] = '9';
            buff[1] = '6';
            buff[2] = '0';
            buff[3] = '0';
            buff[4] = '\0';
            CRate[0] = '9';
            CRate[1] = '6';
            CRate[2] = '0';
            CRate[3] = '0';
            CRate[0] = '\0';
            EEPROM.write(str_ini, '9');
            EEPROM.write(str_ini + 1, '6');
            EEPROM.write(str_ini + 2, '0');
            EEPROM.write(str_ini + 3, '0');
            EEPROM.write(str_ini + 4, '\0');
            EEPROM.commit();
            delay(50);
            break;
        }
    }
    CRate[sb] = buff[sb] = 0;
    val = atol(Atolng);
    if (val != 1200 && val != 2400 && val != 4800 && val != 9600 && val != 14400 && val != 19200 && val != 38400 && val != 57600 && val != 115200)
    {
        EEPROM.write(str_ini, '9');
        EEPROM.write(str_ini + 1, '6');
        EEPROM.write(str_ini + 2, '0');
        EEPROM.write(str_ini + 3, '0');
        EEPROM.write(str_ini + 4, '\0');
        EEPROM.commit();
        delay(50);
        val = 9600;
    }
}

void ReadEEPROMbyte_config(byte *val, int str)
{
    char buff_[17] = "";
    char *AtoInt = buff_;
    int repm;
    int sb = 0;
    int numArr = 0;

    for (; numArr < 4; str++)
    {
        repm = EEPROM.read(str);
        if ((repm < 32 or repm > 127) and repm != 0)
        {
            return;
        }
        if (repm == '.')
        {
            buff_[sb] = '\0';
            sb = 0;
            val[numArr] = atoi(AtoInt);
            numArr++;
        }
        else if (repm > 31)
        {
            buff_[sb] = repm;
            sb++;
        }
        else
        {
            buff_[sb] = '\0';
            val[numArr] = atoi(AtoInt);
            numArr++;
        }
    }
}
//-------------------------------------InitEEPROM----------------------------------//
/*int EEprom_Add[29]= {0,1,21,41,61,81,101,121,151,161,181,291,211,241,271,276,361,381,400,430,450,460,470,480,490,496,506,507,508,509};
0 *******
1  local_ip,1
2  subnet,21
3  gateway,41
4  dnsip,61
5  ssid,81
6  password,101
7  APRSIS,121
8  IS_port,151
9  MyCallSign,161     a
10  is_passcode,181
11  JavaFilter,291
12  Beacon,211
13  comment,241
14  interval,271
15  Brate,276         j
16  DigiDelay,361
17  VIAPATH,381       d
18  Profile1,400      b
19  Profile2,430      c
20  Fastspd,450       e
21  Fastrate,460      f
22  Slowspd,470       g
23  Slowrate,480      h
24  mice,490          i
25  Grate 496         k
26  Read_ResetOnTime 506
27  Vin 507 // 0/1 false/true
28  Inet_toRF_KM 508
29  V_offset 509
*/

void InitEEPROM()
{ //**********ไปดึงการตั้งค่า จาก EEPROM เข้าตัวแปรต่างๆที่ใช้งานในตอนเริ่มเปิดเครื่อง
    Serial.println("> Initial EEProm");
    // ReadEEPROMchr_config(ตัวแปร,แอดเดรสของEEPROM)
    String BUF_EEPROM = "";
    //---------------------------local_ip---------------------//
    ReadEEPROMbyte_config(local_ip, EEprom_Add[1]);
    //---------------------------subnet---------------------//
    ReadEEPROMbyte_config(subnet, EEprom_Add[2]);
    //---------------------------gateway---------------------//
    ReadEEPROMbyte_config(gateway, EEprom_Add[3]);
    //---------------------------dnsip---------------------//
    ReadEEPROMbyte_config(dnsip, EEprom_Add[4]);
    //---------------------------ssid---------------------//
    BUF_EEPROM = EP_read_char(EEprom_Add[5]);
    BUF_EEPROM.toCharArray(ssid, 20);
    //---------------------------password-----------------//
    BUF_EEPROM = EP_read_char(EEprom_Add[6]);
    BUF_EEPROM.toCharArray(password, 20);
    //--------------------------- APRSIS-------------------//
    BUF_EEPROM = EP_read_char(EEprom_Add[7]);
    BUF_EEPROM.toCharArray(APRSIS, 30);
    //--------------------------- IS_port-------------------//
    BUF_EEPROM = EP_read_char(EEprom_Add[8]);
    IS_port = BUF_EEPROM.toInt();
    //----------------------MyCallSign------------------------//

    BUF_EEPROM = EP_read_char(EEprom_Add[9]);
    BUF_EEPROM.toCharArray(MyRawCall, 20);
    String MYCALL = "";
    int len_CALL = BUF_EEPROM.length();
    String MYCALL_only = "";
    String MYSSID_only = "";
    int Pos_Item = BUF_EEPROM.indexOf(")");
    if (Pos_Item > 0)
    {
        ITEM = BUF_EEPROM.substring(Pos_Item, len_CALL);
        MYCALL = BUF_EEPROM.substring(0, Pos_Item);
        MYCALL.toCharArray(MyCallSign, 11);
    }
    else
    {
        MYCALL = BUF_EEPROM.substring(0, len_CALL);
        MYCALL.toCharArray(MyCallSign, 11);
    }
    //----------------------is_passcode------------------------//
    aprspass();
    //BUF_EEPROM = EP_read_char(EEprom_Add[10]);
    //BUF_EEPROM.toCharArray(is_passcode,6);
    //----------------------JavaFilter------------------------//
    BUF_EEPROM = EP_read_char(EEprom_Add[11]);
    BUF_EEPROM.toCharArray(JavaFilter, 60);
    //----------------------Beacon------------------------//
    BUF_EEPROM = EP_read_char(EEprom_Add[12]);
    BUF_EEPROM.toCharArray(Beacon, 21);
    //----------------------comment------------------------//
    BUF_EEPROM = EP_read_char(EEprom_Add[13]);
    BUF_EEPROM.toCharArray(comment, 30);
    //----------------------interval------------------------//
    BUF_EEPROM = EP_read_char(EEprom_Add[14]);
    interval = BUF_EEPROM.toInt();
    //----------------------VIAPATH------------------------//
    BUF_EEPROM = EP_read_char(EEprom_Add[17]);
    BUF_EEPROM.toCharArray(VIAPATH, 20);
    //----------------------DigiDelay------------------------//
    BUF_EEPROM = EP_read_char(EEprom_Add[16]);
    DigiDelay = BUF_EEPROM.toInt();

    //----------------------LINE TOKEN------------------------//
    LINE_TOKEN = EP_read_char(EEprom_Add[40]);
#ifdef LINE_Notify
//LINE.setToken(LINE_TOKEN);
#endif

    if (DigiDelay > 5)
    {
        DigiDelay = 1;
    }
    Read_ResetOnTime = EEPROM.read(506);
    delay(20);
    //Inet_toRF_KM = EEPROM.read(508);delay(20);
    //---------------------- Brate----------------------//
    BUF_EEPROM = EP_read_char(EEprom_Add[15]);
    if (BUF_EEPROM.indexOf("1200") == 0)
    {
        Brate = 1200;
    } ////กำหนดให้ ความเร็ว ที่ตั้งไว้ให้กับพอร์ท TNC_Serial
    else if (BUF_EEPROM.indexOf("2400") == 0)
    {
        Brate = 2400;
    }
    else if (BUF_EEPROM.indexOf("4800") == 0)
    {
        Brate = 4800;
    }
    else if (BUF_EEPROM.indexOf("9600") == 0)
    {
        Brate = 9600;
    }
    else if (BUF_EEPROM.indexOf("14400") == 0)
    {
        Brate = 14400;
    }
    else if (BUF_EEPROM.indexOf("19200") == 0)
    {
        Brate = 19200;
    }
    else if (BUF_EEPROM.indexOf("28800") == 0)
    {
        Brate = 28800;
    }
    else if (BUF_EEPROM.indexOf("38400") == 0)
    {
        Brate = 38400;
    }
    else if (BUF_EEPROM.indexOf("57600") == 0)
    {
        Brate = 57600;
    }
    else if (BUF_EEPROM.indexOf("115200") == 0)
    {
        Brate = 115200;
    }
    else
    {
        Brate = 9600;
    }

    //----------------------------------------------------//
    int State_IGATE = EEPROM.read(511);
    if (State_IGATE == 0)
    {
        IGATE_ON == false;
    }
    else
    {
        IGATE_ON = true;
    }
}

////////////////////////////////////////////////////////////////-----------LOOP----------////////////////////////////////////////////////////////
void loop()
{
    static int previousMillis = 0;
    httpServer.handleClient(); //เชคเซสชั่นการทำงานของเวบ
    SAB_CONFIG();              //การอ่านค่าจาก APRS-IS และ TNC เพื่อรับส่งข้อมูลระหว่างกัน

    while (Serial.available()) //--------Read Command from Serial---------//อ่านค่าจากพอร์ท UART สำหรับการตั้งค่า จากซีเรียลพอร์ท
    {
        RawCommand[indexCommand] = Serial.read();
        if (RawCommand[indexCommand] < 14)
        {
            RawCommand[indexCommand] = '\0';
            indexCommand = 0;
            if (RawCommand[0] == '?')
            {
                Serial.println(RawCommand);
                bool UART = true;
                CMD(String(RawCommand), UART);
            }
        }
        else
        {
            indexCommand++;
        }
    }

#ifdef To_BeaconINET
    while (client.available()) //--------Read Packet from APRS-IS---------//อ่านค่าจาก APRS-IS
    {

        RawServer[indexServer] = client.read();
        if (RawServer[indexServer] < 14)
        {
            RawServer[indexServer] = '\0';
            if (serverClients[0] && serverClients[0].connected() && RawServer[0] > 32 && RawServer[0] != '#')
            {
                serverClients[0].printf("%s\r\n", RawServer);
            }
            if (RawServer[0] == '#')
            {
                if (CMD_debug == true)
                { /*Serial.println(RawServer);*/
                }
            }
            else if (RawServer[0] > 47 && indexServer < (buffsize - 2))
            {

                if (CMD_debug == true)
                {
                    Serial.print(F("Free RAM:-->"));
                    Serial.println(ESP.getFreeHeap());
                    Serial.println("GET Frm IS--> OK");
                }
#ifdef OLED
                BUF_display = String(RawServer);
                Serial.println(NTP_DATETIME_sec + " fm APRS SRV | " + BUF_display);
                Inet = true;
                TX_Beep(Buzzer, frq, timeOn);
                if (Page == 3 or Page == 4 or Page == 5 or Page == 6)
                {
                }
                else
                {
                    Show_frm_Srv();
                }
#endif

                if (FWD_RF == true)
                {
                    String S_RawServer = String(RawServer);
                    String My_Rev = ',' + MyCallSign + ':';
                    int pos_My_Rev = S_RawServer.indexOf(My_Rev); // เชคว่าต้องไม่ใช่แพกเก็ตที่IGATEตัวเองรับเข้าไป//2128 22/10/18  HS3LSE
                    if (pos_My_Rev < 1)
                    {
                        TLM_T++;
                        Tx_cnt++;

                        BUFF = "}" + String(RawServer);
                        SendAPRS(String(VIAPATH));
                        if (CMD_debug == true)
                        {
                            Serial.printf("IS>KISS--> OK \r\n");
                        }
                        RawServer[0] = 0;
                    }
                }
            }
            indexServer = 0;
        }
        else if (indexServer < (buffsize - 2))
        {
            indexServer++;
        }
    }
#endif

    //---------------------------------------------//

    while (TNC_Serial.available() || STARTKISS == 2) //----------------Read Serial ------------------//อ่านค่าจาก TNC ที่รับข้อมูลมาจากวิทยุ
    {
        if (STARTKISS == 0)
        {
            //ข้อมูลที่ผ่านได้จะเป็นดังนี้*******************
            //C0 00 86 A2 40 40 40 40 60 90 A6 66 98 A6 8A 6D 03 F0 54 45 53 54 C0
            //       C  Q              H  S 3   L  S  E  1 0         T  E  S T

            char c = TNC_Serial.read();

            //if(CMD_debug==true){Serial.print(c);}
            /*
RawSerial[indexSerial]=c;// HS3LSE 10.50 27/09/18//

if (RawSerial[indexSerial] ==13 || RawSerial[indexSerial] ==10 || indexSerial>(buffsize-2))//if (RawSerial[indexSerial] <14 || indexSerial>(buffsize-2))
{
RawSerial[indexSerial]='\0';
indexSerial=0;
// if(CMD_debug==true){Serial.println("");}
if (((RawSerial[0]>47 && RawSerial[0]<58)|| (RawSerial[0]>64 && RawSerial[0]<91))  && ( RawSerial[3]=='>' || RawSerial[4]=='>' || RawSerial[5]=='>' || RawSerial[6]=='>' || RawSerial[7]=='>' || RawSerial[8]=='>' || RawSerial[9]=='>'  )   ) {
#ifdef OLED
BUF_display = String(RawSerial);
Inet=false;
Rx_cnt++;
RX_Beep(Buzzer,frq,timeOn);
if(Page!=3){
if(BUF_display.indexOf("::")>0){CHK_MSG=true;}
Show_frm_Srv();
}
RawSerial[0]=0;
indexSerial=0;
#endif
}else{RawSerial[0]=0;indexSerial=0;}



}else{
indexSerial++;
// if(CMD_debug==true){Serial.print(indexSerial);}
}
*/
            if (c != 10 && c != 13)
            {
                KISS[indexkiss] = c;
            }
            //if(KISS[indexkiss]==0xF0){Tag_F0=indexkiss;}
            if (c != 0xC0 && c != 10 && c != 13)
            {
                if (CMD_debug == true)
                {
                    Serial.printf("%c%d", c, indexkiss);
                }
                indexkiss++;
                if (indexkiss > (buffsize - 2))
                {
                    indexkiss = 1;
                    return;
                }
            }
            else
            {
                KISS[0] = 0xC0;
                KISS[indexkiss] = 0xC0;
                int Tag_Fo = 0, Tag_X3 = 0; //HS3LSE 10.29 30/09/18
                for (int m = 0; m <= indexkiss; m++)
                {
                    if (KISS[m] == 0xF0)
                    {
                        Tag_Fo = m;
                    }
                    if (KISS[m] == 0x03)
                    {
                        Tag_X3 = m;
                    }
                } //HS3LSE 10.29 30/09/18
                if (indexkiss > 18 and KISS[0] == 0xC0 and KISS[1] == 0x00 && Tag_Fo > 15 && Tag_X3 > 15)
                {
                    CHK_KISS = true;
                    if (serverClients[0].connected())
                    {
                        for (int m = 0; m <= indexkiss; m++)
                        {
                            serverClients[0].write(KISS[m]);
                        }
                    }
                }
                else
                {
                    indexkiss = 1;
                    indexSerial = 0;
                    return;
                };
                indexkiss = 1;
                indexSerial = 0;
            }
            //---------------------------------------------
        }
        //}  **3.1d fix bug ย้ายปิดลูป while เชค รับเข้าทาง TNC เมื่อ ครบถึง 0xC0 แล้ว ให้ ไปถอดKISS เลยทันที ป้องกัน การเก็บค่าอื่นเข้าทับ KISS ทำให้ Destiation ตัวแรกผิด
        if (CHK_KISS == true || STARTKISS == 2) // STARTKISS==2 =>>> get PKT from Droid  insert to APRS-IS
        {
            char PATH[8];
            char SRC[10], DST[10], PTH[10];
            char DEST[10], SOURCE[10], DIGIED[56];                  //KINFO[100];
            String S_SRC = "", S_DST = "", S_PTH = "", S_INFO = ""; //,S_RAW="";
            boolean CHK_DIGI = false, NOGATE = false;
            int A = 0, B = 0, C = 0;
            STARTKISS = 0;
            TxMODEKISS = true;
            CHK_KISS = false;
            NOGATE = false;
            for (int s = 0; s < 16; s++)
            {
                DIGIKISS[s] = KISS[s];
            }
            if (CMD_debug == true)
            {
                Serial.printf(" Get INFO Frame--> \r\n");
            }
            if (CMD_debug == true)
            {
                serverClients[0].println(millis());
            }
            while (KISS[A] != 0x03)
            {
                A++;
            }
            while (KISS[A + 2 + C] != 0xC0) //-----------------Get INFO Frame------------------//
            {
                S_INFO = S_INFO + char(KISS[A + 2 + C]); //KINFO[C]=KISS[A+2+C];
                C++;
            }

            //S_INFO =':'+S_INFO;
            if (CMD_debug == true)
            {
                Serial.println("Start Decode KISS--> OK");
            }
            Decodekiss(SOURCE, KISS, 9, 6);
            S_SRC = String(SOURCE) + KISS_SSID(KISS[15]) + '>';
            Last_Call = String(SOURCE) + KISS_SSID(KISS[15]); //17.45   8/03/19   HS6TUX
            Decodekiss(DEST, KISS, 2, 6);
            S_DST = String(DEST) + KISS_SSID(KISS[8]);
            if (A >= 23)
            {
                Decodekiss(PATH, KISS, 16, 6);
                S_PTH = ',' + String(PATH) + KISS_SSID(KISS[22]);
                String MYDIGI = ',' + String(MyCallSign);
                if ((KISS[22] & 0x80) == 0x80)
                {
                    S_PTH = S_PTH + '*';
                    DIGI_ON = false;
                }
                else
                {
                    if (S_PTH.indexOf(",WIDE1-1") == 0 or S_PTH.indexOf(MYDIGI) == 0)
                    {
                        CHK_DIGI = true;
                    }
                    else
                    {
                        DIGI_ON = false;
                    }
                }
            }
            if (CMD_debug == true)
            {
                if (CHK_DIGI)
                {
                    Serial.println("CHK DiGi--> True");
                }
                else
                {
                    Serial.println("CHK DiGi--> False");
                }
            }
            if (A >= 30)
            {
                Decodekiss(PATH, KISS, 23, 6);
                S_PTH = S_PTH + ',' + String(PATH) + KISS_SSID(KISS[29]);
                if ((KISS[29] & 0x80) == 0x80)
                {
                    S_PTH = S_PTH + '*';
                }
            }
            if (A >= 37)
            {
                Decodekiss(PATH, KISS, 30, 6);
                S_PTH = S_PTH + ',' + String(PATH) + KISS_SSID(KISS[36]);
                if ((KISS[36] & 0x80) == 0x80)
                {
                    S_PTH = S_PTH + '*';
                }
            }
            if (A >= 44)
            {
                Decodekiss(PATH, KISS, 37, 6);
                S_PTH = S_PTH + ',' + String(PATH) + KISS_SSID(KISS[43]);
                if ((KISS[43] & 0x80) == 0x80)
                {
                    S_PTH = S_PTH + '*';
                }
            }
            if (A >= 51)
            {
                Decodekiss(PATH, KISS, 44, 6);
                S_PTH = S_PTH + ',' + String(PATH) + KISS_SSID(KISS[50]);
                if ((KISS[50] & 0x80) == 0x80)
                {
                    S_PTH = S_PTH + '*';
                }
            }
            if (A >= 58)
            {
                Decodekiss(PATH, KISS, 51, 6);
                S_PTH = S_PTH + ',' + String(PATH) + KISS_SSID(KISS[57]);
                if ((KISS[57] & 0x80) == 0x80)
                {
                    S_PTH = S_PTH + '*';
                }
            }
            if (A >= 65)
            {
                Decodekiss(PATH, KISS, 58, 6);
                S_PTH = S_PTH + ',' + String(PATH) + KISS_SSID(KISS[64]);
                if ((KISS[64] & 0x80) == 0x80)
                {
                    S_PTH = S_PTH + '*';
                }
            }
            if (A >= 72)
            {
                Decodekiss(PATH, KISS, 65, 6);
                S_PTH = S_PTH + ',' + String(PATH) + KISS_SSID(KISS[71]);
                if ((KISS[71] & 0x80) == 0x80)
                {
                    S_PTH = S_PTH + '*';
                }
            }
            String S_Head = S_SRC + S_DST + S_PTH;

            if ((S_INFO.indexOf(",TCP") > 0) || (S_INFO.indexOf(",qA") > 0) || (S_INFO.indexOf(",RFONLY") > 0) || (S_INFO.indexOf(",NOGATE") > 0))
            {
                TLM_D++;
                RX_Beep(Buzzer, frq, timeOn);
                NOGATE = true;
#ifdef OLED
                BUF_display = S_INFO;
                Serial.println(NTP_DATETIME_sec + " fm TNC Reject | " + S_Head + ":" + S_INFO);
                Inet = true;
                if (Page != 3)
                {
                    if (BUF_display.indexOf("::") > 0)
                    {
                        CHK_MSG = true;
                    }
                    RX_Beep(Buzzer, frq, timeOn);
                    Show_frm_Srv();
                }
#endif
                if (CMD_debug == true)
                {
                    Serial.println("Frame is TCP / Block to IS / DUP count/ LCD");
                }

                for (int y = 0; y <= buffsize; y++) //-----------------Clear KISS Frame------------------//
                {
                    KISS[y] = 0;
                }
                return;
            }
            else
            {
                TLM_R++;
            }
            S_INFO = ':' + S_INFO;

            if (S_Head.indexOf('>' > 0))
            {
                BUF_display = S_Head + S_INFO;

                ////CHECK SAT Source call sign
                if (S_Head.indexOf(",ARISS") > 0 and S_Head.indexOf("*") < 0)
                {
                    NOGATE = true;
                }
                if (BUF_display.indexOf("YB0X") == 0 or BUF_display.indexOf("RS0ISS") == 0 or
                    BUF_display.indexOf("PSAT") == 0 or BUF_display.indexOf("PCSAT") == 0 or
                    BUF_display.indexOf("USNAP1-1") == 0 or
                    BUF_display.indexOf("W3ADO") == 0)
                {
                    NOGATE = false;
                }

                if (!NOGATE && client.connected())
                {
                    client.println(BUF_display);
                    if (CMD_debug == true)
                    {
                        Serial.println("Send data To IS --> OK");
                    }
                }
                else
                {
                    if (CMD_debug == true)
                    {
                        Serial.println("Send data To IS --> Block");
                    }
                }
                if (CMD_debug == true)
                {
                    Serial.println("KISS Decode--> ");
                }
                Serial.println(NTP_DATETIME_sec + " fm TNC | " + S_Head + S_INFO);
                int Pos_Call = BUF_display.indexOf(">");
                String RxCall = BUF_display.substring(0, Pos_Call);

                //*********SAT MODE*******************//
                if (BUF_display.indexOf("RS0ISS*") > 0 && !Inet)
                {
                    BUF_via = " via RS0ISS ";
                    BUFF = ":Heard    :" + RxCall + " UR RPRT 599" + BUF_via;
                    SendAPRS("ARISS,ARISS");
#ifdef LINE_Notify
#ifdef LINE_SAT_RCV
                    if (Read_PWR == 0)
                    {
                        Serial.print("> Line notify [" + String(MyCallSign) + "] via SAT:" + BUF_display);
                        Line_Notify("[" + String(MyCallSign) + "] via SAT:" + BUF_display);
                    }
#endif
#endif
                }
                else if (BUF_display.indexOf("YB0X-1*") > 0 && !Inet)
                {
                    BUF_via = " via YB0X-1 ";
                    BUFF = ":Heard    :" + RxCall + " UR RPRT 599" + BUF_via;
                    SendAPRS("ARISS,ARISS");
#ifdef LINE_Notify
#ifdef LINE_SAT_RCV
                    if (Read_PWR == 0)
                    {
                        Serial.print("> Line notify [" + String(MyCallSign) + "] via SAT:" + BUF_display);
                        Line_Notify("[" + String(MyCallSign) + "] via SAT:" + BUF_display);
                    }
#endif
#endif
                }
                else if (BUF_display.indexOf("PSAT2*") > 0 && !Inet)
                {
                    BUF_via = " via PSAT2 ";
                    BUFF = ":Heard    :" + RxCall + " UR RPRT 599" + BUF_via;
                    SendAPRS("ARISS,ARISS");
#ifdef LINE_Notify
#ifdef LINE_SAT_RCV
                    if (Read_PWR == 0)
                    {
                        Serial.print("> Line notify [" + String(MyCallSign) + "] via SAT:" + BUF_display);
                        Line_Notify("[" + String(MyCallSign) + "] via SAT:" + BUF_display);
                    }
#endif
#endif
                }
                else if (BUF_display.indexOf("USNAP1-1*") > 0 && !Inet)
                {
                    BUF_via = " via BRICSAT ";
                    BUFF = ":Heard    :" + RxCall + " UR RPRT 599" + BUF_via;
                    SendAPRS("ARISS,ARISS");
#ifdef LINE_Notify
#ifdef LINE_SAT_RCV
                    if (Read_PWR == 0)
                    {
                        Serial.print("> Line notify [" + String(MyCallSign) + "] via SAT:" + BUF_display);
                        Line_Notify("[" + String(MyCallSign) + "] via SAT:" + BUF_display);
                    }
#endif
#endif
                }
                else if (BUF_display.indexOf("PSAT*") > 0 && !Inet)
                {
                    BUF_via = " via PSAT ";
                    BUFF = ":Heard    :" + RxCall + " UR RPRT 599" + BUF_via;
                    SendAPRS("ARISS,ARISS");
#ifdef LINE_Notify
#ifdef LINE_SAT_RCV
                    if (Read_PWR == 0)
                    {
                        Serial.print("> Line notify [" + String(MyCallSign) + "] via SAT:" + BUF_display);
                        Line_Notify("[" + String(MyCallSign) + "] via SAT:" + BUF_display);
                    }
#endif
#endif
                }
                else if (BUF_display.indexOf("W3ADO-1*") > 0 && !Inet)
                {
                    BUF_via = " via W3ADO-1 ";
                    BUFF = ":Heard    :" + RxCall + " UR RPRT 599" + BUF_via;
                    SendAPRS("ARISS,ARISS");
#ifdef LINE_Notify
#ifdef LINE_SAT_RCV
                    if (Read_PWR == 0)
                    {
                        Serial.print("> Line notify [" + String(MyCallSign) + "] via SAT:" + BUF_display);
                        Line_Notify("[" + String(MyCallSign) + "] via SAT:" + BUF_display);
                    }
#endif
#endif
                }
                else
                {
                    BUF_via = " ";
                }

                if (BUF_display.indexOf("YB0X") == 0 or BUF_display.indexOf("RS0ISS") == 0 or BUF_display.indexOf("PSAT") == 0 or
                    BUF_display.indexOf("PCSAT") == 0 or BUF_display.indexOf("ARISS") == 0 or BUF_display.indexOf("W3ADO") == 0 or BUF_display.indexOf("USNAP") == 0)
                {
                    BUFF = String(Beacon) + String(comment);
                    SendAPRS("ARISS,ARISS");
#ifdef LINE_Notify
#ifdef LINE_PKT_SAT_RCV
                    if (Read_PWR == 0)
                    {
                        Serial.print("> Line notify [" + String(MyCallSign) + "] via SAT:" + BUF_display);
                        Line_Notify("[" + String(MyCallSign) + "] RX:" + BUF_display);
                    }
#endif
#endif
                }
                //************************************//

#ifdef OLED

                Last_Packet[4] = Last_Packet[3];
                Last_Packet[3] = Last_Packet[2];
                Last_Packet[2] = Last_Packet[1];
                Last_Packet[1] = Last_Packet[0];
#ifdef NTP_Serv
                Last_Packet[0] = NTP_DATETIME_sec + " " + BUF_display;
#else
                Last_Packet[0] = BUF_display;
#endif

                String Mypkt = String(SOURCE) + KISS_SSID(KISS[15]);
                Inet = false;
                Rx_cnt++;
                Time_RX = NTP_TIME; //17.45   8/03/19   HS6TUX

                if (Mypkt.indexOf(String(MyCallSign)) == 0 and Page == 2)
                {
                    //not show Display
                }
                else if (Page == 3 or Page == 4 or Page == 5 or Page == 6 or NOGATE == true)
                {
                } //NOGATE==true 3.1d fix dup show display Rx via IGATE
                else
                {
                    if (BUF_display.indexOf("::") > 0)
                    {
                        CHK_MSG = true;
                    }
                    RX_Beep(Buzzer, frq, timeOn);
                    Show_frm_Srv();
                }

#endif
                if (CMD_debug == true)
                {
                    Serial.println("KISS Dec Display-->OK");
                    Serial.println("<---------------------->");
                }
                if (serverClients[0] && serverClients[0].connected())
                {
                    serverClients[0].print(S_Head);
                    serverClients[0].println(S_INFO);
                    serverClients[0].println("");
                }

                if ((CHK_DIGI == true) && (DigiDelay > 0))
                {
                    DIGI_ON = true;
                    timedigi = millis();
                }
                else
                {
                    for (int y = 0; y <= buffsize; y++) //-----------------Clear KISS Frame------------------//
                    {
                        KISS[y] = 0;
                    }
                }
            }
            else
            {
                for (int y = 0; y <= buffsize; y++) //-----------------Clear KISS Frame------------------//
                {
                    KISS[y] = 0;
                }
            }
        }
    }

    //--------------------------------------------------------------------------------------------------------------//
    if (DIGI_ON == true)
    {
        int ToDelay = 0;
        if (DigiDelay == 1)
        {
            ToDelay = 0;
        }
        else
        {
            ToDelay = DigiDelay * 1000;
        }
        if (millis() - timedigi > ToDelay)
        {
            if (CMD_debug == true)
            {
                Serial.print(F("Free RAM: "));
                Serial.print(ESP.getFreeHeap());
                Serial.println("DIGI--> OK");
                Serial.println("<---------------------->");
            }
            SendDIGI();
            DIGI_ON = false;
            Dg_cnt++;

            for (int y = 0; y <= buffsize; y++) //-----------------Clear KISS Frame------------------//
            {
                KISS[y] = 0;
            }
#ifdef OLED
            DigiP = true;
            TX_Beep(Buzzer, frq, timeOn);
            Show_frm_Srv();
#endif
        }
    }
    //---------------------switch buttom---------------------------------------
    if (digitalRead(Tx_Now) == 0 and Last_buttom == false)
    {
        KEY_Beep(Buzzer, frq, timeOn); ////************HS6TUX-16.00-23/07/19
        delay(400);
        if (digitalRead(Tx_Now) == 1)
        {
            Serial.println("> Switch Press short");
            Screen_page();
            page_Beep(Buzzer, frq, timeOn);
        } ////************HS6TUX-16.00-23/07/19
        else
        {
            Serial.println("> Switch Press long");
            Last_buttom = true;             //Page=8;Screen_page();
            page_Beep(Buzzer, frq, timeOn); ////************HS6TUX-16.00-23/07/19

            if (Page == 0)
            {
                //Serial.println("> Switch Press long");Last_buttom=true;numRate=Time_Status+1;//เชคว่ากดสั้น คือการเปลี่ยนหน้าจอ // ถ้ากดค้าง คือสั่งให้ส่งบีค่อน
                B_B = true;
                if (CMD_config == false)
                {
                    Beacon_Beacon = true;
                    SendBeacon();
                    Beacon_Beacon = false;
                }
            }
            if (Page == 1)
            {
            }
            if (Page == 2)
            {
            }
            if (Page == 9)
            {
                if (Read_PWR == 1)
                {
                    Serial.printf("> AP OFF / Please press Button for switch ON \r\n");
                    EEPROM.write(505, 0);
                    EEPROM.commit();
                    delay(50); //บันทึก ค่าใหม่ลงไป เป็น 0  แสดงว่าสั่งให้OFF
                    display.clear();
                    display.setFont(ArialMT_Plain_24);
                    display.setTextAlignment(TEXT_ALIGN_CENTER);
                    display.drawString(64, 20, "AP OFF"); //ให้แสดงผลที่ OLED ว่า OFF
                    display.display();
                    Serial.printf("> WiFi Stop");
                    WiFi.softAPdisconnect(true);
                    Read_PWR = 0;
                }
                else
                {
                    Serial.printf("> AP ON / Please press Button for switch OFF\r\n");
                    EEPROM.write(505, 1);
                    EEPROM.commit();
                    delay(50); //บันทึก ค่าใหม่ลงไป เป็น 1  แสดงว่าสั่งให้ON
                    display.clear();
                    display.setFont(ArialMT_Plain_24);
                    display.setTextAlignment(TEXT_ALIGN_CENTER);
                    display.drawString(64, 20, "AP ON"); //ให้แสดงผลที่ OLED ว่า ON
                    display.display();

                    if (MyCallSign[0] > 64)
                    {
                        Serial.printf("> Starting AP SSID %s at Channel ?\r\n", MyCallSign, password_s);
                        // WiFi.softAP(MyCallSign, password_s,5,false);
                        WiFi.softAP(MyCallSign, password_s, 1, false);
                    }
                    else
                    {
                        Serial.printf("> Starting AP WiFi SSID NOCALL at Channel ?\r\n", MyCallSign, password_s);
                        WiFi.softAP(ssid_s, password_s, 1, false);
                    } //ใช้ Callsign ของผู้ใช้มาตั้งเป็นชื่อ SSID Wifi ถ้าไม่มีคอลซายหรือยังไม่เคยตั้งค่า ให้ใช้ NOCALL

                    Read_PWR = 1;
                }
                delay(1000);
            }

            if (Page == 10)
            {

                if (State_IGATE == 0)
                { //ถ้าเคยตั้งเป็น OFF
                    Serial.printf("> IGATE ON / Please press Button for switch OFF\r\n");
                    delay(50);
                    EEPROM.write(511, 1);
                    EEPROM.commit();
                    delay(50); //บันทึก ค่าใหม่ลงไป เป็น 1  แสดงว่าสั่งให้ON
                    display.clear();
                    display.setFont(ArialMT_Plain_24);
                    display.setTextAlignment(TEXT_ALIGN_CENTER);
                    display.drawString(64, 20, "IGATE ON"); //ให้แสดงผลที่ OLED ว่า ON
                    display.display();
                    IGATE_ON = true;
                    //IGON_Beep(Buzzer,frq,timeOn);      ////************HS6TUX-16.00-23/07/19
                    State_IGATE == 1;
                }
                else
                { //ถ้าเคยตั้งเป็น ON
                    Serial.printf("> IGATE OFF / Please press Button for switch ON\r\n");
                    EEPROM.write(511, 0);
                    EEPROM.commit();
                    delay(50); //บันทึก ค่าใหม่ลงไป เป็น 0  แสดงว่าสั่งให้OFF
                    display.clear();
                    display.setFont(ArialMT_Plain_24);
                    display.setTextAlignment(TEXT_ALIGN_CENTER);
                    display.drawString(64, 20, "IGATEOFF"); //ให้แสดงผลที่ OLED ว่า OFF
                    display.display();
                    IGATE_ON = false;
                    client.stop();
                    State_IGATE == 0;
                    //IGOFF_Beep(Buzzer,frq,timeOn);      ////************HS6TUX-16.00-23/07/19
                }

                if (local_ip[0] != 0 && IGATE_ON == true)
                {
                    WiFi.config(local_ip, gateway, subnet, dnsip); //ตั้ง IP / Gateway / Subnet กรณี ไม่ใช้ DHCP  //HS3LSE 23.47  20/10/18
                }

                delay(1000);
            }
            if (Page == 5)
            {
            }
        }
    }
    if (digitalRead(Tx_Now) == 1 and Last_buttom == true)
    {
        Last_buttom = false;
    }
    //---------------------------------------------------------------------------

    if ((millis() - previousMillis) > 1000)
    {
        Run_Time_Sec++;
        Run_Time_SWAP_A++;
        previousMillis = millis();

#ifdef NTP_Serv
        if (WiFi.status() == WL_CONNECTED)
        {
            if (String(year()).indexOf("1970") >= 0)
            {
                if (IGATE_ON == true)
                {
                    NTP.setPollingInterval(20);
                    Serial.println("> NTP Start every 20 sec");
                }
            }
            else
            {
                if (NTP.getPollingInterval() != 3900)
                {
                    NTP.setPollingInterval(3900);
                    NTP.stop();
                    Serial.println("> NTP Sync , Stop");
                }

                //*********************** Code for Loc Time **************************//
                time_t utc, local;
                int localHour;
                int localDay;
                int localMonth;
                int localYear;
                int localweekday;
                utc = now();
                local = utc + 7 * 60 * 60; // Local time 8 hours behind UTC
                localHour = hour(local);
                localDay = day(local);
                localweekday = weekday(local);
                localYear = year(local);
                localMonth = month(local);
                localYear = year(local);

                String DD = "", MM = "";
                if (localweekday == 1)
                {
                    DD = "Sun";
                }
                else if (localweekday == 2)
                {
                    DD = "Mon";
                }
                else if (localweekday == 3)
                {
                    DD = "Tue";
                }
                else if (localweekday == 4)
                {
                    DD = "Wed";
                }
                else if (localweekday == 5)
                {
                    DD = "Thu";
                }
                else if (localweekday == 6)
                {
                    DD = "Fri";
                }
                else if (localweekday == 7)
                {
                    DD = "Sat";
                }
                if (localMonth == 1)
                {
                    MM = "Jan";
                }
                else if (localMonth == 2)
                {
                    MM = "Feb";
                }
                else if (localMonth == 3)
                {
                    MM = "Mar";
                }
                else if (localMonth == 4)
                {
                    MM = "Apr";
                }
                else if (localMonth == 5)
                {
                    MM = "May";
                }
                else if (localMonth == 6)
                {
                    MM = "Jun";
                }
                else if (localMonth == 7)
                {
                    MM = "Jul";
                }
                else if (localMonth == 8)
                {
                    MM = "Aug";
                }
                else if (localMonth == 9)
                {
                    MM = "Sep";
                }
                else if (localMonth == 10)
                {
                    MM = "Oct";
                }
                else if (localMonth == 11)
                {
                    MM = "Nov";
                }
                else if (localMonth == 12)
                {
                    MM = "Dec";
                }
                String NTP_HH = "";
                String NTP_MM = "";
                String NTP_SS = "";
                String NTP_MN = "";
                String NTP_DT = "";
                if (localHour < 10)
                {
                    NTP_HH = "0" + String(localHour);
                }
                else
                {
                    NTP_HH = String(localHour);
                }
                if (minute() < 10)
                {
                    NTP_MM = "0" + String(minute());
                }
                else
                {
                    NTP_MM = String(minute());
                }
                if (second() < 10)
                {
                    NTP_SS = "0" + String(second());
                }
                else
                {
                    NTP_SS = String(second());
                }
                if (localMonth < 10)
                {
                    NTP_MN = "0" + String(localMonth);
                }
                else
                {
                    NTP_MN = String(localMonth);
                }
                if (day() < 10)
                {
                    NTP_DT = "0" + String(day());
                }
                else
                {
                    NTP_DT = String(day());
                }
                //NTP_DATETIME = String(year()) + "-" + NTP_MN + "-" + NTP_DT + " " + NTP_HH + ":" + NTP_MM ;//
                NTP_DATETIME = NTP_DT + "-" + NTP_MN + "-" + String(year()) + " " + NTP_HH + ":" + NTP_MM;
                NTP_DATETIME_sec = NTP_DT + "-" + NTP_MN + "-" + String(year()) + " " + NTP_HH + ":" + NTP_MM + ":" + NTP_SS;
                NTP_TIME = NTP_HH + ":" + NTP_MM + ":" + NTP_SS; //17.45   8/03/19   HS6TUX
                //***********************************************************************************//

                //*********************** Code for UTC Time use for WX **************************//
                String UTC_MN = "";
                String UTC_DT = "";
                String UTC_HH = "";
                if (hour() < 10)
                {
                    UTC_HH = "0" + String(hour());
                }
                else
                {
                    UTC_HH = String(hour());
                }
                if (month() < 10)
                {
                    UTC_MN = "0" + String(month());
                }
                else
                {
                    UTC_MN = String(month());
                }
                if (day() < 10)
                {
                    UTC_DT = "0" + String(day());
                }
                else
                {
                    UTC_DT = String(day());
                }
                NTP_MDHM = UTC_MN + UTC_DT + UTC_HH + NTP_MM; // for WX UTC
                //***********************************************************************************//

                if (Page == 0 and BUF_display != "")
                {

                    if (Run_Time_SWAP_A < SWAP_delay)
                    {
                        Run_Time_SWAP_B = 0;
                        display.clear();
                        /*display.setTextAlignment(TEXT_ALIGN_CENTER);///////////////////////////////////////////////17.45   8/03/19   HS6TUX
display.setFont(ArialMT_Plain_16);
display.drawString(64, 0, DD );
display.setFont(ArialMT_Plain_16);
display.drawString(64, 27,String(day()) + " " + MM + " " + String(year()) );
display.drawString(64, 45,NTP_HH + ":" + NTP_MM + ":" + NTP_SS);*/
                        ////////////////////////
                        ///////////////////////////////////////////////  //17.45   8/03/19   HS6TUX
                        //long UPT = millis();
                        int UPTIME_H = previousMillis / 3600000;
                        int UPTIME_M = (previousMillis % 3600000) / 60000;
                        String U_HH = String(UPTIME_H);
                        String U_MM = String(UPTIME_M);
                        if (UPTIME_H < 10)
                        {
                            U_HH = '0' + String(UPTIME_H);
                        }
                        if (UPTIME_M < 10)
                        {
                            U_MM = '0' + String(UPTIME_M);
                        }
                        ////////////////////////////////////////////////  //17.45   8/03/19   HS6TUX

                        display.drawHorizontalLine(X_hor, Y_hor, 128); //17.45   8/03/19   HS6TUX
                        display.setTextAlignment(TEXT_ALIGN_LEFT);
                        display.setFont(ArialMT_Plain_16);
                        display.drawString(0, 0, NTP_HH + ":" + NTP_MM + ":" + NTP_SS);
                        display.drawString(68, 0, DD);
                        display.setFont(ArialMT_Plain_10);
                        display.drawString(0, 14, String(day()) + " " + MM + " " + String(year()));
                        display.drawString(0, 25, "LastCall:" + String(Last_Call)); //+" >"+String(Last_KM)+"km");
                        display.drawString(0, 40, "RX: " + String(Rx_cnt) + "  DIG: " + String(Dg_cnt) + "  TX: " + String(Tx_cnt));

                        display.drawString(0, 54, "DX:" + Dx_Callx + String(Dx_km) + "km " + String(Dx_br) + "°");
                        display.setTextAlignment(TEXT_ALIGN_RIGHT);
                        display.setFont(ArialMT_Plain_10);
                        //display.drawString(127, 14,String(MyCallSign));
                        display.drawString(128, 14, "UPTime:" + String(U_HH) + ':' + String(U_MM));
                        /////// WIFI ///////////
                        /*
if (WiFi.RSSI()>-67)
{display.drawXbm(98,0, 15, 10, S_wifi3);}
else if (WiFi.RSSI()>-80)
{display.drawXbm(98,0, 15, 10, S_wifi2);}
else
{display.drawXbm(98,0, 15, 10, S_wifi1);}
display.drawString(128, 0, String(WiFi.RSSI()));
*/
                        //////////////////////////////////////////////////////////////////////////////////////////17.45   8/03/19   HS6TUX
                        display.display();
                    }
                    else
                    {
                        if (Run_Time_SWAP_A == SWAP_delay)
                        {
                            Show_frm_Srv();
                        }
                        Run_Time_SWAP_B++;
                        if (Run_Time_SWAP_B > SWAP_delay)
                        {
                            Run_Time_SWAP_A = 0;
                        }
                    }
                }
            }
        }
#endif
        //-----------------------APRSIS Connect-----------------------//
        if (IGATE_ON == true && Config_Interrupt == false)
        {
            if (WiFi.status() != WL_CONNECTED)
            {
                digitalWrite(LED_online, HIGH);
            }
            else
            {
                digitalWrite(LED_online, LOW);
            }
            if (CMD_config == false)
            {
                if (WiFi.status() != WL_CONNECTED)
                {
                    if (millis() - timeIS > 10000)
                    {
                        wifistart();
                        timeIS = millis();
                        Loop_connect_IS++;
                    }
                }
            } //-----WIFI Connect----//
            if (!client.connected() && WiFi.status() == WL_CONNECTED)
            {
                digitalWrite(LED_online, HIGH);
                if (millis() - timeIS > 10000)
                {
                    client.stop();
                    APRSIS_connect();
                    timeIS = millis();
                    Loop_connect_IS++;
                } //-----APRS-IS notConnect----//
            }
            if (Loop_connect_IS > 40)
            {
                ESP.restart();
            }
        }
        else if (IGATE_ON == false && Config_Interrupt == false && Target_wifi_Start == true)
        {
            if (WiFi.status() != WL_CONNECTED)
            {
                digitalWrite(LED_online, HIGH);
            }
            else
            {
                digitalWrite(LED_online, LOW);
            }
            if (CMD_config == false)
            {
                if (WiFi.status() != WL_CONNECTED)
                {
                    if (millis() - timeIS > 10000)
                    {
                        wifistart();
                        timeIS = millis();
                        Loop_connect_IS++;
                    }
                }
            } //-----WIFI Connect----//
            if (!client.connected() && WiFi.status() == WL_CONNECTED)
            {
                digitalWrite(LED_online, HIGH);
            }
        }

        //------------------------------------------------------ TIME TO SEND BEACON----------------------------------//
        if (Run_Time_Sec > ((interval * 60) + 30))
        {
            B_B = false;
            B_W = false;
            B_S = false;
            Run_Time_Sec = 20;
            if (CMD_config == false)
            {
                Beacon_Telemetry = true;
                SendBeacon();
                Beacon_Telemetry = false;
            }
        }

        else if (Run_Time_Sec > ((interval * 60) + 10) && B_S == false)
        {
            B_S = true;
            if (CMD_config == false)
            {
                Beacon_Status = true;
                SendBeacon();
                Beacon_Status = false;
            }
        }

        else if (Run_Time_Sec > ((interval * 60) + 5) && B_W == false)
        {
            B_W = true;
            if (CMD_config == false)
            {
                Beacon_Wx = true;
                SendBeacon();
                Beacon_Wx = false;
            }
        }

        else if (Run_Time_Sec > (interval * 60) && B_B == false)
        {
            B_B = true;
            if (CMD_config == false)
            {
                Beacon_Beacon = true;
                SendBeacon();
                Beacon_Beacon = false;
            }
        }
        //--------------------------------------------------------------------------------------------------------------//
    }

} // Close LOOP
//---------------------------------------------------------------------------//

void Decodekiss(char *tonew, byte *org, int strwant, int totalwant)
{
    int str = 0, n = 0;
    for (; n < totalwant; strwant++)
    {
        if (org[strwant] != 0x40)
        {
            tonew[str] = org[strwant] >> 1;
            str++;
        }
        n++;
    }
    tonew[str] = '\0';
}
String KISS_SSID(int SSI)
{
    if ((SSI & 0x7F) >> 1 == 48)
    {
        K_SSID = "";
    }
    else if ((SSI & 0x7F) >> 1 == 49)
    {
        K_SSID = "-1";
    }
    else if ((SSI & 0x7F) >> 1 == 50)
    {
        K_SSID = "-2";
    }
    else if ((SSI & 0x7F) >> 1 == 51)
    {
        K_SSID = "-3";
    }
    else if ((SSI & 0x7F) >> 1 == 52)
    {
        K_SSID = "-4";
    }
    else if ((SSI & 0x7F) >> 1 == 53)
    {
        K_SSID = "-5";
    }
    else if ((SSI & 0x7F) >> 1 == 54)
    {
        K_SSID = "-6";
    }
    else if ((SSI & 0x7F) >> 1 == 55)
    {
        K_SSID = "-7";
    }
    else if ((SSI & 0x7F) >> 1 == 56)
    {
        K_SSID = "-8";
    }
    else if ((SSI & 0x7F) >> 1 == 57)
    {
        K_SSID = "-9";
    }
    else if ((SSI & 0x7F) >> 1 == 58)
    {
        K_SSID = "-10";
    }
    else if ((SSI & 0x7F) >> 1 == 59)
    {
        K_SSID = "-11";
    }
    else if ((SSI & 0x7F) >> 1 == 60)
    {
        K_SSID = "-12";
    }
    else if ((SSI & 0x7F) >> 1 == 61)
    {
        K_SSID = "-13";
    }
    else if ((SSI & 0x7F) >> 1 == 62)
    {
        K_SSID = "-14";
    }
    else if ((SSI & 0x7F) >> 1 == 63)
    {
        K_SSID = "-15";
    }
    else
    {
        K_SSID = '-' + char((SSI & 0x7F) >> 1);
    }
    return K_SSID;
}
//************************************************************************************************//
void SendAPRS(String SEND_PATH)
{ //ฟังก์ชั่น ส่งค่าไปให้ TNC ออกาอากศ
    // TX_Beep(Buzzer,frq,timeOn);//สั่งให้มีเสียงปี๊บแสดงสถานะส่ง ปิ๊ปๆ
    SendKISSRF(SEND_PATH); //ส่ง KISS ข้อมูล ผู้รับ ผู้ส่ง Path และข้อมูลไปที่ฟังก์ชั่น SendKISS() ให้ส่งให้อีกทีนึง
}
//************************************************************************************************//
void SendKISS() //ใช้สำหรับส่ง KISS + INFO ภานในฟังก์ชั่น ใช้สำหรับ ส่ง APRS-IS to RF
{
#ifdef To_RF
    char RAWTNC[150] = "INFO";
    int k = 0, n = 0;

    RAWTNC[0] = 0xC0;
    RAWTNC[1] = 0x00;
    for (int pth = 0; pth < 7; pth++)
    {
        RAWTNC[pth + 2] = MYDEST[pth] << 1;
    }

    while ((MyCallSign[k] != 0) && (MyCallSign[k] != '-') && k <= 7)
    {
        RAWTNC[n + 9] = MyCallSign[k] << 1;
        k++;
        n++;
    }
    for (int m = 0; m < (6 - n); m++)
    {
        RAWTNC[9 + n + m] = 0x40; //----space
    }
    RAWTNC[15] = '0' << 1;
    if (MyCallSign[k] == '-')
    {
        k++;
        if (MyCallSign[k] == '0')
        {
            RAWTNC[15] = 0x30 << 1;
        }
        if (MyCallSign[k] == '1')
        {
            RAWTNC[15] = 0x31 << 1;
        }
        if (MyCallSign[k] == '2')
        {
            RAWTNC[15] = 0x32 << 1;
        }
        if (MyCallSign[k] == '3')
        {
            RAWTNC[15] = 0x33 << 1;
        }
        if (MyCallSign[k] == '4')
        {
            RAWTNC[15] = 0x34 << 1;
        }
        if (MyCallSign[k] == '5')
        {
            RAWTNC[15] = 0x35 << 1;
        }
        if (MyCallSign[k] == '6')
        {
            RAWTNC[15] = 0x36 << 1;
        }
        if (MyCallSign[k] == '7')
        {
            RAWTNC[15] = 0x37 << 1;
        }
        if (MyCallSign[k] == '8')
        {
            RAWTNC[15] = 0x38 << 1;
        }
        if (MyCallSign[k] == '9')
        {
            RAWTNC[15] = 0x39 << 1;
        }
        if (MyCallSign[k + 1] == '0')
        {
            RAWTNC[15] = 0x3A << 1;
        }
        if (MyCallSign[k + 1] == '1')
        {
            RAWTNC[15] = 0x3B << 1;
        }
        if (MyCallSign[k + 1] == '2')
        {
            RAWTNC[15] = 0x3C << 1;
        }
        if (MyCallSign[k + 1] == '3')
        {
            RAWTNC[15] = 0x3D << 1;
        }
        if (MyCallSign[k + 1] == '4')
        {
            RAWTNC[15] = 0x3E << 1;
        }
        if (MyCallSign[k + 1] == '5')
        {
            RAWTNC[15] = 0x3F << 1;
        }
    }
    if (VIAPATH[0] == 0)
    {
        RAWTNC[15] = RAWTNC[15] | 0x01;
    }

    k = 0;
    n = 0;
    if (VIAPATH[0] != 0)
    {
        int numPath = 0;
        for (int i = 0; VIAPATH[i] != 0; i++)
        {
            if (VIAPATH[i] == ',')
            {
                numPath = i;
            }
        }
        while ((VIAPATH[k] != 0) && (VIAPATH[k] != '-') && VIAPATH[k] != ',')
        {
            RAWTNC[n + 16] = VIAPATH[k] << 1;
            k++;
            n++;
        }
        for (int m = 0; m < (6 - n); m++)
        {
            RAWTNC[16 + n + m] = 0x40; //----space
        }
        RAWTNC[22] = (0x30 << 1);

        if (VIAPATH[k] == '-')
        {
            k++;
            if (VIAPATH[k] == '0')
            {
                RAWTNC[22] = (0x30 << 1);
            }
            if (VIAPATH[k] == '1')
            {
                RAWTNC[22] = (0x31 << 1);
            }
            if (VIAPATH[k] == '2')
            {
                RAWTNC[22] = (0x32 << 1);
            }
            if (VIAPATH[k] == '3')
            {
                RAWTNC[22] = (0x33 << 1);
            }
            if (VIAPATH[k] == '4')
            {
                RAWTNC[22] = (0x34 << 1);
            }
            if (VIAPATH[k] == '5')
            {
                RAWTNC[22] = (0x35 << 1);
            }
            if (VIAPATH[k] == '6')
            {
                RAWTNC[22] = (0x36 << 1);
            }
            if (VIAPATH[k] == '7')
            {
                RAWTNC[22] = (0x37 << 1);
            }
            if (VIAPATH[k] == '8')
            {
                RAWTNC[22] = (0x38 << 1);
            }
            if (VIAPATH[k] == '9')
            {
                RAWTNC[22] = (0x39 << 1);
            }
            if (VIAPATH[k + 1] == '0')
            {
                RAWTNC[22] = (0x3A << 1);
            }
            if (VIAPATH[k + 1] == '1')
            {
                RAWTNC[22] = (0x3B << 1);
            }
            if (VIAPATH[k + 1] == '2')
            {
                RAWTNC[22] = (0x3C << 1);
            }
            if (VIAPATH[k + 1] == '3')
            {
                RAWTNC[22] = (0x3D << 1);
            }
            if (VIAPATH[k + 1] == '4')
            {
                RAWTNC[22] = (0x3E << 1);
            }
            if (VIAPATH[k + 1] == '5')
            {
                RAWTNC[22] = (0x3F << 1);
            }
        }
        if (numPath == 0)
        {
            RAWTNC[22] = RAWTNC[22] | 0x01;
            RAWTNC[23] = 0x03;
            RAWTNC[24] = 0xF0;
            //for(int s=0;s<=24;s++){TNC_Serial.write(RAWTNC[s]);}
            TNC_Serial.write(RAWTNC, 25);
            TNC_Serial.write(0x7D); // คือ } ออกจากไอเกต
            TNC_Serial.print(BUFF);
            BUFF = "";
            TNC_Serial.write(0xC0); //ปิดแพกเก็ต
            RAWTNC[0] = 0;
        }
        else
        {
            n = 0;
            while ((VIAPATH[numPath + 1] != 0) && (VIAPATH[numPath + 1] != '-') && VIAPATH[numPath + 1] != ',')
            {
                RAWTNC[n + 23] = VIAPATH[numPath + 1] << 1;
                numPath++;
                n++;
            }
            for (int m = 0; m < (6 - n); m++)
            {
                RAWTNC[23 + n + m] = 0x40; //----space
            }
            RAWTNC[29] = (0x30 << 1);
            k = numPath + 1;
            if (VIAPATH[k] == '-')
            {
                k++;
                if (VIAPATH[k] == '0')
                {
                    RAWTNC[29] = (0x30 << 1);
                }
                if (VIAPATH[k] == '1')
                {
                    RAWTNC[29] = (0x31 << 1);
                }
                if (VIAPATH[k] == '2')
                {
                    RAWTNC[29] = (0x32 << 1);
                }
                if (VIAPATH[k] == '3')
                {
                    RAWTNC[29] = (0x33 << 1);
                }
                if (VIAPATH[k] == '4')
                {
                    RAWTNC[29] = (0x34 << 1);
                }
                if (VIAPATH[k] == '5')
                {
                    RAWTNC[29] = (0x35 << 1);
                }
                if (VIAPATH[k] == '6')
                {
                    RAWTNC[29] = (0x36 << 1);
                }
                if (VIAPATH[k] == '7')
                {
                    RAWTNC[29] = (0x37 << 1);
                }
                if (VIAPATH[k] == '8')
                {
                    RAWTNC[29] = (0x38 << 1);
                }
                if (VIAPATH[k] == '9')
                {
                    RAWTNC[29] = (0x39 << 1);
                }
                if (VIAPATH[k + 1] == '0')
                {
                    RAWTNC[29] = (0x3A << 1);
                }
                if (VIAPATH[k + 1] == '1')
                {
                    RAWTNC[29] = (0x3B << 1);
                }
                if (VIAPATH[k + 1] == '2')
                {
                    RAWTNC[29] = (0x3C << 1);
                }
                if (VIAPATH[k + 1] == '3')
                {
                    RAWTNC[29] = (0x3D << 1);
                }
                if (VIAPATH[k + 1] == '4')
                {
                    RAWTNC[29] = (0x3E << 1);
                }
                if (VIAPATH[k + 1] == '5')
                {
                    RAWTNC[29] = (0x3F << 1);
                }
            }
            RAWTNC[29] = RAWTNC[29] | 0x01;
            RAWTNC[30] = 0x03;
            RAWTNC[31] = 0xF0;
            TNC_Serial.write(RAWTNC, 32);
            TNC_Serial.write(0x7D); // คือ } ออกจากไอเกต
            TNC_Serial.print(BUFF);
            BUFF = "";
            TNC_Serial.write(0xC0);
            RAWTNC[0] = 0;
        }
    }
    else
    {
        RAWTNC[16] = 0x03;
        RAWTNC[17] = 0xF0;
        TNC_Serial.write(RAWTNC, 18); //for(int s=0;s<=17;s++){TNC_Serial.write(RAWTNC[s]);}
        TNC_Serial.write(0x7D);
        TNC_Serial.print(BUFF);
        BUFF = "";
        TNC_Serial.write(0xC0);
        RAWTNC[0] = 0;
    }
    RAWTNC[0] = 0;
#endif
}
void SendKISSRF(String SEND_PATH) //สำหรับส่งKISS DEST CALL PATH ไปก่อน ใช้สำหรับ DIGI หรือ Beacon และต้องตามด้วย INFO ทีหลัง
{
#ifdef To_RF
    char VIA_PATH[20] = "";
    char RAWTNC[150] = "INFO";
    int k = 0, n = 0;

    SEND_PATH.toCharArray(VIA_PATH, 20);

    RAWTNC[0] = 0xC0;
    RAWTNC[1] = 0x00;
    for (int pth = 0; pth < 7; pth++)
    {
        RAWTNC[pth + 2] = MYDEST[pth] << 1;
    }
    while ((MyCallSign[k] != 0) && (MyCallSign[k] != '-') && k <= 7)
    {
        RAWTNC[n + 9] = MyCallSign[k] << 1;
        k++;
        n++;
    }
    for (int m = 0; m < (6 - n); m++)
    {
        RAWTNC[9 + n + m] = 0x40; //----space
    }
    RAWTNC[15] = '0' << 1;
    if (MyCallSign[k] == '-')
    {
        k++;
        if (MyCallSign[k] == '0')
        {
            RAWTNC[15] = 0x30 << 1;
        }
        if (MyCallSign[k] == '1')
        {
            RAWTNC[15] = 0x31 << 1;
        }
        if (MyCallSign[k] == '2')
        {
            RAWTNC[15] = 0x32 << 1;
        }
        if (MyCallSign[k] == '3')
        {
            RAWTNC[15] = 0x33 << 1;
        }
        if (MyCallSign[k] == '4')
        {
            RAWTNC[15] = 0x34 << 1;
        }
        if (MyCallSign[k] == '5')
        {
            RAWTNC[15] = 0x35 << 1;
        }
        if (MyCallSign[k] == '6')
        {
            RAWTNC[15] = 0x36 << 1;
        }
        if (MyCallSign[k] == '7')
        {
            RAWTNC[15] = 0x37 << 1;
        }
        if (MyCallSign[k] == '8')
        {
            RAWTNC[15] = 0x38 << 1;
        }
        if (MyCallSign[k] == '9')
        {
            RAWTNC[15] = 0x39 << 1;
        }
        if (MyCallSign[k + 1] == '0')
        {
            RAWTNC[15] = 0x3A << 1;
        }
        if (MyCallSign[k + 1] == '1')
        {
            RAWTNC[15] = 0x3B << 1;
        }
        if (MyCallSign[k + 1] == '2')
        {
            RAWTNC[15] = 0x3C << 1;
        }
        if (MyCallSign[k + 1] == '3')
        {
            RAWTNC[15] = 0x3D << 1;
        }
        if (MyCallSign[k + 1] == '4')
        {
            RAWTNC[15] = 0x3E << 1;
        }
        if (MyCallSign[k + 1] == '5')
        {
            RAWTNC[15] = 0x3F << 1;
        }
    }
    if (VIA_PATH[0] == 0)
    {
        RAWTNC[15] = RAWTNC[15] | 0x01;
    }

    k = 0;
    n = 0;
    if (VIA_PATH[0] != 0)
    {
        int numPath = 0;
        for (int i = 0; VIA_PATH[i] != 0; i++)
        {
            if (VIA_PATH[i] == ',')
            {
                numPath = i;
            }
        }
        while ((VIA_PATH[k] != 0) && (VIA_PATH[k] != '-') && VIA_PATH[k] != ',')
        {
            RAWTNC[n + 16] = VIA_PATH[k] << 1;
            k++;
            n++;
        }
        for (int m = 0; m < (6 - n); m++)
        {
            RAWTNC[16 + n + m] = 0x40; //----space
        }
        RAWTNC[22] = (0x30 << 1);

        if (VIA_PATH[k] == '-')
        {
            k++;
            if (VIA_PATH[k] == '0')
            {
                RAWTNC[22] = (0x30 << 1);
            }
            if (VIA_PATH[k] == '1')
            {
                RAWTNC[22] = (0x31 << 1);
            }
            if (VIA_PATH[k] == '2')
            {
                RAWTNC[22] = (0x32 << 1);
            }
            if (VIA_PATH[k] == '3')
            {
                RAWTNC[22] = (0x33 << 1);
            }
            if (VIA_PATH[k] == '4')
            {
                RAWTNC[22] = (0x34 << 1);
            }
            if (VIA_PATH[k] == '5')
            {
                RAWTNC[22] = (0x35 << 1);
            }
            if (VIA_PATH[k] == '6')
            {
                RAWTNC[22] = (0x36 << 1);
            }
            if (VIA_PATH[k] == '7')
            {
                RAWTNC[22] = (0x37 << 1);
            }
            if (VIA_PATH[k] == '8')
            {
                RAWTNC[22] = (0x38 << 1);
            }
            if (VIA_PATH[k] == '9')
            {
                RAWTNC[22] = (0x39 << 1);
            }
            if (VIA_PATH[k + 1] == '0')
            {
                RAWTNC[22] = (0x3A << 1);
            }
            if (VIA_PATH[k + 1] == '1')
            {
                RAWTNC[22] = (0x3B << 1);
            }
            if (VIA_PATH[k + 1] == '2')
            {
                RAWTNC[22] = (0x3C << 1);
            }
            if (VIA_PATH[k + 1] == '3')
            {
                RAWTNC[22] = (0x3D << 1);
            }
            if (VIA_PATH[k + 1] == '4')
            {
                RAWTNC[22] = (0x3E << 1);
            }
            if (VIA_PATH[k + 1] == '5')
            {
                RAWTNC[22] = (0x3F << 1);
            }
        }
        if (numPath == 0)
        {
            RAWTNC[22] = RAWTNC[22] | 0x01;
            RAWTNC[23] = 0x03;
            RAWTNC[24] = 0xF0;
            TNC_Serial.write(RAWTNC, 25);
            TNC_Serial.print(BUFF);
            BUFF = "";
            TNC_Serial.write(0xC0);
        }
        else
        {
            n = 0;
            while ((VIA_PATH[numPath + 1] != 0) && (VIA_PATH[numPath + 1] != '-') && VIA_PATH[numPath + 1] != ',')
            {
                RAWTNC[n + 23] = VIA_PATH[numPath + 1] << 1;
                numPath++;
                n++;
            }
            for (int m = 0; m < (6 - n); m++)
            {
                RAWTNC[23 + n + m] = 0x40; //----space
            }
            RAWTNC[29] = (0x30 << 1);
            k = numPath + 1;
            if (VIA_PATH[k] == '-')
            {
                k++;
                if (VIA_PATH[k] == '0')
                {
                    RAWTNC[29] = (0x30 << 1);
                }
                if (VIA_PATH[k] == '1')
                {
                    RAWTNC[29] = (0x31 << 1);
                }
                if (VIA_PATH[k] == '2')
                {
                    RAWTNC[29] = (0x32 << 1);
                }
                if (VIA_PATH[k] == '3')
                {
                    RAWTNC[29] = (0x33 << 1);
                }
                if (VIA_PATH[k] == '4')
                {
                    RAWTNC[29] = (0x34 << 1);
                }
                if (VIA_PATH[k] == '5')
                {
                    RAWTNC[29] = (0x35 << 1);
                }
                if (VIA_PATH[k] == '6')
                {
                    RAWTNC[29] = (0x36 << 1);
                }
                if (VIA_PATH[k] == '7')
                {
                    RAWTNC[29] = (0x37 << 1);
                }
                if (VIA_PATH[k] == '8')
                {
                    RAWTNC[29] = (0x38 << 1);
                }
                if (VIA_PATH[k] == '9')
                {
                    RAWTNC[29] = (0x39 << 1);
                }
                if (VIA_PATH[k + 1] == '0')
                {
                    RAWTNC[29] = (0x3A << 1);
                }
                if (VIA_PATH[k + 1] == '1')
                {
                    RAWTNC[29] = (0x3B << 1);
                }
                if (VIA_PATH[k + 1] == '2')
                {
                    RAWTNC[29] = (0x3C << 1);
                }
                if (VIA_PATH[k + 1] == '3')
                {
                    RAWTNC[29] = (0x3D << 1);
                }
                if (VIA_PATH[k + 1] == '4')
                {
                    RAWTNC[29] = (0x3E << 1);
                }
                if (VIA_PATH[k + 1] == '5')
                {
                    RAWTNC[29] = (0x3F << 1);
                }
            }
            RAWTNC[29] = RAWTNC[29] | 0x01;
            RAWTNC[30] = 0x03;
            RAWTNC[31] = 0xF0;
            TNC_Serial.write(RAWTNC, 32);
            TNC_Serial.print(BUFF);
            BUFF = "";
            TNC_Serial.write(0xC0);
        }
    }
    else
    {
        RAWTNC[16] = 0x03;
        RAWTNC[17] = 0xF0;
        TNC_Serial.write(RAWTNC, 18);
        TNC_Serial.print(BUFF);
        BUFF = "";
        TNC_Serial.write(0xC0);
    }
    RAWTNC[0] = 0;
#endif
}

void SendDIGI()
{
#ifdef To_RF
    int k = 0, n = 0;
    char DIGIED[10];
    DIGIKISS[0] = 0xC0;
    DIGIKISS[1] = 0x00;
    for (int s = 0; s < 16; s++)
    {
        TNC_Serial.write(DIGIKISS[s]);
    }
    //-----------Path 1----------------------//
    while ((MyCallSign[k] != 0) && (MyCallSign[k] != '-'))
    {
        DIGIED[n] = MyCallSign[k] << 1;
        k++;
        n++;
    }
    for (int m = 0; m < (6 - n); m++)
    {
        DIGIED[n + m] = 0x40; //----space
    }
    DIGIED[6] = '0' << 1 | 0x80;
    if (MyCallSign[k] == '-')
    {
        k++;
        if (MyCallSign[k] == '0')
        {
            DIGIED[6] = ('0' << 1) | 0x80;
        }
        if (MyCallSign[k] == '1')
        {
            DIGIED[6] = ('1' << 1) | 0x80;
        }
        if (MyCallSign[k] == '2')
        {
            DIGIED[6] = ('2' << 1) | 0x80;
        }
        if (MyCallSign[k] == '3')
        {
            DIGIED[6] = ('3' << 1) | 0x80;
        }
        if (MyCallSign[k] == '4')
        {
            DIGIED[6] = ('4' << 1) | 0x80;
        }
        if (MyCallSign[k] == '5')
        {
            DIGIED[6] = ('5' << 1) | 0x80;
        }
        if (MyCallSign[k] == '6')
        {
            DIGIED[6] = ('6' << 1) | 0x80;
        }
        if (MyCallSign[k] == '7')
        {
            DIGIED[6] = ('7' << 1) | 0x80;
        }
        if (MyCallSign[k] == '8')
        {
            DIGIED[6] = ('8' << 1) | 0x80;
        }
        if (MyCallSign[k] == '9')
        {
            DIGIED[6] = ('9' << 1) | 0x80;
        }
        if (MyCallSign[k + 1] == '0')
        {
            DIGIED[6] = (58 << 1) | 0x80;
        }
        if (MyCallSign[k + 1] == '1')
        {
            DIGIED[6] = (59 << 1) | 0x80;
        }
        if (MyCallSign[k + 1] == '2')
        {
            DIGIED[6] = (60 << 1) | 0x80;
        }
        if (MyCallSign[k + 1] == '3')
        {
            DIGIED[6] = (61 << 1) | 0x80;
        }
        if (MyCallSign[k + 1] == '4')
        {
            DIGIED[6] = (62 << 1) | 0x80;
        }
        if (MyCallSign[k + 1] == '5')
        {
            DIGIED[6] = (63 << 1) | 0x80;
        }
    }
    if (KISS[23] == 0x03)
    {
        DIGIED[6] = DIGIED[6] | 0x01;
    }

    for (int s = 0; s <= 6; s++)
    {
        TNC_Serial.write(DIGIED[s]);
    }
    for (int s = 23; KISS[s] != 192; s++)
    {
        TNC_Serial.write(KISS[s]);
    }
    TNC_Serial.write(192);
    /*
//-----------Path 2----------------------//
while((MyCallSign[k]!=0) && (MyCallSign[k]!='-')){DIGIED[n]=MyCallSign[k]<<1;k++;n++;}
for(int m=0;m<(6-n);m++)
{
DIGIED[n+m]=0x40;//----space
}
DIGIED[6]='0'<<1|0x80;
if(MyCallSign[k]=='-'){
k++;
if(MyCallSign[k]=='1'){DIGIED[6]=('1'<<1)|0x80;}
if(MyCallSign[k]=='2'){DIGIED[6]=('2'<<1)|0x80;}
if(MyCallSign[k]=='3'){DIGIED[6]=('3'<<1)|0x80;}
if(MyCallSign[k]=='4'){DIGIED[6]=('4'<<1)|0x80;}
if(MyCallSign[k]=='5'){DIGIED[6]=('5'<<1)|0x80;}
if(MyCallSign[k]=='6'){DIGIED[6]=('6'<<1)|0x80;}
if(MyCallSign[k]=='7'){DIGIED[6]=('7'<<1)|0x80;}
if(MyCallSign[k]=='8'){DIGIED[6]=('8'<<1)|0x80;}
if(MyCallSign[k]=='9'){DIGIED[6]=('9'<<1)|0x80;}
if(MyCallSign[k+1]=='0'){DIGIED[6]=(58<<1)|0x80;}
if(MyCallSign[k+1]=='1'){DIGIED[6]=(59<<1)|0x80;}
if(MyCallSign[k+1]=='2'){DIGIED[6]=(60<<1)|0x80;}
if(MyCallSign[k+1]=='3'){DIGIED[6]=(61<<1)|0x80;}
if(MyCallSign[k+1]=='4'){DIGIED[6]=(62<<1)|0x80;}
if(MyCallSign[k+1]=='5'){DIGIED[6]=(63<<1)|0x80;}
}
if(KISS[30]==0x03){DIGIED[6]=DIGIED[6]|0x01;}

for(int s=16;s<23;s++)
{TNC_Serial.write(KISS[s]);}
for(int s=0;s<=6;s++)
{TNC_Serial.write(DIGIED[s]);}
for(int s=30;KISS[s]!=192;s++)
{TNC_Serial.write(KISS[s]);}TNC_Serial.write(192);

*/

    KISS[0] = 0;
#endif
}
/*"(---------------------------------------------------------------------)"*/
/*"(-                           Client String for saving memory         -)"*/
/*"(---------------------------------------------------------------------)"*/
void clientString(PGM_P s)
{
    char c;
    while ((c = pgm_read_byte(s++)) != 0)
        client.print(c);
}

//--------------------------------------------------------------------BEACON-----------------------------------------------//
void SendBeacon()
{
    if (String(year()).indexOf("1970") < 0)
    {
        ;
    }
    else
    {
        NTP_MDHM = "00000000";
    }

#ifdef To_BeaconINET
    boolean Rain_state = false;
    String S_Beacon = "";
    char WXBeacon[21];
    float h = 0.0;
    float t = 0.0;
    float f = 0.0;
    float p = 0.0;
    int Cp = 0;
    int Tp = 0;
    int lp = 0;
    int LDR_index = 0;
    String Pretemp = ""; //สำหรับ เติม 0 หน้า องศาฟาเรนไฮ กรณี น้อยกว่า 100 เพื่อให้มี t000 เลข 3 หลัก
    String Prebaro = "";
    String SEND_PATH = String(VIAPATH);
    if (client.connected())
    {
        SEND_PATH = "";
    }

#ifdef D_H_T
    h = dht.readHumidity();
    t = dht.readTemperature();
    f = dht.readTemperature(true);
    Show_t = t;
    Show_h = h;
#endif
#ifdef BME
    if (Ad_BME == BME_address1 or Ad_BME == BME_address2)
    {
        h = bme280.readHumidity();
        t = bme280.readTempC();
        f = bme280.readTempF();
        p = bme280.readPressure();
        Show_t = t;
        Show_h = h;
        Show_p = p;
        Cp = round(p); //Cp = (int)p;
        Tp = round(f); //Tp = (int)f;
    }
#endif

#ifdef LDR
    LDR_Read();
#endif
    LDR_index = (float(LDR_value) / 1023.0) * 100.0;
    int Rain_Value = 1;
#ifdef Rain_s
    Rain_Value = digitalRead(Rain_Pin);
#endif
#ifdef D_H_T
    if (!isnan(h) && !isnan(t))
    {
        Tp = round(f); //Tp = (int)f;
    }
    while (lp < 2 && (isnan(h) || isnan(t)))
    {
        h = dht.readHumidity();
        t = dht.readTemperature();
        f = dht.readTemperature(true);
        Show_t = t;
        Tp = round(f); //Tp = (int)f;
        Show_h = h;
        delay(2000);
        lp++;
    }

#endif
    if (Tp < 10)
    {
        Pretemp = "00" + String(Tp);
    }
    else if (Tp < 100)
    {
        Pretemp = "0" + String(Tp);
    }
    else
    {
        Pretemp = String(Tp);
    }
    if (Cp < 10)
    {
        Prebaro = "000" + String(Cp) + "0";
    }
    else if (Cp < 100)
    {
        Prebaro = "00" + String(Cp) + "0";
    }
    else if (Cp < 1000)
    {
        Prebaro = "0" + String(Cp) + "0";
    }
    else
    {
        Prebaro = String(Cp) + "0";
    }
    if (h >= 100)
    {
        Show_h = 99;
        h = 99;
    }

    //-----------------------------------------------------Beacon NO Senser----------------------------------------//

    if (isnan(h) || isnan(t) || h == 0 || t == 0)
    { //ถ้าไม่ต่อเซนเซอร์
        if (Beacon_Beacon == true)
        {
            BUFF = String(Beacon) + String(comment);
            SendAPRS(SEND_PATH);
            //--------------NET----------------------//
            String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + ITEM + Beacon + comment;
            client.println(BUFF_NET);
            Serial.println(NTP_DATETIME_sec + " Beacon | " + BUFF_NET);
            BUF_display = BUFF_NET;
        }
        Show_frm_Srv();
    }

    //-----------------------------------------------------Beacon Wx----------------------------------------//
    else if (LDR_index < 5)
    {
        strncpy(WXBeacon, Beacon, 19);
        WXBeacon[0] = '=';
        WXBeacon[9] = '/';
        WXBeacon[19] = 0;
        String Wx_status = "";
        char Wx_symbol = '_';
        if (Rain_Value == 0)
        {
            Wx_symbol = '`';
            Wx_status = " : Raining SUN";
        }

        if (Beacon_Beacon == true)
        {
        }

        if (Beacon_Wx == true)
        {
            if (p != 0.0)
            {
                //BUFF= String(WXBeacon) + Wx_symbol + "c...s...g...t" + Pretemp + "r...p...P...h" + String(round(h)) + "b" + Prebaro + String(comment) + " T=" + String(round(Show_t)) + "° H=" + String(round(Show_h)) + "%   P=" + String(Cp);
                BUFF = String(WXBeacon) + Wx_symbol + "000/000g000t" + Pretemp + "r...p...P...h" + String(round(h)) + "b" + Prebaro + String(comment) + " T=" + String(round(Show_t)) + "° H=" + String(round(Show_h)) + "%   P=" + String(Cp);
                SendAPRS(SEND_PATH);
                //--------------NET----------------------//
                //String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + String(WXBeacon) + "_c...s...g...t" + Pretemp + "r...p...P...h" + String(round(h)) + "b" + Prebaro + String(comment) + " T=" + String(round(Show_t)) + "° H=" + String(round(Show_h)) + "%   P=" + String(Cp);
                String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + String(WXBeacon) + "_000/000g000t" + Pretemp + "r...p...P...h" + String(round(h)) + "b" + Prebaro + String(comment) + " T=" + String(round(Show_t)) + "° H=" + String(round(Show_h)) + "%   P=" + String(Cp);
                client.println(BUFF_NET);
                Serial.println(NTP_DATETIME_sec + " Beacon | " + BUFF_NET);
                BUF_display = BUFF_NET;
            }
            else
            {
                //BUFF= String(WXBeacon) + Wx_symbol + "c...s...g...t" + Pretemp + "r...p...P...h" + String(round(h)) + "b....." + String(comment) + " T=" + String(round(Show_t)) + "° H=" + String(round(Show_h)) + "%";
                BUFF = String(WXBeacon) + Wx_symbol + "000/000g000t" + Pretemp + "r...p...P...h" + String(round(h)) + "b....." + String(comment) + " T=" + String(round(Show_t)) + "° H=" + String(round(Show_h)) + "%";
                SendAPRS(SEND_PATH);
                //--------------NET----------------------//

                //String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + String(WXBeacon) + "_c...s...g...t" + Pretemp + "r...p...P...h" + String(round(h)) + "b....." + String(comment) + " T=" + String(round(Show_t)) + "° H=" + String(round(Show_h)) + "%";
                String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + String(WXBeacon) + "_000/000g000t" + Pretemp + "r...p...P...h" + String(round(h)) + "b....." + String(comment) + " T=" + String(round(Show_t)) + "° H=" + String(round(Show_h)) + "%";
                client.println(BUFF_NET);
                Serial.println(NTP_DATETIME_sec + " Beacon | " + BUFF_NET);
                BUF_display = BUFF_NET;
            }

            Show_frm_Srv();
        }
    }
    //-----------------------------------------------------Beacon Cloudy----------------------------------------//
    else if (LDR_value < change_symbol_LDR)
    {
        strncpy(WXBeacon, Beacon, 19);
        WXBeacon[0] = '=';
        WXBeacon[9] = 92;
        WXBeacon[19] = 0;
        String Wx_status = " : Cloudy ";
        char Wx_symbol = '(';
        if (Rain_Value == 0)
        {
            Wx_symbol = '`';
            Wx_status = " : Raining SUN";
        }

        if (Beacon_Beacon == true)
        {
            BUFF = String(WXBeacon) + Wx_symbol + String(comment) + Wx_status + String(LDR_index) + "% T=" + String(round(Show_t)) + "° H=" + String(round(Show_h)) + "% P=" + String(Cp);
            SendAPRS(SEND_PATH);
            //--------------NET----------------------//

            String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + String(WXBeacon) + Wx_symbol + String(comment) + Wx_status + String(LDR_index) + "%   T=" + String(round(Show_t)) + "° H=" + String(round(Show_h)) + "%   P=" + String(Cp);
            client.println(BUFF_NET);
            Serial.println(NTP_DATETIME_sec + " Beacon | " + BUFF_NET);
        }

        if (Beacon_Wx == true)
        {
            if (p != 0.0)
            {
                BUFF = "_" + NTP_MDHM + "c...s...g...t" + Pretemp + "r...p...P...h" + String(round(h)) + "b" + Prebaro;
                SendAPRS(SEND_PATH);
                //--------------NET----------------------//

                String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":_" + NTP_MDHM + "c...s...g...t" + Pretemp + "r...p...P...h" + String(round(h)) + "b" + Prebaro;
                client.println(BUFF_NET);
                Serial.println(NTP_DATETIME_sec + " Beacon | " + BUFF_NET);
                BUF_display = BUFF_NET;
            }
            else
            {
                BUFF = "_" + NTP_MDHM + "c...s...g...t0" + String(round(f)) + "r...p...P...h" + String(round(h)) + "b.....";
                SendAPRS(SEND_PATH);
                //--------------NET----------------------//

                String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":_" + NTP_MDHM + "c...s...g...t" + Pretemp + "r...p...P...h" + String(round(h)) + "b.....";
                client.println(BUFF_NET);
                Serial.println(NTP_DATETIME_sec + " Beacon | " + BUFF_NET);
                BUF_display = BUFF_NET;
            }
            Show_frm_Srv();
        }
    }
    //-----------------------------------------------------Beacon Sunny----------------------------------------//
    else
    {
        strncpy(WXBeacon, Beacon, 19);
        WXBeacon[0] = '=';
        WXBeacon[9] = 92;
        WXBeacon[19] = 0;
        String Wx_status = " : Sunny ";
        char Wx_symbol = 'U';
        if (Rain_Value == 0)
        {
            Wx_symbol = '`';
            Wx_status = " : Raining SUN";
        }

        if (Beacon_Beacon == true)
        {
            BUFF = String(WXBeacon) + Wx_symbol + String(comment) + Wx_status + String(LDR_index) + "% T=" + String(round(Show_t)) + "° H=" + String(round(Show_h)) + "% P=" + String(Cp);
            SendAPRS(SEND_PATH);
            //--------------NET----------------------//

            String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + String(WXBeacon) + Wx_symbol + String(comment) + Wx_status + String(LDR_index) + "%   T=" + String(round(Show_t)) + "° H=" + String(round(Show_h)) + "%   P=" + String(Cp);
            client.println(BUFF_NET);
            Serial.println(NTP_DATETIME_sec + " Beacon | " + BUFF_NET);
        }
        if (Beacon_Wx == true)
        {

            if (p != 0.0)
            {
                BUFF = "_" + NTP_MDHM + "c...s...g...t" + Pretemp + "r...p...P...h" + String(round(h)) + "b" + Prebaro;
                SendAPRS(SEND_PATH);
                //--------------NET----------------------//

                String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":_" + NTP_MDHM + "c...s...g...t" + Pretemp + "r...p...P...h" + String(round(h)) + "b" + Prebaro;
                client.println(BUFF_NET);
                Serial.println(NTP_DATETIME_sec + " Beacon | " + BUFF_NET);
                BUF_display = BUFF_NET;
            }
            else
            {
                BUFF = "_" + NTP_MDHM + "c...s...g...t" + Pretemp + "r...p...P...h" + String(round(h)) + "b.....";
                SendAPRS(SEND_PATH);
                //--------------NET----------------------//

                String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":_" + NTP_MDHM + "c...s...g...t" + Pretemp + "r...p...P...h" + String(round(h)) + "b.....";
                client.println(BUFF_NET);
                Serial.println(NTP_DATETIME_sec + " Beacon | " + BUFF_NET);
                BUF_display = BUFF_NET;
            }
            Show_frm_Srv();
        }
    }
    if (Beacon_Status)
    {
        unsigned long UPT = millis();
        unsigned int UPTIME_H = UPT / 3600000;
        unsigned int UPTIME_M = (UPT % 3600000) / 60000;
        String U_HH = String(UPTIME_H);
        String U_MM = String(UPTIME_M);
        if (UPTIME_H < 10)
        {
            U_HH = '0' + String(UPTIME_H);
        }
        if (UPTIME_M < 10)
        {
            U_MM = '0' + String(UPTIME_M);
        }
        String UPTIME = " UpTime:" + U_HH + "." + U_MM;

        if (!client.connected())
        {
            if (Dx_km > 0)
            {
                BUFF = ">V." + String(FW_V) + " Rx:" + String(Rx_cnt) + " Digi:" + String(Dg_cnt) + " Tx:" + String(Tx_cnt) + UPTIME + " DX:" + Dx_Call + String(Dx_km) + "km " + String(Dx_br) + "°";
                SendAPRS(SEND_PATH);
            }
            else
            {
                BUFF = ">V." + String(FW_V) + " Rx:" + String(Rx_cnt) + " Digi:" + String(Dg_cnt) + " Tx:" + String(Tx_cnt) + UPTIME;
                SendAPRS(SEND_PATH);
            }
        }

        if (client.connected()) //--------------NET----------------------//
        {
#ifdef SHOW_SSID_BEACON
            String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":>V." + String(FW_V) + " SSID:" + ssid + " Rx:" + String(Rx_cnt) + " Digi:" + String(Dg_cnt) + " Tx:" + String(Tx_cnt) + UPTIME;
#else
            String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":>V." + String(FW_V) + " Rx:" + String(Rx_cnt) + " Digi:" + String(Dg_cnt) + " Tx:" + String(Tx_cnt) + UPTIME;
#endif
            client.print(BUFF_NET);
            Serial.println(NTP_DATETIME_sec + " Beacon | " + BUFF_NET);
            if (Dx_km > 0)
            {
                String DX_print = " DX:" + Dx_Call + String(Dx_km) + "km " + String(Dx_br) + "°";
                client.println(DX_print);
            }
            else
            {
                client.println("");
            }
        }

        if (Read_ResetOnTime > 0 && UPTIME_H >= Read_ResetOnTime)
        {
            ESP.restart();
        }
    }

    //-------------------------------BEACON TELEMETRY----------------------------------------//
    if (Beacon_Telemetry)
    {
#ifdef TLM //HS3LSE 13.38 10/10/18
        if (Tcount == 0)
        {

            //-----PARM
            String MYCALL9 = String(MyCallSign) + "         ";
            MYCALL9 = MYCALL9.substring(0, 9);
            BUFF = ":" + MYCALL9 + ":PARM.RxTraffic,TxTraffic,RxDrop,RSSI,SUN";
            if (!client.connected())
            {
                SendAPRS(SEND_PATH);
            }
            String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + "::" + MYCALL9 + ":PARM.RxTraffic,TxTraffic,RxDrop,RSSI,SUN";
            client.println(BUFF_NET);
            Serial.println(NTP_DATETIME_sec + " Beacon | " + BUFF_NET);
            delay(4000);
            //-----UNIT
            BUFF = ":" + MYCALL9 + ":UNIT.Pkt,Pkt,Pkt,dBm,%";
            if (!client.connected())
            {
                SendAPRS(SEND_PATH);
            }
            BUFF_NET = String(MyCallSign) + ">" + FW_destNET + "::" + MYCALL9 + ":UNIT.Pkt,Pkt,Pkt,dBm,%";
            client.println(BUFF_NET);
            Serial.println(NTP_DATETIME_sec + " Beacon | " + BUFF_NET);
            delay(4000);
            //-----EQNS
            BUFF = ":" + MYCALL9 + ":EQNS.0,1,0,0,1,0,0,1,0,0,-1,0,0,1,0";
            if (!client.connected())
            {
                SendAPRS(SEND_PATH);
            }
            BUFF_NET = String(MyCallSign) + ">" + FW_destNET + "::" + MYCALL9 + ":EQNS.0,1,0,0,1,0,0,1,0,0,-1,0,0,1,0";
            client.println(BUFF_NET);
            Serial.println(NTP_DATETIME_sec + " Beacon | " + BUFF_NET);
            delay(4000);
        }

        //-----TLM
        String BUFF_NET = "", TT = "";
        Tcount++;
        if (Tcount < 10)
        {
            TT = "00";
        }
        else if (Tcount < 100)
        {
            TT = "0";
        }
        BUFF = "T#" + TT + String(Tcount) + "," + String(TLM_R) + "," + String(TLM_T) + "," + String(TLM_D) + "," + String(WiFi.RSSI() * -1) + "," + String(LDR_index) + "," + "00000000";
        if (!client.connected())
        {
            SendAPRS(SEND_PATH);
        }
        BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":T#" + TT + String(Tcount) + "," + String(TLM_R) + "," + String(TLM_T) + "," + String(TLM_D) + "," + String(WiFi.RSSI() * -1) + "," + String(LDR_index) + "," + "00000000";
        client.println(BUFF_NET);
        Serial.println(NTP_DATETIME_sec + " Beacon | " + BUFF_NET);
        TLM_T = 0;
        TLM_R = 0;
        TLM_D = 0;

#ifdef Developer
        //client.println(String(MyCallSign) + ">" + FW_destNET + ":)SURIN-RE!1453.33N/10329.39E+SURIN SUCCOUR & RESCUE 1994");
        //client.println(String(MyCallSign) + ">" + FW_destNET + ":)SURIN-VR!1452.72N/10330.57E+VR RESCUE SURIN ASSOCIATION");
        client.println(String(MyCallSign) + ">" + FW_destNET + ":)SURIN1669!1452.58N/10330.20E+Emergency Medical Call and Dispatch Center 1669");
        client.println("HS3MQX-2>AESPG2-2,WIDE1-1:=1501.47N/10306.61ErAPRS@Korat");
#endif
        if (Tcount == 1)
        {
#ifdef LINE_Notify
            if (Read_PWR == 0)
            {
                Serial.print("> Line notify" + String(MyCallSign) + " " + String(comment) + " Online");
                Line_Notify(String(MyCallSign) + " " + String(comment) + " Online");
            }
#endif
        }

#endif
    }
#endif
}
//--------------------------------------------------------------------END BEACON-------------------------------//

#ifdef LDR
int LDR_Read()
{
    //digitalWrite(LDR_Pin, HIGH);
    delay(300);
    LDR_value = analogRead(sensorPin);
    //LDR_value = constrain(LDR_value, 300, 850);
    //LDR_value = map(LDR_value, 300, 850, 0, 1023);
    //digitalWrite(LDR_Pin, LOW);
    return LDR_value;
}
#endif
/*int EEprom_Add[29]= {0,1,21,41,61,81,101,121,151,161,181,291,211,241,271,276,361,381,400,430,450,460,470,480,490,496,506,507,508,509};
0 *******
1  local_ip,1         a
2  subnet,21          b
3  gateway,41         c
4  dnsip,61           d
5  ssid,81            e
6  password,101       f
7  APRSIS,121         g
8  IS_port,151        h
9  MyCallSign,161     i
10  is_passcode,181   j
11  JavaFilter,291    k
12  Beacon,211        l
13  comment,241       m
14  interval,271      n
15  Brate,276         o
16  DigiDelay,361     q
17  VIAPATH,381       p
18  Profile1,400
19  Profile2,430
20  Fastspd,450
21  Fastrate,460
22  Slowspd,470
23  Slowrate,480
24  mice,490
25  Grate 496
26  Read_ResetOnTime 506
27  Vin 507 // 0/1 false/true
28  Inet_toRF_KM 508
29  V_offset 509
*/
void CMD(String RawSerial, bool UART) //ฟังก์ชั่นการตรวจเชค ว่ามีการส่งคำสั่งอะไรมาตั้งค่า UART คือเชคว่ามาทาง Serial หรือทาง telnet และสั่งให้ไปตั้งค่าใน แอดเดรสไหนของ EEPROM
{
    if (RawSerial.indexOf("?b=1") == 0)
    {
        Beacon_Beacon = true;
        SendBeacon();
        Beacon_Beacon = false;
    }
    else if (RawSerial.indexOf("?d=1") == 0)
    {
        CMD_debug = true;
        if (UART)
        {
            Serial.println("Debug ON");
        }
        else
        {
            serverClients[0].println("Debug ON");
        }
    }
    else if (RawSerial.indexOf("?d=0") == 0)
    {
        CMD_debug = false;
        if (UART)
        {
            Serial.println("Debug OFF");
        }
        else
        {
            serverClients[0].println("Debug OFF");
        }
    }
    else if (RawSerial.indexOf("?na?") == 0)
    {
        if (UART)
        {
            Serial.println(WiFi.localIP()) + (ssid) + (WiFi.RSSI());
        }
        else
        {
            serverClients[0].println(WiFi.localIP()) + (ssid) + (WiFi.RSSI());
        }
    }
    else if (RawSerial.indexOf("?nb?") == 0)
    {
        ScanNetwork();
    }
    else if (RawSerial.indexOf("?nc?") == 0)
    {
        InitEEPROM();
        wifistart();
    }
    else if (RawSerial.indexOf("?nd?") == 0)
    {
        InitEEPROM();
        APRSIS_connect();
    }
    else if (RawSerial.indexOf("?wa?") == 0)
    {
        if (UART)
        {
            Serial.println(Label_config[1]);
            Serial.print(EP_write_char(EEprom_Add[1], RawSerial));
            Serial.println(" :OK");
        }
        else
        {
            serverClients[0].println(Label_config[1]);
            serverClients[0].print(EP_write_char(EEprom_Add[1], RawSerial));
            serverClients[0].println(" :OK");
        }
    }
    else if (RawSerial.indexOf("?wb?") == 0)
    {
        if (UART)
        {
            Serial.println(Label_config[2]);
            Serial.print(EP_write_char(EEprom_Add[2], RawSerial));
            Serial.println(" :OK");
        }
        else
        {
            serverClients[0].println(Label_config[2]);
            serverClients[0].print(EP_write_char(EEprom_Add[2], RawSerial));
            serverClients[0].println(" :OK");
        }
    }
    else if (RawSerial.indexOf("?wc?") == 0)
    {
        if (UART)
        {
            Serial.println(Label_config[3]);
            Serial.print(EP_write_char(EEprom_Add[3], RawSerial));
            Serial.println(" :OK");
        }
        else
        {
            serverClients[0].println(Label_config[3]);
            serverClients[0].print(EP_write_char(EEprom_Add[3], RawSerial));
            serverClients[0].println(" :OK");
        }
    }
    else if (RawSerial.indexOf("?wd?") == 0)
    {
        if (UART)
        {
            Serial.println(Label_config[4]);
            Serial.print(EP_write_char(EEprom_Add[4], RawSerial));
            Serial.println(" :OK");
        }
        else
        {
            serverClients[0].println(Label_config[4]);
            serverClients[0].print(EP_write_char(EEprom_Add[4], RawSerial));
            serverClients[0].println(" :OK");
        }
    }
    else if (RawSerial.indexOf("?we?") == 0)
    {
        if (UART)
        {
            Serial.println(Label_config[5]);
            Serial.print(EP_write_char(EEprom_Add[5], RawSerial));
            Serial.println(" :OK");
        }
        else
        {
            serverClients[0].println(Label_config[5]);
            serverClients[0].print(EP_write_char(EEprom_Add[5], RawSerial));
            serverClients[0].println(" :OK");
        }
    }
    else if (RawSerial.indexOf("?wf?") == 0)
    {
        if (UART)
        {
            Serial.println(Label_config[6]);
            Serial.print(EP_write_char(EEprom_Add[6], RawSerial));
            Serial.println(" :OK");
        }
        else
        {
            serverClients[0].println(Label_config[6]);
            serverClients[0].print(EP_write_char(EEprom_Add[6], RawSerial));
            serverClients[0].println(" :OK");
        }
    }
    else if (RawSerial.indexOf("?wg?") == 0)
    {
        if (UART)
        {
            Serial.println(Label_config[7]);
            Serial.print(EP_write_char(EEprom_Add[7], RawSerial));
            Serial.println(" :OK");
        }
        else
        {
            serverClients[0].println(Label_config[7]);
            serverClients[0].print(EP_write_char(EEprom_Add[7], RawSerial));
            serverClients[0].println(" :OK");
        }
    }
    else if (RawSerial.indexOf("?wh?") == 0)
    {
        if (UART)
        {
            Serial.println(Label_config[8]);
            Serial.print(EP_write_char(EEprom_Add[8], RawSerial));
            Serial.println(" :OK");
        }
        else
        {
            serverClients[0].println(Label_config[8]);
            serverClients[0].print(EP_write_char(EEprom_Add[8], RawSerial));
            serverClients[0].println(" :OK");
        }
    }
    else if (RawSerial.indexOf("?wi?") == 0)
    {
        RawSerial.toUpperCase();
        if (UART)
        {
            Serial.println(Label_config[9]);
            Serial.print(EP_write_char(EEprom_Add[9], RawSerial));
            Serial.println(" :OK");
        }
        else
        {
            serverClients[0].println(Label_config[9]);
            serverClients[0].print(EP_write_char(EEprom_Add[9], RawSerial));
            serverClients[0].println(" :OK");
        }
        String BUFF_config = EP_read_char(EEprom_Add[9]);
        BUFF_config.toCharArray(MyRawCall, 20);
    }
    else if (RawSerial.indexOf("?wj?") == 0)
    {
        if (UART)
        {
            Serial.println(Label_config[10]);
            Serial.print(EP_write_char(EEprom_Add[10], RawSerial));
            Serial.println(" :OK");
        }
        else
        {
            serverClients[0].println(Label_config[10]);
            serverClients[0].print(EP_write_char(EEprom_Add[10], RawSerial));
            serverClients[0].println(" :OK");
        }
    }
    else if (RawSerial.indexOf("?wk?") == 0)
    {
        if (UART)
        {
            Serial.println(Label_config[11]);
            Serial.print(EP_write_char(EEprom_Add[11], RawSerial));
            Serial.println(" :OK");
        }
        else
        {
            serverClients[0].println(Label_config[11]);
            serverClients[0].print(EP_write_char(EEprom_Add[11], RawSerial));
            serverClients[0].println(" :OK");
        }
    }
    else if (RawSerial.indexOf("?wl?") == 0)
    {
        if (UART)
        {
            Serial.println(Label_config[12]);
            Serial.print(EP_write_char(EEprom_Add[12], RawSerial));
            Serial.println(" :OK");
        }
        else
        {
            serverClients[0].println(Label_config[12]);
            serverClients[0].print(EP_write_char(EEprom_Add[12], RawSerial));
            serverClients[0].println(" :OK");
        }
    }
    else if (RawSerial.indexOf("?wm?") == 0)
    {
        if (UART)
        {
            Serial.println(Label_config[13]);
            Serial.print(EP_write_char(EEprom_Add[13], RawSerial));
            Serial.println(" :OK");
        }
        else
        {
            serverClients[0].println(Label_config[13]);
            serverClients[0].print(EP_write_char(EEprom_Add[13], RawSerial));
            serverClients[0].println(" :OK");
        }
    }
    else if (RawSerial.indexOf("?wn?") == 0)
    {
        if (UART)
        {
            Serial.println(Label_config[14]);
            Serial.print(EP_write_char(EEprom_Add[14], RawSerial));
            Serial.println(" :OK");
        }
        else
        {
            serverClients[0].println(Label_config[14]);
            serverClients[0].print(EP_write_char(EEprom_Add[14], RawSerial));
            serverClients[0].println(" :OK");
        }
    }
    else if (RawSerial.indexOf("?wo?") == 0)
    {
        if (UART)
        {
            Serial.println(Label_config[15]);
            Serial.print(EP_write_char(EEprom_Add[15], RawSerial));
            Serial.println(" :OK");
        }
        else
        {
            serverClients[0].println(Label_config[15]);
            serverClients[0].print(EP_write_char(EEprom_Add[15], RawSerial));
            serverClients[0].println(" :OK");
        }
    }
    else if (RawSerial.indexOf("?wp?") == 0)
    {
        RawSerial.toUpperCase();
        if (UART)
        {
            Serial.println(Label_config[17]);
            Serial.print(EP_write_char(EEprom_Add[17], RawSerial));
            Serial.println(" :OK");
        }
        else
        {
            serverClients[0].println(Label_config[17]);
            serverClients[0].print(EP_write_char(EEprom_Add[17], RawSerial));
            serverClients[0].println(" :OK");
        }
        String BUFF_config = EP_read_char(EEprom_Add[17]);
        BUFF_config.toCharArray(VIAPATH, 20);
    }
    else if (RawSerial.indexOf("?wq?") == 0)
    {
        if (UART)
        {
            Serial.println(Label_config[16]);
            Serial.print(EP_write_char(EEprom_Add[16], RawSerial));
            Serial.println(" :OK");
        }
        else
        {
            serverClients[0].println(Label_config[16]);
            serverClients[0].print(EP_write_char(EEprom_Add[16], RawSerial));
            serverClients[0].println(" :OK");
        }
        String BUFF_config = EP_read_char(EEprom_Add[16]);
        DigiDelay = BUFF_config.toInt();
    }
    else if (RawSerial.indexOf("?wr?") == 0)
    {
        String C_restart = "";
        int i = 4;
        while (RawSerial.charAt(i) != 0)
        {
            C_restart += RawSerial.charAt(i);
            i++;
        }
        int V_restart = C_restart.toInt(); // atoi(C_restart);
        EEPROM.write(506, V_restart);
        EEPROM.commit();
        delay(20);
        if (UART)
        {
            Serial.println(Label_config[18]);
            Serial.print(C_restart);
            Serial.println(" :OK");
        }
        else
        {
            serverClients[0].println(Label_config[18]);
            serverClients[0].print(C_restart);
            serverClients[0].println(" :OK");
        }
    }
    else if (RawSerial.indexOf("?ws?") == 0)
    {
        String C_KM;
        C_KM = RawSerial.charAt(4) + RawSerial.charAt(5) + RawSerial.charAt(6);
        int V_KM = C_KM.toInt();
        EEPROM.write(508, V_KM);
        EEPROM.commit();
        delay(20);
        if (UART)
        {
            Serial.println(" :OK");
        }
        else
        {
            serverClients[0].println(" :OK");
        }
    }

    else if (RawSerial.indexOf("?wt?") == 0)
    {
        if (UART)
        {
            Serial.println(Label_config[19]);
            Serial.print(EP_write_char(EEprom_Add[40], RawSerial));
            Serial.println(" :OK");
        }
        else
        {
            serverClients[0].println(Label_config[19]);
            serverClients[0].print(EP_write_char(EEprom_Add[40], RawSerial));
            serverClients[0].println(" :OK");
        }
        LINE_TOKEN = EP_read_char(EEprom_Add[40]);
#ifdef LINE_Notify
//LINE.setToken(LINE_TOKEN);
#endif
    }

    else if (RawSerial.indexOf("?ra?") == 0)
    {
        String R_data = EP_read_char(EEprom_Add[1]);
        if (CMD_REMOTE == true)
        {
            BUFF = String(Answer) + String(R_data);
            SendAPRS(String(VIAPATH));
            String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + String(Answer) + String(R_data);
            client.println(BUFF_NET);
        }
    }
    else if (RawSerial.indexOf("?rb?") == 0)
    {
        String R_data = EP_read_char(EEprom_Add[2]);
        if (CMD_REMOTE == true)
        {
            BUFF = String(Answer) + String(R_data);
            SendAPRS(String(VIAPATH));
            String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + String(Answer) + String(R_data);
            client.println(BUFF_NET);
        }
    }
    else if (RawSerial.indexOf("?rc?") == 0)
    {
        String R_data = EP_read_char(EEprom_Add[3]);
        if (CMD_REMOTE == true)
        {
            BUFF = String(Answer) + String(R_data);
            SendAPRS(String(VIAPATH));
            String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + String(Answer) + String(R_data);
            client.println(BUFF_NET);
        }
    }
    else if (RawSerial.indexOf("?rd?") == 0)
    {
        String R_data = EP_read_char(EEprom_Add[7]);
        if (CMD_REMOTE == true)
        {
            BUFF = String(Answer) + String(R_data);
            SendAPRS(String(VIAPATH));
            String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + String(Answer) + String(R_data);
            client.println(BUFF_NET);
        }
    }
    else if (RawSerial.indexOf("?re?") == 0)
    {
        String R_data = EP_read_char(EEprom_Add[5]);
        if (CMD_REMOTE == true)
        {
            BUFF = String(Answer) + String(R_data);
            SendAPRS(String(VIAPATH));
            String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + String(Answer) + String(R_data);
            client.println(BUFF_NET);
        }
    }
    else if (RawSerial.indexOf("?rf?") == 0)
    {
        String R_data = EP_read_char(EEprom_Add[6]);
        if (CMD_REMOTE == true)
        {
            BUFF = String(Answer) + String(R_data);
            SendAPRS(String(VIAPATH));
            String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + String(Answer) + String(R_data);
            client.println(BUFF_NET);
        }
    }
    else if (RawSerial.indexOf("?rg?") == 0)
    {
        String R_data = EP_read_char(EEprom_Add[7]);
        if (CMD_REMOTE == true)
        {
            BUFF = String(Answer) + String(R_data);
            SendAPRS(String(VIAPATH));
            String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + String(Answer) + String(R_data);
            client.println(BUFF_NET);
        }
    }
    else if (RawSerial.indexOf("?rh?") == 0)
    {
        String R_data = EP_read_char(EEprom_Add[8]);
        if (CMD_REMOTE == true)
        {
            BUFF = String(Answer) + String(R_data);
            SendAPRS(String(VIAPATH));
            String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + String(Answer) + String(R_data);
            client.println(BUFF_NET);
        }
    }
    else if (RawSerial.indexOf("?ri?") == 0)
    {
        String R_data = EP_read_char(EEprom_Add[9]);
        if (CMD_REMOTE == true)
        {
            BUFF = String(Answer) + String(R_data);
            SendAPRS(String(VIAPATH));
            String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + String(Answer) + String(R_data);
            client.println(BUFF_NET);
        }
    }
    else if (RawSerial.indexOf("?rj?") == 0)
    {
        String R_data = EP_read_char(EEprom_Add[10]);
        if (CMD_REMOTE == true)
        {
            BUFF = String(Answer) + String(R_data);
            SendAPRS(String(VIAPATH));
            String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + String(Answer) + String(R_data);
            client.println(BUFF_NET);
        }
    }
    else if (RawSerial.indexOf("?rk?") == 0)
    {
        String R_data = EP_read_char(EEprom_Add[11]);
        if (CMD_REMOTE == true)
        {
            BUFF = String(Answer) + String(R_data);
            SendAPRS(String(VIAPATH));
            String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + String(Answer) + String(R_data);
            client.println(BUFF_NET);
        }
    }
    else if (RawSerial.indexOf("?rl?") == 0)
    {
        String R_data = EP_read_char(EEprom_Add[12]);
        if (CMD_REMOTE == true)
        {
            BUFF = String(Answer) + String(R_data);
            SendAPRS(String(VIAPATH));
            String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + String(Answer) + String(R_data);
            client.println(BUFF_NET);
        }
    }
    else if (RawSerial.indexOf("?rm?") == 0)
    {
        String R_data = EP_read_char(EEprom_Add[13]);
        if (CMD_REMOTE == true)
        {
            BUFF = String(Answer) + String(R_data);
            SendAPRS(String(VIAPATH));
            String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + String(Answer) + String(R_data);
            client.println(BUFF_NET);
        }
    }
    else if (RawSerial.indexOf("?rn?") == 0)
    {
        String R_data = EP_read_char(EEprom_Add[14]);
        if (CMD_REMOTE == true)
        {
            BUFF = String(Answer) + String(R_data);
            SendAPRS(String(VIAPATH));
            String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + String(Answer) + String(R_data);
            client.println(BUFF_NET);
        }
    }
    else if (RawSerial.indexOf("?ro?") == 0)
    {
        String R_data = EP_read_char(EEprom_Add[15]);
        if (CMD_REMOTE == true)
        {
            BUFF = String(Answer) + String(R_data);
            SendAPRS(String(VIAPATH));
            String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + String(Answer) + String(R_data);
            client.println(BUFF_NET);
        }
    }
    else if (RawSerial.indexOf("?rp?") == 0)
    {
        String R_data = EP_read_char(EEprom_Add[17]);
        if (CMD_REMOTE == true)
        {
            BUFF = String(Answer) + String(R_data);
            SendAPRS(String(VIAPATH));
            String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + String(Answer) + String(R_data);
            client.println(BUFF_NET);
        }
    }
    else if (RawSerial.indexOf("?rq?") == 0)
    {
        String R_data = EP_read_char(EEprom_Add[16]);
        if (CMD_REMOTE == true)
        {
            BUFF = String(Answer) + String(R_data);
            SendAPRS(String(VIAPATH));
            String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + String(Answer) + String(R_data);
            client.println(BUFF_NET);
        }
    }
    else if (RawSerial.indexOf("?rr?") == 0)
    {
        String R_data = String(Read_ResetOnTime) = EEPROM.read(506);
        if (CMD_REMOTE == true)
        {
            BUFF = String(Answer) + String(R_data);
            SendAPRS(String(VIAPATH));
            String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + String(Answer) + String(R_data);
            client.println(BUFF_NET);
        }
    }
    else if (RawSerial.indexOf("?rs?") == 0)
    {
        Read_ResetOnTime = EEPROM.read(506);
        delay(20);
        if (CMD_REMOTE == true)
        {
            BUFF = String(Answer) + String(Read_ResetOnTime);
            SendAPRS(String(VIAPATH));
            String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + String(Answer) + String(Read_ResetOnTime);
            client.println(BUFF_NET);
        }
    }
    else if (RawSerial.indexOf("?rt?") == 0)
    {
        String R_data = EP_read_char(EEprom_Add[40]);
        if (CMD_REMOTE == true)
        {
            BUFF = String(Answer) + String(R_data);
            SendAPRS(String(VIAPATH));
            String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + String(Answer) + String(R_data);
            client.println(BUFF_NET);
        }
    }
    else if (RawSerial.indexOf("?restart") == 0)
    {
        if (UART)
        {
            Serial.println("OK Restart...");
            delay(3000);
            ESP.restart();
        }
        else
        {
            serverClients[0].println("OK Restart...");
            delay(3000);
            ESP.restart();
        }
    }
    else if ((RawSerial.indexOf("?DISP") == 0) || (RawSerial.indexOf("?disp") == 0))
    {
        InitEEPROM();
        if (UART)
        {
            Serial.print(Label_config[1]);
            Serial.printf("%d.%d.%d.%d\r\n", local_ip[0], local_ip[1], local_ip[2], local_ip[3]);
            Serial.print(Label_config[2]);
            Serial.printf("%d.%d.%d.%d\r\n", subnet[0], subnet[1], subnet[2], subnet[3]);
            Serial.print(Label_config[3]);
            Serial.printf("%d.%d.%d.%d\r\n", gateway[0], gateway[1], gateway[2], gateway[3]);
            Serial.print(Label_config[4]);
            Serial.printf("%d.%d.%d.%d\r\n", dnsip[0], dnsip[1], dnsip[2], dnsip[3]);
            Serial.print(Label_config[5]);
            Serial.println(ssid);
            Serial.print(Label_config[6]);
            Serial.println(password);
            Serial.print(Label_config[7]);
            Serial.println(APRSIS);
            Serial.print(Label_config[8]);
            Serial.println(IS_port);
            Serial.print(Label_config[9]);
            Serial.println(MyRawCall);
            Serial.print(Label_config[10]);
            Serial.println(is_passcode);
            Serial.print(Label_config[11]);
            Serial.println(JavaFilter);
            Serial.print(Label_config[12]);
            Serial.println(Beacon);
            Serial.print(Label_config[13]);
            Serial.println(comment);
            Serial.print(Label_config[14]);
            Serial.println(interval);
            Serial.print(Label_config[15]);
            Serial.println(Brate);
            Serial.print(Label_config[17]);
            Serial.println(VIAPATH);
            Serial.print(Label_config[16]);
            Serial.println(DigiDelay);
            Read_ResetOnTime = EEPROM.read(506);
            delay(100);
            Serial.print(Label_config[18]);
            Serial.println(String(Read_ResetOnTime));
            Serial.print(Label_config[19]);
            Serial.println(LINE_TOKEN);
            //Inet_toRF_KM = EEPROM.read(508);delay(100);
            //Serial.printf("s_IS>RF in Km  %d\r\n",Inet_toRF_KM);
        }
        else
        {
            serverClients[0].print(Label_config[1]);
            serverClients[0].printf("%d.%d.%d.%d\r\n", local_ip[0], local_ip[1], local_ip[2], local_ip[3]);
            serverClients[0].print(Label_config[2]);
            serverClients[0].printf("%d.%d.%d.%d\r\n", subnet[0], subnet[1], subnet[2], subnet[3]);
            serverClients[0].print(Label_config[3]);
            serverClients[0].printf("%d.%d.%d.%d\r\n", gateway[0], gateway[1], gateway[2], gateway[3]);
            serverClients[0].print(Label_config[4]);
            serverClients[0].printf("%d.%d.%d.%d\r\n", dnsip[0], dnsip[1], dnsip[2], dnsip[3]);
            serverClients[0].print(Label_config[5]);
            serverClients[0].println(ssid);
            serverClients[0].print(Label_config[6]);
            serverClients[0].println(password);
            serverClients[0].print(Label_config[7]);
            serverClients[0].println(APRSIS);
            serverClients[0].print(Label_config[8]);
            serverClients[0].println(IS_port);
            serverClients[0].print(Label_config[9]);
            serverClients[0].println(MyRawCall);
            serverClients[0].print(Label_config[10]);
            serverClients[0].println(is_passcode);
            serverClients[0].print(Label_config[11]);
            serverClients[0].println(JavaFilter);
            serverClients[0].print(Label_config[12]);
            serverClients[0].println(Beacon);
            serverClients[0].print(Label_config[13]);
            serverClients[0].println(comment);
            serverClients[0].print(Label_config[14]);
            serverClients[0].println(interval);
            serverClients[0].print(Label_config[15]);
            serverClients[0].println(Brate);
            serverClients[0].print(Label_config[17]);
            serverClients[0].println(VIAPATH);
            serverClients[0].print(Label_config[16]);
            serverClients[0].println(DigiDelay);
            Read_ResetOnTime = EEPROM.read(506);
            delay(100);
            serverClients[0].print(Label_config[18]);
            serverClients[0].println(String(Read_ResetOnTime));
            serverClients[0].print(Label_config[19]);
            serverClients[0].println(LINE_TOKEN);
            //Inet_toRF_KM = EEPROM.read(508);delay(100);
            //serverClients[0].printf("s_IS>RF in Km  %d\r\n",Inet_toRF_KM);
        }
    }
    else if (RawSerial.indexOf("?reeprom") == 0)
    { //3.1d
        char Reep;
        Serial.print("\r\n");
        for (int r_e = 0; r_e <= 512; r_e++)
        {
            Reep = EEPROM.read(r_e);
            delay(10);
            Serial.print(Reep, DEC);
        }
        Serial.print("\r\n");
        for (int r_e = 0; r_e <= 512; r_e++)
        {
            Reep = EEPROM.read(r_e);
            delay(10);
            Serial.print(Reep);
        }
    }

    else if (RawSerial.indexOf("?h") == 0)
    {
        if (UART)
        {
            Serial.println("Ex ตัวอย่างการใช้คำสั่ง ?wa?192.168.1.22 and enter");
            Serial.println("------- ตัวอย่าง --------");
            Serial.println("?wa?192.168.1.22 [Static IP Igate]");
            Serial.println("?wb?255.255.255.0 [Subnet]");
            Serial.println("?wc?192.168.1.1 [Gateway]");
            Serial.println("?wd?8.8.8.8 [DNS]");
            Serial.println("?we?MyhomeWIFI [SSID WIFI]");
            Serial.println("?wf?123456 [Password WIFI]");
            Serial.println("?wg?aprsth.nakhonthai.net [APRS Server]");
            Serial.println("?wh?14580 [APRSIS Port]");
            Serial.println("?wi?HS3LSE-1 [Mycall]");
            //Serial.println("?wj?000000 [Passcode]");
            Serial.println("?wk?m/1 [filter]");
            Serial.println("?wl?!1455.03N/10329.84E& [Location table and symbol]");
            Serial.println("?wm?MyIgate [comment]");
            Serial.println("?wn?15 [Interval beacon minute]");
            Serial.println("?wo?9600 [Baud Rate]");
            Serial.println("?wp?WIDE1-1 [IGATE Via PATH]");
            Serial.println("?wq?OFF [DIGIPEATER]");
            Serial.println("?wr?0 [Restart every]");
            Serial.println("?wt? LINE TOKEN");
            Serial.println("***************************");
            Serial.println("?na? [Lookup IP]");
            Serial.println("?nb? [Scan Network]");
            Serial.println("?nd? [Connect APRS Serv]");
            Serial.println("***************************");
            Serial.println("?restart [Restart System]");
            Serial.println("***************************");
            Serial.println("อ่านค่าที่ตั้งไว้ ?ra? ถึง ?rq?  หรือ ?DISP อ่านค่าทั้งหมด");
        }
        else
        {
            serverClients[0].println("Ex ตัวอย่างการใช้คำสั่ง ?wa?192.168.1.22 and enter");
            serverClients[0].println("------- ตัวอย่าง --------");
            serverClients[0].println("?wa?192.168.1.22 [Static IP Igate]");
            serverClients[0].println("?wb?255.255.255.0 [Subnet]");
            serverClients[0].println("?wc?192.168.1.1 [Gateway]");
            serverClients[0].println("?wd?8.8.8.8 [DNS]");
            serverClients[0].println("?we?MyhomeWIFI [SSID WIFI]");
            serverClients[0].println("?wf?123456 [Password WIFI]");
            serverClients[0].println("?wg?aprsth.nakhonthai.net [APRS Server]");
            serverClients[0].println("?wh?14580 [APRSIS Port]");
            serverClients[0].println("?wi?HS3LSE-1 [Mycall]");
            //serverClients[0].println("?wj?000000 [Passcode]");
            serverClients[0].println("?wk?m/1 [filter]");
            serverClients[0].println("?wl?!1455.03N/10329.84E& [Location table and symbol]");
            serverClients[0].println("?wm?MyIgate [comment]");
            serverClients[0].println("?wn?15 [Interval beacon minute]");
            serverClients[0].println("?wo?9600 [Baud Rate]");
            serverClients[0].println("?wp?WIDE1-1 [IGATE Via PATH]");
            serverClients[0].println("?wq?OFF [DIGIPEATER]");
            serverClients[0].println("?wr?0 [Restart every]");
            serverClients[0].println("?wt? LINE TOKEN");
            serverClients[0].println("***************************");
            serverClients[0].println("?na? [Lookup IP]");
            serverClients[0].println("?nb? [Scan Network]");
            serverClients[0].println("?nd? [Connect APRS Serv]");
            serverClients[0].println("***************************");
            serverClients[0].println("?restart [Restart System]");
            serverClients[0].println("***************************");
            serverClients[0].println("อ่านค่าที่ตั้งไว้ ?ra? ถึง ?rq?  หรือ ?DISP อ่านค่าทั้งหมด");
        }
    }

    else
    {
        if (CMD_debug == true)
        {
            Serial.println("error");
        }
    }
}

void wifistart()
{

    WiFi.disconnect();
#ifdef OLED
    display.clear();
#ifdef Two_color //****************จอ 2 สี*******************//

    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.setFont(ArialMT_Plain_16);
    display.drawString(64, 0, "WiFi > START...");
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.setFont(ArialMT_Plain_10);
    String bf = "SSID:" + String(ssid);
    display.drawString(64, 24, bf); //HS6TUX_23092561-1029//
#else                               //*****************************************//
#ifdef SHOW_SCAN
    String s = "";
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.setFont(ArialMT_Plain_10);
    int n = WiFi.scanNetworks();
    if (n == 0)
    {
        s += "no networks found";
        display.drawString(0, 0, s);
    }
    else
    {
        s += String(n) + " networks found ";
        display.drawString(0, 0, s);
    }
    for (int i = 0; i < n; ++i)
    {
        s = String(i + 1);
        s += ": ";
        s += String(WiFi.SSID(i));
        s += " (";
        s += String(WiFi.RSSI(i));
        s += ")";
        s += String((WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*");
        //s += "<br>";
        if (String(WiFi.SSID(i)) == String(ssid))
        {
            Target_wifi_Start = true;
        }
        display.drawString(0, 12 * (i + 1), s);
        delay(10);
    }
    display.display();
    delay(5000);
    display.clear();
    if (Target_wifi_Start)
    {
        display.drawString(0, 0, String(ssid) + " >Found");
    }
    else
    {
        display.drawString(0, 0, String(ssid) + " >Not Found");
    }
    display.drawString(0, 12, "Connecting to > ... ");
    display.drawString(0, 24, String(ssid));

#else
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.setFont(ArialMT_Plain_10);
    display.drawString(0, 0, "WIFI > Start...");
    display.drawString(0, 12, "SSID:" + String(ssid));
#endif
#endif

#endif
    client.stop();
    Serial.println("> Connecting to : SSID: " + String(ssid) + " | Password " + String(password));
    serverClients[0].printf("SSID:%s\r\n", ssid);
    WiFi.begin(ssid, password);

    if (WiFi.status() == WL_CONNECTED)
    {
        String bf = "IP: " + WiFi.localIP().toString();
        display.drawString(0, 24, bf);
        Serial.println(bf); //แสดงหมายเลข IP ของ ESP8266
        Serial.print("> Server:");
        Serial.print(APRSIS);
        Serial.printf(":%d\r\n", IS_port);
        wifi_loss++;
    }
    else
    {
        Serial.println("> ReConnect");
    }

#ifdef OLED
    display.display();
#endif
}
void APRSIS_connect()
{
    String bf = "IP: " + WiFi.localIP().toString();
    if (WiFi.status() == WL_CONNECTED)
    {

#ifdef OLED

        display.clear();
#ifdef Two_color //****************จอ 2 สี*******************//

        display.setTextAlignment(TEXT_ALIGN_CENTER);
        display.setFont(ArialMT_Plain_16);
        display.drawString(64, 0, "Connected OK");
        display.setFont(ArialMT_Plain_10);
        display.drawStringMaxWidth(64, 30, 128, bf); //display.drawString(0, 36, bf);  //HS6TUX_23092561-1030//
#else                                                //*******************************************//
        display.drawString(0, 0, "WIFI > Start...");
        display.drawString(0, 12, "SSID:" + String(ssid));
        display.drawString(0, 24, bf);
        bf = "Serv: " + String(APRSIS);
        display.drawStringMaxWidth(0, 36, 128, bf); //display.drawString(0, 36, bf);
#endif
        display.display();

#endif
    }
    Serial.println("> Connecting to Server:" + String(APRSIS) + ":" + String(IS_port));
    String Log_in = "";
    client.connect(APRSIS, IS_port); //เชื่อมต่อกับ APRSServer
    if (CMD_debug == true)
    {
        Serial.print("*");
    }
    digitalWrite(LED_online, LOW);
    delay(500);
    digitalWrite(LED_online, HIGH);
    if (serverClients[0] && serverClients[0].connected())
    {
        serverClients[0].print("*");
    }

    if (client.connected()) //-----APRS-IS Connected----//
    {
        Serial.println("> IGATE Online");
        client.printf("user %s pass %s vers %s  filter %s\r\n", MyCallSign, is_passcode, IGATE_Ver, JavaFilter);

        if (serverClients[0] && serverClients[0].connected())
        {
            serverClients[0].println("Online");
        }
        digitalWrite(LED_online, LOW);
        Serial.println("> Send Log in");
        Serial.printf("user %s pass %s vers %s  filter %s\r\n", MyCallSign, is_passcode, IGATE_Ver, JavaFilter);

        Inet_loss++;
        Loop_connect_IS = 0;
        if (CMD_config == false)
        {
            Run_Time_Sec = interval * 60;
        } //สั่งให้เวลาครบกำหนดส่ง Beacon
    }
}
void ScanNetwork()
{
    Serial.println("> Scaning network WiFi");
    int n = WiFi.scanNetworks();
    if (n == 0)
    {
        if (serverClients[0] && serverClients[0].connected())
        {
            serverClients[0].println("no networks found");
        }
        Serial.println("> no networks found");
    }
    else
    {
        if (serverClients[0] && serverClients[0].connected())
        {
            serverClients[0].printf("%d  networks found\r\n", n);
        }
        Serial.print(n);
        Serial.println(" networks found");
    }
    for (int i = 0; i < n; ++i)
    {
        // if(CMD_debug==true){   // Print SSID and RSSI for each network found
        Serial.print(i + 1);
        Serial.print(": ");
        Serial.print(WiFi.SSID(i));
        Serial.print(" (");
        Serial.print(WiFi.RSSI(i));
        Serial.print(")");
        Serial.println((WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*");
        //  }
        delay(10);
        if (String(WiFi.SSID(i)) == String(ssid))
        {
            Target_wifi_Start = true;
        }
        if (serverClients[0] && serverClients[0].connected())
        {
            serverClients[0].printf("%d: ", i + 1);
            serverClients[0].println(WiFi.SSID(i));
        }
    }
}

void SAB_CONFIG()
{

    if (server.hasClient())
    {
        if (!serverClients[0].connected())
        {

            serverClients[0] = server.available();
            if (IGATE_ON == true && (WiFi.status() != WL_CONNECTED))
            {
                Config_Interrupt = true;
                IGATE_ON = false;
                WiFi.disconnect();
                Serial.printf("> IGATE OFF \r\n");
                display.clear();
                display.setFont(ArialMT_Plain_24);
                display.setTextAlignment(TEXT_ALIGN_CENTER);
                display.drawString(64, 20, "IGATEOFF"); //ให้แสดงผลที่ OLED ว่า OFF
                display.display();
            }
            Serial.println("> Has Client on your AccessPoint Connected");
            serverClients[0].printf("\r\n%s Config Mode.. \r\n", MyCallSign);
            if ((WiFi.status() != WL_CONNECTED))
            {
                serverClients[0].printf("SSID: %s > ", ssid);
                serverClients[0].printf("ไม่สำเร็จ Fail!!\r\nกรุณาตวจสอบ [SSID] ?nb? หรือ ?re?\r\n");
                serverClients[0].printf("[Password] ?rf?\r\nพิมพ์ ?h คำสั่งช่วยเหลือ\r\n");
            }
            else
            {
                serverClients[0].printf("SSID: %s > Connected OK\r\n", ssid);
            }
            if (!client.connected())
            {
                serverClients[0].printf("Conneting Server: %s > ", APRSIS);
            }
            else
            {
                serverClients[0].printf("Server: %s > ", APRSIS);
                serverClients[0].printf(" สำเร็จ Connected OK\r\n  ?h for help\r\n");
            }
        }
    }
    char RawConfig[50];
    byte RawConfigkiss[120];
    boolean CHK_ComC = false;
    while (serverClients[0].available())
    { //อ่านข้อมูลจาก WiFi หรือ APRSDroid
        delay(2);
        char c = (char)serverClients[0].read();
        RawConfig[indexcon] = c;
        KISS[indexRK] = c;
        if (c != 0xC0)
        {
            indexRK++;
            if (indexRK > (buffsize - 2))
            {
                indexRK = 1;
            }
        }
        else
        {
            KISS[0] = 0xC0;
            KISS[indexRK] = 0xC0;
            indexRK = 1;
            if (KISS[0] == 0xC0 && KISS[1] == 0x00)
            {
                STARTKISS = 2;
                if (CMD_debug == true)
                {
                    Serial.println("Droid--KISS C0");
                }
            }
        }
        if (c == '?')
        {
            CHK_ComC = true;
        }
        if (CHK_ComC && c < 14)
        {
            RawConfig[indexcon] = 0;
            CMD(String(RawConfig), false);
            indexcon = 0;
            CHK_ComC = false;
            if (CMD_debug == true)
            {
                Serial.println("CommandOK");
            }
        }
        if (CHK_ComC && indexcon < 48)
        {
            indexcon++;
        }
    }
}

void CMD_KISS_Parameter()
{
    byte CMD_KISS[16] = {
        192,
        1,
        25,
        192,
        192,
        2,
        64,
        192,
        192,
        3,
        10,
        192,
        192,
        4,
        2,
        192,
    };
    TNC_Serial.write(CMD_KISS, 16);
}
//byte CMD_KISS[16]={0xC0,0x01,0x12,0xC0,0xC0,0x02,0x40,0xC0,0xC0,0x03,0x0A,0xC0,0xC0,0x04,0x02,0xC0,};
//0x00  0x01  0x02  0x03  0x040 0x05  0x06  0xFF
//Datafr  Txdelay P Slot  Txtail  Fulldup Set Return
//ตัวอย่าง ถ้า ตั้ง Txdelay คือ 0x01 ก็ จะส่ง0xC0,0x01,Txdelay*10ms,0xC0 //0x12 = 18*10 =180ms // 0x32 = 50*10 = 500ms

//-----------------------------------------------------DISPLAY------------------------------------------------
#ifdef OLED
void Show_frm_Srv()
{
    boolean Digied = false;
    //String BUF=String(RawServer);
    int len = BUF_display.length();
    int Pos_Call = BUF_display.indexOf(">");
    int Pos_Info = BUF_display.indexOf(":");
    int Pos_digi = BUF_display.lastIndexOf("*", Pos_Info);
    if (Pos_digi > 0)
    {
        Digied = true;
    }
    String Num_ack = "";

    String RxCall = BUF_display.substring(0, Pos_Call);
    String RxInfo = "";
    Run_Time_SWAP_A = SWAP_delay + 1;
    Run_Time_SWAP_B = 0;

    //*********SAT MODE*******************//
    if (BUF_display.indexOf("ARISS*") > 0 && !Inet)
    {
        Digied = false;
    }
    else if (BUF_display.indexOf("RS0ISS*") > 0 && !Inet)
    {
        Digied = false;
    }
    else if (BUF_display.indexOf("YB0X-1*") > 0 && !Inet)
    {
        Digied = false;
    }
    else if (BUF_display.indexOf("PSAT*") > 0 && !Inet)
    {
        Digied = false;
    }
    else if (BUF_display.indexOf("PSAT2*") > 0 && !Inet)
    {
        Digied = false;
    }
    else if (BUF_display.indexOf("W3ADO-1*") > 0 && !Inet)
    {
        Digied = false;
    }
    else if (BUF_display.indexOf("USNAP1-1*") > 0 && !Inet)
    {
        Digied = false;
    }
    else
    {
        BUF_via = " ";
    }

    //************************************//

    display.clear();
    S_wifi();
#ifdef Two_color //****************จอ 2 สี*******************//
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.setFont(ArialMT_Plain_16);
    if (Digied and RxCall.charAt(0) != '}' and !Inet)
    {
        display.drawString(0, 0, "*" + RxCall);
    } // 0 0 แก้ไขแล้วแสดงคอลซายบรรทัดบนสุดของจอ
    else
    {
        display.drawString(0, 0, RxCall);
    } // 0 0 แก้ไขแล้วแสดงคอลซายบรรทัดบนสุดของจอ
#else //*****************************************//
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.setFont(ArialMT_Plain_16);
    if (Digied and RxCall.charAt(0) != '}' and !Inet)
    {
        display.drawString(0, 10, "*" + RxCall);
    }
    else
    {
        display.drawString(0, 10, RxCall);
    }
#endif

    //String BUF_display=String(RawServer);
    char C_table, C_symbol;
    char DestAddress[10];
    if (BUF_display.charAt(Pos_Info + 1) == '`')
    {
        Pos_Info = Pos_Info + 1;
    } //:`p0!mH+>/`"5E}INFO FACEBOOK SOMPOPE_"
    if (BUF_display.charAt(Pos_Info + 1) == '!')
    {
        Pos_Info = Pos_Info + 2;
    } //:!1455.03N/10329.84E&ESP8266 IGATE
    if (BUF_display.charAt(Pos_Info + 1) == '=')
    {
        Pos_Info = Pos_Info + 2;
    } //:=1455.03N/10329.84E&ESP8266 IGATE
    if (BUF_display.charAt(Pos_Info + 1) == '/')
    {
        Pos_Info = Pos_Info + 9;
    } // :/232059Z/Gb-;h#Yw5+!G/A=000028VIN:12.5V
    if (BUF_display.charAt(Pos_Info + 1) == '@')
    {
        Pos_Info = Pos_Info + 9;
    } //:@240147z1349.38N/10027.54E-https://www.facebook.com/rast.org
    //if(BUF_display.charAt(Pos_Info+1)==';'){Pos_Info=Pos_Info+9;}//:;E24DD  C *021558z1453.34ND10330.41EaRNG0020 2m Voice 145.6125 -0.600 MHz
    if (BUF_display.charAt(Pos_Info + 1) == '_')
    {
        Pos_Info = Pos_Info + 1;
    } // :_00000000c...s...g...t084r...p...P...h99b.....
    //if(BUF_display.charAt(Pos_Info+1)==':'){FWD_RF=true;}else{FWD_RF=false;}
    if (BUF_display.charAt(Pos_Info + 1) == ')') // )1579!1455.04NP10329.85E>000/000/A=000462Parking
    {
        int Pos_itm = BUF_display.indexOf('!');
        Pos_Info = Pos_itm + 1;
    }
    if (BUF_display.charAt(Pos_Info + 1) == ';')
    {
        Pos_Info = Pos_Info + 18;
    } // ;LEADERVVV*092345z4903.50N/07201.75W>088/036
    RxInfo = BUF_display.substring(Pos_Info, len);
    String RxDest = BUF_display.substring(Pos_Call + 1, Pos_Call + 7);
    int Pos_ack = RxInfo.indexOf("{");
    int len_RxInfo = RxInfo.length();
    if (Pos_ack > 0)
    {
        Num_ack = RxInfo.substring(Pos_ack + 1, len_RxInfo);
    }

    if (RxInfo.charAt(4) == '.' && RxInfo.charAt(14) == '.')
    {                               //  !1455.04NP10329.85E>000/000/A=000462Parking
        C_table = RxInfo.charAt(8); //1455.03N/10329.84E&ESP8266 IGATE
        C_symbol = RxInfo.charAt(18);
    }
    if (RxInfo.charAt(0) == '/')
    { //  /Fwqth=G_vrIG  หรือ /Gb-:h#Yu5+!G/A=000031 HDOP00.8 SATS10 VIN:12.5V
        C_table = RxInfo.charAt(0);
        C_symbol = RxInfo.charAt(9);
    }
    if (RxInfo.charAt(0) == '\\')
    { // \FvD'h<rH>r0G
        C_table = RxInfo.charAt(0);
        C_symbol = RxInfo.charAt(9);
    }
    if (RxInfo.charAt(0) == '`')
    { // `p0!mH+>/`"5E}INFO FACEBOOK SOMPOPE_"
        C_table = RxInfo.charAt(8);
        C_symbol = RxInfo.charAt(7);
    }

    if (CMD_debug == true)
    {
        /* serverClients[0].println("Show_frm_Srv");
serverClients[0].println(String(C_table)+String(C_symbol));*/
    }
    if (Page != 1)
    { //ถ้าเป็นหน้าอื่นๆ ที่ไม่ใช่หน้า 1 หน้า Raw  ให้แสดงรูปสัญลักษณ์ตามปกติ HS3LSE 08.44 230918

        if (C_symbol == '&')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_hfgateway);
        }
        else if (C_table == '/' and C_symbol == 'k')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_truck);
        }
        else if (C_table != '/' and C_symbol == '>')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_nocar);
        }
        else if (C_table == '\\' and C_symbol == 'P')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_parking);
        }
        else if (C_symbol == '>')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_car);
        }
        else if (C_table == '/' and C_symbol == '(')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_car);
        }
        else if (C_table == '/' and C_symbol == '_')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_wx);
        }
        else if (C_table == '/' and C_symbol == 'W')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_wx);
        }
        else if (C_table == 'I' and C_symbol == '#')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_istar);
        } // HS3LSE 15.23 01/10/18
        else if (C_symbol == '#')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_digi);
        }
        else if (C_symbol == '[')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_man);
        }
        else if (C_symbol == 'i')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_iota);
        }
        else if (C_symbol == '$')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_phone);
        }
        else if (C_symbol == '/')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_reddot);
        }
        else if (C_table == '\\' and C_symbol == '(')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_cloudy);
        }
        else if (C_table == '/' and C_symbol == '<')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_motorcycle);
        }
        else if (C_table == '/' and C_symbol == '-')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_home);
        }
        else if (C_symbol == 'v')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_van);
        }
        else if (C_table == '/' and C_symbol == 'b')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_bike);
        }
        else if (C_table == '/' and C_symbol == 'a')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_ambulance);
        }
        else if (C_table == '/' and C_symbol == 's')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_powerboat);
        }
        else if (C_table == '/' and C_symbol == 'U')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_bus);
        }
        else if (C_table == '\\' and C_symbol == 'c')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_civdef);
        }
        else if (C_table == '/' and C_symbol == 'f')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_firetruck);
        }
        else if (C_table == '/' and C_symbol == 'X')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_helo);
        }
        else if (C_table == '\\' and C_symbol == '-')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_homehf);
        }
        else if (C_table == '/' and C_symbol == 'h')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_hospital);
        }
        else if (C_table == '/' and C_symbol == 'j')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_jeep);
        }
        else if (C_table == '/' and C_symbol == '\'')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_planesm);
        }
        else if (C_table == '/' and C_symbol == 'P')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_police);
        }
        else if (C_table == '/' and C_symbol == '`')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_dish);
        }
        else if (C_table == '/' and C_symbol == '`')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_rain);
        }
        else if (C_table == '\\' and C_symbol == 'S')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_sat);
        }
        else if (C_table == '\\' and C_symbol == 'U')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_sunny);
        }
        else if (C_table == '\\' and C_symbol == 'k')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_suv);
        }
        else if (C_symbol == 'Q')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_tbd);
        }
        else if (C_table == '/' and C_symbol == 'F')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_tractor);
        }
        else if (C_table == '/' and C_symbol == 'Y')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_yacht);
        }
        else if (C_table == '/' and C_symbol == 'y')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_yagi);
        }
        else if (C_symbol == 'u')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_truck18wh);
        }
        else if (C_table == '/' and C_symbol == '%')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_dx);
        }
        else if (C_table == '/' and C_symbol == '+')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_redcross);
        }
        else if (C_table == '/' and C_symbol == '.')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_x);
        }
        else if (C_table == '/' and C_symbol == '0')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_circle0);
        }
        else if (C_table == '/' and C_symbol == '1')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_circle1);
        }
        else if (C_table == '/' and C_symbol == '2')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_circle2);
        }
        else if (C_table == '/' and C_symbol == '3')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_circle3);
        }
        else if (C_table == '/' and C_symbol == '4')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_circle4);
        }
        else if (C_table == '/' and C_symbol == '5')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_circle5);
        }
        else if (C_table == '/' and C_symbol == '6')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_circle6);
        }
        else if (C_table == '/' and C_symbol == '7')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_circle7);
        }
        else if (C_table == '/' and C_symbol == '8')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_circle8);
        }
        else if (C_table == '/' and C_symbol == '9')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_circle9);
        }
        else if (C_table == '/' and C_symbol == ')')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_weelchair);
        }
        else if (C_table == '/' and C_symbol == '=')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_train);
        }
        else if (C_table == '/' and C_symbol == 'I')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_tcpip);
        }
        else if (C_table == '/' and C_symbol == 'O')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_ballon);
        }
        else if (C_table == '/' and C_symbol == 'K')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_school);
        }
        else if (C_table == '\\' and C_symbol == 'N')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_navbuoy);
        }
        else if (C_table == '/' and C_symbol == 'H')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_hotel);
        }
        else if (C_table == '/' and C_symbol == 'c')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_icp);
        }
        else if (C_table == '/' and C_symbol == 'p')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_rover);
        }
        else if (C_table == '/' and C_symbol == 'r')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_tower);
        } // HS3LSE 15.23 01/10/18
        else if (BUF_display.charAt(Pos_Info + 1) == ':')
        {
            display.drawXbm(X_symbol, Y_symbol, S_width, S_hight, S_msg);
        }
    }

    //---------------------------------------------
    if (Page == 1)
    { //กรณี Raw *************************************

        display.setFont(ArialMT_Plain_10);
#ifdef Two_color //****************จอ 2 สี*******************//
        display.drawHorizontalLine(X_hor, Y_hor, 100);
        display.drawString(0, 14, String(Time_RX)); //17.45   8/03/19   HS6TUX
        if (DigiP)
        {
            display.drawString(50, 14, "Digi >" + String(Dg_cnt));
        }
        else if (Inet)
        {
            display.drawString(50, 14, "TX >" + String(Tx_cnt));
        }
        else
        {
            display.drawString(50, 14, "RX >" + String(Rx_cnt));
        }
#else //****************************************//
        display.drawHorizontalLine(X_hor, Y_hor, 128);
        if (DigiP)
        {
            display.drawString(33, 0, "Digi >" + String(Dg_cnt));
        }
        else if (Inet)
        {
            display.drawString(33, 0, "TX >" + String(Tx_cnt));
        }
        else
        {
            display.drawString(33, 0, "RX >" + String(Rx_cnt));
        }
#endif

        /*
display.drawStringMaxWidth(0, 26, 125,BUF_display);
display.display();
*/
        //------------------------------
        int lenB = BUF_display.length();
        display.drawHorizontalLine(X_hor, Y_hor, 100);
#ifdef Two_color //****************จอ 2 สี*******************//
        // display.drawStringMaxWidth(0, 25, 128,BUF_display);
        display.drawString(0, 25, BUF_display.substring(0, 20));
        if (lenB > 20)
        {
            display.drawString(0, 35, BUF_display.substring(20, 40));
        }
        if (lenB > 39)
        {
            display.drawString(0, 45, BUF_display.substring(40, 60));
        }
        if (lenB > 59)
        {
            display.drawString(0, 54, BUF_display.substring(60, 83));
        } //HS6TUX-21.19-11.10.18//
#else     //****************************************//
        display.drawHorizontalLine(X_hor, Y_hor, 128);
        display.drawString(0, 26, BUF_display.substring(0, 20));
        if (lenB > 20)
        {
            display.drawString(0, 37, BUF_display.substring(20, 40));
        }
        if (lenB > 39)
        {
            display.drawString(0, 48, BUF_display.substring(40, 59));
        }
#endif
        display.display();

        //------------------------------
    }

    //int Pos_SPD = BUF_display.indexOf('/',19);
    else if (C_table == '/' and C_symbol == '_' and RxInfo.charAt(22) == '/') // 1507.15N/10420.05E_.../...g...t080r...p...P...h99b..... WX-IGATE SRISAKET // 1329.95N/10012.10E_112/000g000t093P000h56b10141V122OTW1
    {
        int Pos_t = RxInfo.indexOf('t');
        int Pos_h = RxInfo.indexOf('h');
        int Pos_p = RxInfo.indexOf('b');
        String S_temp = RxInfo.substring(Pos_t + 1, Pos_t + 4);
        int F_temp = S_temp.toInt();
        float C_temp = float(F_temp);
        C_temp = (C_temp - 32) / 1.8;
        String S_hum = RxInfo.substring(Pos_h + 1, Pos_h + 3);
        String S_press = RxInfo.substring(Pos_p + 1, Pos_p + 6);
        int I_press = S_press.toInt() / 10;
        display.setFont(ArialMT_Plain_10);
//display.drawString(33, 0, "IS >Wx");
#ifdef Two_color                                    //****************จอ 2 สี*******************//
        display.drawString(0, 14, String(Time_RX)); //17.45   8/03/19   HS6TUX
        if (DigiP)
        {
            display.drawString(50, 14, "Digi >" + String(Dg_cnt));
        }
        else if (Inet)
        {
            display.drawString(50, 14, "TX >" + String(Tx_cnt));
        }
        else
        {
            display.drawString(50, 14, "RX >" + String(Rx_cnt));
        }
#else //*****************************************//
        if (DigiP)
        {
            display.drawString(33, 0, "Digi >" + String(Dg_cnt));
        }
        else if (Inet)
        {
            display.drawString(33, 0, "TX >" + String(Tx_cnt));
        }
        else
        {
            display.drawString(33, 0, "RX >" + String(Rx_cnt));
        }
#endif

#ifdef Two_color                                       //****************จอ 2 สี*******************//
        display.drawHorizontalLine(X_hor, Y_hor, 100); // HS6TUX 00.09 12/10/18
        display.drawVerticalLine(X_ver, Y_ver, 39);    // HS6TUX 00.09 12/10/18
        String WB = "";
        WB = String(int(C_temp)) + "°c";
        display.setFont(ArialMT_Plain_16);
        display.drawString(0, 47, WB); //display.drawString(0, 37,WB ); // HS3LSE 12.43 01/10/18
        WB = " " + S_hum + " %";
        display.drawString(53, 29, WB); // HS3LSE 12.43 01/10/18
        WB = " " + String(I_press) + " mbar";
        display.drawString(53, 45, WB);            // HS3LSE 12.43 01/10/18
        display.drawXbm(40, 45, 16, 16, Sb_press); // HS3LSE 12.43 01/10/18
        display.drawXbm(40, 28, 16, 16, Sb_hum);   // HS3LSE 12.43 01/10/18
        display.drawXbm(5, 28, 16, 16, Sb_temp);   // HS3LSE 12.43 01/10/18
#else                                              //**************************************//
        display.drawHorizontalLine(X_hor, Y_hor, 128); // HS3LSE 12.43 01/10/18
        //display.drawVerticalLine(X_ver, Y_ver, 38);       // HS3LSE 12.43 01/10/18
        String WB = "";
        WB = String(int(C_temp)) + "°c";
        display.setFont(ArialMT_Plain_16);
        display.drawString(0, 47, WB); //display.drawString(0, 37,WB ); // HS3LSE 12.43 01/10/18
        WB = " " + S_hum + " %";
        display.drawString(53, 29, WB); // HS3LSE 12.43 01/10/18
        WB = " " + String(I_press) + " mbar";
        display.drawString(53, 45, WB);            // HS3LSE 12.43 01/10/18
        display.drawXbm(40, 45, 16, 16, Sb_press); // HS3LSE 12.43 01/10/18
        display.drawXbm(40, 28, 16, 16, Sb_hum);   // HS3LSE 12.43 01/10/18
        display.drawXbm(5, 28, 16, 16, Sb_temp);   // HS3LSE 12.43 01/10/18
#endif

        display.display();
    }
    else if (C_table == '/' and C_symbol == '_') // 1507.15N/10420.05E_c...s...g...t080r...p...P...h99b..... WX-IGATE SRISAKET // 1329.95N/10012.10E_112/000g000t093P000h56b10141V122OTW1
    {
        int Pos_t = RxInfo.indexOf('t');
        int Pos_h = RxInfo.indexOf('h');
        int Pos_p = RxInfo.indexOf('b');
        String S_temp = RxInfo.substring(Pos_t + 1, Pos_t + 4);
        int F_temp = S_temp.toInt();
        float C_temp = float(F_temp);
        C_temp = (C_temp - 32) / 1.8;
        String S_hum = RxInfo.substring(Pos_h + 1, Pos_h + 3);
        String S_press = RxInfo.substring(Pos_p + 1, Pos_p + 6);
        int I_press = S_press.toInt() / 10;
        display.setFont(ArialMT_Plain_10);
//display.drawString(33, 0, "IS >Wx");
#ifdef Two_color                                    //****************จอ 2 สี*******************//
        display.drawString(0, 14, String(Time_RX)); //17.45   8/03/19   HS6TUX
        if (DigiP)
        {
            display.drawString(50, 14, "Digi >" + String(Dg_cnt));
        }
        else if (Inet)
        {
            display.drawString(50, 14, "TX >" + String(Tx_cnt));
        }
        else
        {
            display.drawString(50, 14, "RX >" + String(Rx_cnt));
        }
#else //*****************************************//
        if (DigiP)
        {
            display.drawString(33, 0, "Digi >" + String(Dg_cnt));
        }
        else if (Inet)
        {
            display.drawString(33, 0, "TX >" + String(Tx_cnt));
        }
        else
        {
            display.drawString(33, 0, "RX >" + String(Rx_cnt));
        }
#endif

#ifdef Two_color                                       //****************จอ 2 สี*******************//
        display.drawHorizontalLine(X_hor, Y_hor, 100); // HS6TUX 00.09 12/10/18
        display.drawVerticalLine(X_ver, Y_ver, 39);    // HS6TUX 00.09 12/10/18
        String WB = "";
        WB = String(int(C_temp)) + "°c";
        display.setFont(ArialMT_Plain_16);
        display.drawString(0, 47, WB); //display.drawString(0, 37,WB ); // HS3LSE 12.43 01/10/18
        WB = " " + S_hum + " %";
        display.drawString(53, 29, WB); // HS3LSE 12.43 01/10/18
        WB = " " + String(I_press) + " mbar";
        display.drawString(53, 45, WB);            // HS3LSE 12.43 01/10/18
        display.drawXbm(40, 45, 16, 16, Sb_press); // HS3LSE 12.43 01/10/18
        display.drawXbm(40, 28, 16, 16, Sb_hum);   // HS3LSE 12.43 01/10/18
        display.drawXbm(5, 28, 16, 16, Sb_temp);   // HS3LSE 12.43 01/10/18
#else                                              //**************************************//
        display.drawHorizontalLine(X_hor, Y_hor, 128); // HS3LSE 12.43 01/10/18
        //display.drawVerticalLine(X_ver, Y_ver, 38);       // HS3LSE 12.43 01/10/18
        String WB = "";
        WB = String(int(C_temp)) + "°c";
        display.setFont(ArialMT_Plain_16);
        display.drawString(0, 47, WB); //display.drawString(0, 37,WB ); // HS3LSE 12.43 01/10/18
        WB = " " + S_hum + " %";
        display.drawString(53, 29, WB); // HS3LSE 12.43 01/10/18
        WB = " " + String(I_press) + " mbar";
        display.drawString(53, 45, WB);            // HS3LSE 12.43 01/10/18
        display.drawXbm(40, 45, 16, 16, Sb_press); // HS3LSE 12.43 01/10/18
        display.drawXbm(40, 28, 16, 16, Sb_hum);   // HS3LSE 12.43 01/10/18
        display.drawXbm(5, 28, 16, 16, Sb_temp);   // HS3LSE 12.43 01/10/18
#endif

        display.display();
    }
    else if (RxInfo.charAt(0) == '_')
    { // :_00000000c...s...g...t084r...p...P...h99b.....
        int Pos_t = RxInfo.indexOf('t');
        int Pos_h = RxInfo.indexOf('h');
        int Pos_p = RxInfo.indexOf('b');
        String S_temp = RxInfo.substring(Pos_t + 1, Pos_t + 4);
        int F_temp = S_temp.toInt();
        float C_temp = float(F_temp);
        C_temp = (C_temp - 32) / 1.8;
        String S_hum = RxInfo.substring(Pos_h + 1, Pos_h + 3);
        String S_press = RxInfo.substring(Pos_p + 1, Pos_p + 6);
        int I_press = S_press.toInt() / 10;
        display.setFont(ArialMT_Plain_10);
#ifdef Two_color                                    //****************จอ 2 สี*******************//
        display.drawString(0, 14, String(Time_RX)); //17.45   8/03/19   HS6TUX
        if (DigiP)
        {
            display.drawString(50, 14, "Digi >" + String(Dg_cnt));
        }
        else if (Inet)
        {
            display.drawString(50, 14, "TX >" + String(Tx_cnt));
        }
        else
        {
            display.drawString(50, 14, "RX >" + String(Rx_cnt));
        }
#else
        if (DigiP)
        {
            display.drawString(33, 0, "Digi >" + String(Dg_cnt));
        }
        else if (Inet)
        {
            display.drawString(33, 0, "TX >" + String(Tx_cnt));
        }
        else
        {
            display.drawString(33, 0, "RX >" + String(Rx_cnt));
        }
#endif

#ifdef Two_color                                       //****************จอ 2 สี*******************//
        display.drawHorizontalLine(X_hor, Y_hor, 100); // HS6TUX 00.09 12/10/18
        display.drawVerticalLine(X_ver, Y_ver, 39);    // HS6TUX 00.09 12/10/18
        String WB = "";
        WB = String(int(C_temp)) + "°c";
        display.setFont(ArialMT_Plain_16);
        display.drawString(0, 47, WB); //display.drawString(0, 37,WB ); // HS3LSE 12.43 01/10/18
        WB = " " + S_hum + " %";
        display.drawString(53, 29, WB); // HS3LSE 12.43 01/10/18
        WB = " " + String(I_press) + " mbar";
        display.drawString(53, 45, WB);            // HS3LSE 12.43 01/10/18
        display.drawXbm(40, 45, 16, 16, Sb_press); // HS3LSE 12.43 01/10/18
        display.drawXbm(40, 28, 16, 16, Sb_hum);   // HS3LSE 12.43 01/10/18
        display.drawXbm(5, 28, 16, 16, Sb_temp);   // HS3LSE 12.43 01/10/18
// display.drawXbm(40,45,25,25,S_press);
#else //******************************************//
        display.drawHorizontalLine(X_hor, Y_hor, 128); // HS3LSE 12.43 01/10/18
        display.drawVerticalLine(X_ver, Y_ver, 38);    // HS3LSE 12.43 01/10/18
        String WB = "";
        WB = String(int(C_temp)) + "°c";
        display.setFont(ArialMT_Plain_16);
        display.drawString(0, 47, WB); //display.drawString(0, 37,WB ); // HS3LSE 12.43 01/10/18
        WB = " " + S_hum + " %";
        display.drawString(53, 29, WB); // HS3LSE 12.43 01/10/18
        WB = " " + String(I_press) + " mbar";
        display.drawString(53, 45, WB);            // HS3LSE 12.43 01/10/18
        display.drawXbm(40, 45, 16, 16, Sb_press); // HS3LSE 12.43 01/10/18
        display.drawXbm(40, 28, 16, 16, Sb_hum);   // HS3LSE 12.43 01/10/18
        display.drawXbm(5, 28, 16, 16, Sb_temp);   // HS3LSE 12.43 01/10/18
#endif

        display.display();
    }
    /////------------------กรณีสถานีเคลื่อนที่ เต็ม
    else if (C_symbol != '_' and RxInfo.charAt(4) == '.' and RxInfo.charAt(14) == '.' and RxInfo.charAt(22) == '/') // 1506.45N/10328.54E>329/046Hybrid Tracker
    {

        if (!isDigit(RxInfo.charAt(0)))
            return;
        if (!isDigit(RxInfo.charAt(1)))
            return;
        if (!isDigit(RxInfo.charAt(2)))
            return;
        if (!isDigit(RxInfo.charAt(3)))
            return;

        if (!isDigit(RxInfo.charAt(5)))
            return;
        if (!isDigit(RxInfo.charAt(6)))
            return;
        if (!isAlpha(RxInfo.charAt(7)))
            return;

        if (!isDigit(RxInfo.charAt(9)))
            return;
        if (!isDigit(RxInfo.charAt(10)))
            return;
        if (!isDigit(RxInfo.charAt(11)))
            return;
        if (!isDigit(RxInfo.charAt(12)))
            return;
        if (!isDigit(RxInfo.charAt(13)))
            return;

        if (!isDigit(RxInfo.charAt(15)))
            return;
        if (!isDigit(RxInfo.charAt(16)))
            return;
        if (!isAlpha(RxInfo.charAt(17)))
            return;

        String Alt = "";
        int intAlt = 0;
        int Pos_A = RxInfo.indexOf("A=");
        if (Pos_A > 0)
        {
            if (RxInfo.indexOf("A=00000") > 1)
            {
                Alt = RxInfo.substring((Pos_A) + 2 + 5, (Pos_A) + 2 + 5 + 1);
            }
            else if (RxInfo.indexOf("A=0000") > 1)
            {
                Alt = RxInfo.substring((Pos_A) + 2 + 4, (Pos_A) + 2 + 4 + 2);
            }
            else if (RxInfo.indexOf("A=000") > 1)
            {
                Alt = RxInfo.substring((Pos_A) + 2 + 3, (Pos_A) + 2 + 3 + 3);
            }
            else if (RxInfo.indexOf("A=00") > 1)
            {
                Alt = RxInfo.substring((Pos_A) + 2 + 2, (Pos_A) + 2 + 2 + 4);
            }
            else if (RxInfo.indexOf("A=0") > 1)
            {
                Alt = RxInfo.substring((Pos_A) + 2 + 1, (Pos_A) + 2 + 1 + 5);
            }
            else
            {
                Alt = RxInfo.substring((Pos_A) + 2, (Pos_A) + 2 + 6);
            }
            float fV_Alt = Alt.toInt() / 3.280;
            intAlt = fV_Alt;
        }

        String Bearing = RxInfo.substring(19, 22);
        String S_SPD = RxInfo.substring(23, 26);
        int CRS = Bearing.toInt();
        int SPD = S_SPD.toInt();
        float B_SPD = float(SPD);
        B_SPD = B_SPD * 1.852;
        SPD = int(B_SPD);

        float flat1, flon1, flat2, flon2;
        char LatTrack[8], LonTrack[9];
        float LatDDint, LatMMint, LatSSint, LonDDint, LonMMint, LonSSint, synt;
        RxInfo.toCharArray(LatTrack, 8);

        //Beacon[21] = "!1455.03N/10329.84E&";
        //--------------------------POSITION 2---------------------------------//

        char Latcdn = Beacon[8];
        char Loncdn = Beacon[18];

        LatDDint = ((Beacon[1] - '0') * 10.0) + (Beacon[2] - '0');
        LatMMint = ((Beacon[3] - '0') * 10.0) + (Beacon[4] - '0');
        LatSSint = ((Beacon[6] - '0') * 10.0) + (Beacon[7] - '0');
        LatSSint = (LatSSint / 100.0) * 60.0;
        LonDDint = ((Beacon[10] - '0') * 100.0) + ((Beacon[11] - '0') * 10.0) + (Beacon[12] - '0');
        LonMMint = ((Beacon[13] - '0') * 10.0) + (Beacon[14] - '0');
        LonSSint = ((Beacon[16] - '0') * 10.0) + (Beacon[17] - '0');
        LonSSint = (LonSSint / 100.0) * 60.0;
        if (Latcdn == 'N')
        {
            synt = 1.0;
        }
        else
        {
            synt = -1.0;
        }
        flat2 = (LatDDint + (LatMMint / 60.0) + (LatSSint / 3600.0)) * synt;
        if (Loncdn == 'E')
        {
            synt = 1.0;
        }
        else
        {
            synt = -1.0;
        }
        flon2 = (LonDDint + (LonMMint / 60.0) + (LonSSint / 3600.0)) * synt;
        //--------------------------POSITION 1---------------------------------//

        LatDDint = 0;
        LatMMint = 0;
        LatSSint = 0;
        LonDDint = 0;
        LonMMint = 0;
        LonSSint = 0;
        synt = 0;
        String LN = RxInfo.substring(9, 17);
        LN.toCharArray(LonTrack, 9);
        //--------------------------POSITION 1---------------------------------//
        LN = RxInfo.substring(9, 17);
        LN.toCharArray(LonTrack, 9);
        Latcdn = RxInfo.charAt(7);
        Loncdn = RxInfo.charAt(17);

        LatDDint = ((LatTrack[0] - '0') * 10.0) + (LatTrack[1] - '0');
        LatMMint = ((LatTrack[2] - '0') * 10.0) + (LatTrack[3] - '0');
        LatSSint = ((LatTrack[5] - '0') * 10.0) + (LatTrack[6] - '0');
        LatSSint = (LatSSint / 100.0) * 60.0;
        LonDDint = ((LonTrack[0] - '0') * 100.0) + ((LonTrack[1] - '0') * 10.0) + (LonTrack[2] - '0');
        LonMMint = ((LonTrack[3] - '0') * 10.0) + (LonTrack[4] - '0');
        LonSSint = ((LonTrack[6] - '0') * 10.0) + (LonTrack[7] - '0');
        LonSSint = (LonSSint / 100.0) * 60.0;
        if (Latcdn == 'N')
        {
            synt = 1.0;
        }
        else
        {
            synt = -1.0;
        }
        flat1 = (LatDDint + (LatMMint / 60.0) + (LatSSint / 3600.0)) * synt;
        if (Loncdn == 'E')
        {
            synt = 1.0;
        }
        else
        {
            synt = -1.0;
        }
        flon1 = (LonDDint + (LonMMint / 60.0) + (LonSSint / 3600.0)) * synt;
        //--------------------------POSITION 2---------------------------------//

        //----------------------convert  Position---------------
        float DXKM = ConvPos(flat1, flon1, flat2, flon2);
        int bearing = calc_bearing(flat1, flon1, flat2, flon2);
        //-------------------------------------------------------------

#ifdef Two_color //****************จอ 2 สี*******************//
        display.setFont(ArialMT_Plain_10);
        display.drawString(0, 14, String(Time_RX)); //17.45   8/03/19   HS6TUX
        if (DigiP)
        {
            display.drawString(50, 14, "Digi >" + String(Dg_cnt));
        }
        else if (Inet)
        {
            display.drawString(50, 14, "TX >" + String(Tx_cnt));
        }
        else
        {
            display.drawString(50, 14, "RX >" + String(Rx_cnt));
        }
#else //******************************************//
        display.setFont(ArialMT_Plain_10);
        if (DigiP)
        {
            display.drawString(33, 0, "Digi >" + String(Dg_cnt));
        }
        else if (Inet)
        {
            display.drawString(33, 0, "TX >" + String(Tx_cnt));
        }
        else
        {
            display.drawString(33, 0, "RX >" + String(Rx_cnt));
        }
#endif

#ifdef Two_color //****************จอ 2 สี*******************//
        display.drawHorizontalLine(X_hor, Y_hor, 100);
        display.drawVerticalLine(X_ver, Y_ver, 39);
        display.setFont(ArialMT_Plain_16);
        //display.drawString(45, 29, String(SPD)+" Km/h"); //HS6TUX****///
        //display.drawString(45, 45, String(CRS) +"?" );   //HS6TUX****///
        display.drawString(37, 29, String(SPD) + " Km/h"); //HS6TUX****///
        if (Pos_A > 0)
        {
            display.drawString(37, 45, String(CRS) + "° " + String(intAlt) + "m");
        }
        else
        {
            display.drawString(37, 45, String(CRS) + "°");
        } //HS3LSE// 2021  240918    add ALT        //HS6TUX-20.35-11.10.18//
#else     //*******************************************//
        display.drawHorizontalLine(X_hor, Y_hor, 128);
        display.drawVerticalLine(X_ver, Y_ver, 39);
        display.setFont(ArialMT_Plain_16);
        display.drawString(45, 29, String(SPD) + " Km/h");
        if (Pos_A > 0)
        {
            display.drawString(45, 45, String(CRS) + "° " + String(intAlt) + "m");
        }
        else
        {
            display.drawString(45, 45, String(CRS) + "°");
        }

#endif

        Show_direction(bearing, DXKM);
        Last_KM = DXKM;
        display.display();
        if (!Digied and RxCall.charAt(0) != '}' and !Inet and int(DXKM) > Dx_km and DXKM < 6000)
        {
#ifdef NTP_Serv
            if (String(year()).indexOf("1970") < 0)
            {
                Dx_Call = NTP_DATETIME + " " + String(RxCall) + BUF_via;
                Dx_km = DXKM;
                Dx_br = bearing;
                Dx_Callx = String(RxCall) + BUF_via;
                Dx_km = DXKM;
                Dx_br = bearing;
            } //17.45   8/03/19   HS6TUX
            else
            {
                Dx_Call = String(RxCall) + BUF_via;
                Dx_km = DXKM;
                Dx_br = bearing;
                Dx_Callx = String(RxCall) + BUF_via;
                Dx_km = DXKM;
                Dx_br = bearing;
            } //17.45   8/03/19   HS6TUX
#else
            Dx_Call = String(RxCall) + BUF_via;
            Dx_km = DXKM;
            Dx_br = bearing;
            Dx_Callx = String(RxCall) + BUF_via;
            Dx_km = DXKM;
            Dx_br = bearing;
#endif
#ifdef LINE_Notify
#ifdef LINE_DX_NEW
            if (Read_PWR == 0)
            {
                Serial.print("> Line notify [" + String(MyCallSign) + "] DX:" + Dx_Call + String(Dx_km) + "km " + String(Dx_br) + "°");
                Line_Notify("[" + String(MyCallSign) + "] DX:" + Dx_Call + String(Dx_km) + "km " + String(Dx_br) + "°");
            }
#endif
#endif
        } //16.55 28/10/18 HS3LSE บันทึกDx Station
        if (CMD_debug == true)
        {
            serverClients[0].println(String(bearing) + "° " + String(DXKM));
        }
    }

    /////------------------กรณีสถานีเคลื่อนที่ คอมเพรส
    else if (RxInfo.charAt(0) == '/' or RxInfo.charAt(0) == '\\')
    {
        int CRS = 0;
        int SPD = 0;
        String Alt = "";
        int intAlt = 0;
        int Pos_A = RxInfo.indexOf("A=");
        if (Pos_A > 0)
        {
            if (RxInfo.indexOf("A=00000") > 1)
            {
                Alt = RxInfo.substring((Pos_A) + 2 + 5, (Pos_A) + 2 + 5 + 1);
            }
            else if (RxInfo.indexOf("A=0000") > 1)
            {
                Alt = RxInfo.substring((Pos_A) + 2 + 4, (Pos_A) + 2 + 4 + 2);
            }
            else if (RxInfo.indexOf("A=000") > 1)
            {
                Alt = RxInfo.substring((Pos_A) + 2 + 3, (Pos_A) + 2 + 3 + 3);
            }
            else if (RxInfo.indexOf("A=00") > 1)
            {
                Alt = RxInfo.substring((Pos_A) + 2 + 2, (Pos_A) + 2 + 2 + 4);
            }
            else if (RxInfo.indexOf("A=0") > 1)
            {
                Alt = RxInfo.substring((Pos_A) + 2 + 1, (Pos_A) + 2 + 1 + 5);
            }
            else
            {
                Alt = RxInfo.substring((Pos_A) + 2, (Pos_A) + 2 + 6);
            }
            float fV_Alt = Alt.toInt() / 3.280;
            intAlt = fV_Alt;
        }

        float flat1, flon1, flat2, flon2;
        float LatDDint, LatMMint, LatSSint, LonDDint, LonMMint, LonSSint, synt;

        float LT = ((RxInfo.charAt(1) - 33) * 91.0 * 91.0 * 91.0) + ((RxInfo.charAt(2) - 33) * 91.0 * 91.0) + ((RxInfo.charAt(3) - 33) * 91.0) + (RxInfo.charAt(4) - 33);
        float LN = ((RxInfo.charAt(5) - 33) * 91.0 * 91.0 * 91.0) + ((RxInfo.charAt(6) - 33) * 91.0 * 91.0) + ((RxInfo.charAt(7) - 33) * 91.0) + (RxInfo.charAt(8) - 33);
        flat1 = 90.0 - (LT / 380926.0);
        flon1 = -180.0 + (LN / 190463.0);
        if (RxInfo.charAt(10) != 0x20)
        {
            CRS = (RxInfo.charAt(10) - 33) * 4;
            ;
            SPD = (pow(1.08, (RxInfo.charAt(11) - 33.0)) - 1) * 1.852;
        }

        //Beacon[21] = "!1455.03N/10329.84E&";
        //--------------------------POSITION 2---------------------------------//

        char Latcdn = Beacon[8];
        char Loncdn = Beacon[18];

        LatDDint = ((Beacon[1] - '0') * 10.0) + (Beacon[2] - '0');
        LatMMint = ((Beacon[3] - '0') * 10.0) + (Beacon[4] - '0');
        LatSSint = ((Beacon[6] - '0') * 10.0) + (Beacon[7] - '0');
        LatSSint = (LatSSint / 100.0) * 60.0;
        LonDDint = ((Beacon[10] - '0') * 100.0) + ((Beacon[11] - '0') * 10.0) + (Beacon[12] - '0');
        LonMMint = ((Beacon[13] - '0') * 10.0) + (Beacon[14] - '0');
        LonSSint = ((Beacon[16] - '0') * 10.0) + (Beacon[17] - '0');
        LonSSint = (LonSSint / 100.0) * 60.0;
        if (Latcdn == 'N')
        {
            synt = 1.0;
        }
        else
        {
            synt = -1.0;
        }
        flat2 = (LatDDint + (LatMMint / 60.0) + (LatSSint / 3600.0)) * synt;
        if (Loncdn == 'E')
        {
            synt = 1.0;
        }
        else
        {
            synt = -1.0;
        }
        flon2 = (LonDDint + (LonMMint / 60.0) + (LonSSint / 3600.0)) * synt;

        //----------------------convert  Position---------------
        float DXKM = ConvPos(flat1, flon1, flat2, flon2);
        int bearing = calc_bearing(flat1, flon1, flat2, flon2);
        //-------------------------------------------------------------

#ifdef Two_color //****************จอ 2 สี*******************//
        display.setFont(ArialMT_Plain_10);
        display.drawString(0, 14, String(Time_RX)); //17.45   8/03/19   HS6TUX
        if (DigiP)
        {
            display.drawString(50, 14, "Digi >" + String(Dg_cnt));
        }
        else if (Inet)
        {
            display.drawString(50, 14, "TX >" + String(Tx_cnt));
        }
        else
        {
            display.drawString(50, 14, "RX >" + String(Rx_cnt));
        }
#else //*****************************************//
        display.setFont(ArialMT_Plain_10);
        if (DigiP)
        {
            display.drawString(33, 0, "Digi >" + String(Dg_cnt));
        }
        else if (Inet)
        {
            display.drawString(33, 0, "TX >" + String(Tx_cnt));
        }
        else
        {
            display.drawString(33, 0, "RX >" + String(Rx_cnt));
        }
#endif
#ifdef Two_color //****************จอ 2 สี*******************//
        display.drawHorizontalLine(X_hor, Y_hor, 100);
        display.drawVerticalLine(X_ver, Y_ver, 39);
        display.setFont(ArialMT_Plain_16);
        //display.drawString(45, 29, String(SPD)+" Km/h"); //HS6TUX****///
        //display.drawString(45, 45, String(CRS) +"?" );   //HS6TUX****///
        display.drawString(37, 29, String(SPD) + " Km/h"); //HS6TUX****///
        if (Pos_A > 0)
        {
            display.drawString(37, 45, String(CRS) + "° " + String(intAlt) + "m");
        }
        else
        {
            display.drawString(37, 45, String(CRS) + "°");
        } //HS3LSE// 2021  240918    add ALT         //HS6TUX-20.35-11.10.18//
#else     //****************************************//
        display.drawHorizontalLine(X_hor, Y_hor, 128);
        display.drawVerticalLine(X_ver, Y_ver, 39);
        display.setFont(ArialMT_Plain_16);
        display.drawString(45, 29, String(SPD) + " Km/h");
        if (Pos_A > 0)
        {
            display.drawString(45, 45, String(CRS) + "° " + String(intAlt) + "m");
        }
        else
        {
            display.drawString(45, 45, String(CRS) + "°");
        }
#endif

        Show_direction(bearing, DXKM);
        Last_KM = DXKM;
        display.display();
        if (!Digied and RxCall.charAt(0) != '}' and !Inet and int(DXKM) > Dx_km and DXKM < 6000)
        {
#ifdef NTP_Serv
            if (String(year()).indexOf("1970") < 0)
            {
                Dx_Call = NTP_DATETIME + " " + String(RxCall) + BUF_via;
                Dx_km = DXKM;
                Dx_br = bearing;
                Dx_Callx = String(RxCall) + BUF_via;
                Dx_km = DXKM;
                Dx_br = bearing;
            } //17.45   8/03/19   HS6TUX
            else
            {
                Dx_Call = String(RxCall) + BUF_via;
                Dx_km = DXKM;
                Dx_br = bearing;
                Dx_Callx = String(RxCall) + BUF_via;
                Dx_km = DXKM;
                Dx_br = bearing;
            } //17.45   8/03/19   HS6TUX
#else
            Dx_Call = String(RxCall) + BUF_via;
            Dx_km = DXKM;
            Dx_br = bearing;
            Dx_Callx = String(RxCall) + BUF_via;
            Dx_km = DXKM;
            Dx_br = bearing;
#endif
#ifdef LINE_Notify
#ifdef LINE_DX_NEW
            if (Read_PWR == 0)
            {
                Serial.print("> Line notify [" + String(MyCallSign) + "] DX:" + Dx_Call + String(Dx_km) + "km" + String(Dx_br) + "°");
                Line_Notify("[" + String(MyCallSign) + "] DX:" + Dx_Call + String(Dx_km) + "km" + String(Dx_br) + "°");
            }
#endif
#endif
        } //16.55 28/10/18 HS3LSE บันทึกDx Station
        if (CMD_debug == true)
        {
            serverClients[0].println(String(bearing) + "° " + String(DXKM));
        }
    }

    /////------------------กรณีสถานีเคลื่อนที่ Mic-E-------------------------------------------------//
    else if (RxInfo.charAt(0) == '`') //HS3LSE-9>QTUUP3,WIDE1-1:`o9ql,q>P"5H}MSG to me-92
    {
        int B_micelat[6], B_micelon[3], numDX = 0;
        int CRS = 0;
        int SPD = 0;
        float flat1, flon1, flat2, flon2;
        float LatDDint, LatMMint, LatSSint, LonDDint, LonMMint, LonSSint, synt;
        RxDest.toCharArray(DestAddress, 7);
        for (int getmiceL = 0; getmiceL < 6; getmiceL++)
        {
            if (DestAddress[getmiceL] < 58)
            {
                B_micelat[getmiceL] = DestAddress[getmiceL] - '0';
            }
            else
            {
                B_micelat[getmiceL] = (DestAddress[getmiceL] - 32) - '0';
            }
        }

        B_micelon[0] = int(RxInfo.charAt(1)) - 28;
        if (DestAddress[4] > 64)
        {
            B_micelon[0] = B_micelon[0] + 100;
        }
        if (180 <= B_micelon[0] && B_micelon[0] <= 189)
        {
            B_micelon[0] = B_micelon[0] - 80;
        }
        if (190 <= B_micelon[0] && B_micelon[0] <= 199)
        {
            B_micelon[0] = B_micelon[0] - 190;
        }

        B_micelon[1] = int(RxInfo.charAt(2)) - 28;
        if (B_micelon[1] >= 60)
        {
            B_micelon[1] = B_micelon[1] - 60;
        }

        B_micelon[2] = int(RxInfo.charAt(3)) - 28;
        flat1 = ((B_micelat[0] * 10) + B_micelat[1]) + (((B_micelat[2] * 10) + B_micelat[3]) / 60.0) + (((((B_micelat[4] * 10) + B_micelat[5]) / 100.0) * 60.0) / 3600.0);
        //flat1 =14.91966666666667;
        flon1 = B_micelon[0] + (B_micelon[1] / 60.0) + (((B_micelon[2] / 100.0) * 60.0) / 3600.0);
        //flon1 =103.4988333333333;
        SPD = ((int(RxInfo.charAt(4)) - 28) * 10) + ((int(RxInfo.charAt(5)) - 28) / 10);
        if (SPD >= 800)
        {
            SPD = SPD - 800;
        }
        SPD = SPD * 1.852;
        CRS = (((int(RxInfo.charAt(5)) - 28) % 10) * 100) + (int(RxInfo.charAt(6)) - 28); // micECourse+micECourseunit;
        if (CRS >= 400)
        {
            CRS = CRS - 400;
        }
        int intAlt = 0; //HS3LSE 21.42 240918
        if (RxInfo.charAt(12) == 0x7d)
        {
            intAlt = ((((int(RxInfo.charAt(10)) - 33) * 91) + (int(RxInfo.charAt(11)) - 33)) + ((int(RxInfo.charAt(9)) - 33) * 8281)) - 10000;
        }

        //Beacon[21] = "!1455.03N/10329.84E&";
        //--------------------------POSITION 2---------------------------------//

        char Latcdn = Beacon[8];
        char Loncdn = Beacon[18];

        LatDDint = ((Beacon[1] - '0') * 10.0) + (Beacon[2] - '0');
        LatMMint = ((Beacon[3] - '0') * 10.0) + (Beacon[4] - '0');
        LatSSint = ((Beacon[6] - '0') * 10.0) + (Beacon[7] - '0');
        LatSSint = (LatSSint / 100.0) * 60.0;
        LonDDint = ((Beacon[10] - '0') * 100.0) + ((Beacon[11] - '0') * 10.0) + (Beacon[12] - '0');
        LonMMint = ((Beacon[13] - '0') * 10.0) + (Beacon[14] - '0');
        LonSSint = ((Beacon[16] - '0') * 10.0) + (Beacon[17] - '0');
        LonSSint = (LonSSint / 100.0) * 60.0;
        if (Latcdn == 'N')
        {
            synt = 1.0;
        }
        else
        {
            synt = -1.0;
        }
        flat2 = (LatDDint + (LatMMint / 60.0) + (LatSSint / 3600.0)) * synt;
        if (Loncdn == 'E')
        {
            synt = 1.0;
        }
        else
        {
            synt = -1.0;
        }
        flon2 = (LonDDint + (LonMMint / 60.0) + (LonSSint / 3600.0)) * synt;

        //----------------------convert  Position---------------
        float DXKM = ConvPos(flat1, flon1, flat2, flon2);
        int bearing = calc_bearing(flat1, flon1, flat2, flon2);
        //-------------------------------------------------------------

#ifdef Two_color //****************จอ 2 สี*******************//
        display.setFont(ArialMT_Plain_10);
        display.drawString(0, 14, String(Time_RX)); //17.45   8/03/19   HS6TUX
        if (DigiP)
        {
            display.drawString(50, 14, "Digi >" + String(Dg_cnt));
        }
        else if (Inet)
        {
            display.drawString(50, 14, "TX >" + String(Tx_cnt));
        }
        else
        {
            display.drawString(50, 14, "RX >" + String(Rx_cnt));
        }
        display.drawHorizontalLine(X_hor, Y_hor, 100);
#else //*****************************************//
        display.setFont(ArialMT_Plain_10);
        if (DigiP)
        {
            display.drawString(33, 0, "Digi >" + String(Dg_cnt));
        }
        else if (Inet)
        {
            display.drawString(33, 0, "TX >" + String(Tx_cnt));
        }
        else
        {
            display.drawString(33, 0, "RX >" + String(Rx_cnt));
        }
#endif
#ifdef Two_color //****************จอ 2 สี*******************//
        display.drawHorizontalLine(X_hor, Y_hor, 100);
        display.drawVerticalLine(X_ver, Y_ver, 39);
        display.setFont(ArialMT_Plain_16);
        //display.drawString(45, 29, String(SPD)+" Km/h"); //HS6TUX****///
        //display.drawString(45, 45, String(CRS) +"?" );   //HS6TUX****///
        display.drawString(37, 29, String(SPD) + " Km/h"); //HS6TUX****///
        if (RxInfo.charAt(12) == 0x7d)
        {
            display.drawString(37, 45, String(CRS) + "° " + String(intAlt) + "m");
        }
        else
        {
            display.drawString(37, 45, String(CRS) + "°");
        } //HS6TUX-20.35-11.10.18// HS3LSE 21.42 แก้ if()
#else     //******************************************//
        display.drawHorizontalLine(X_hor, Y_hor, 128);
        display.drawVerticalLine(X_ver, Y_ver, 39);
        display.setFont(ArialMT_Plain_16);
        display.drawString(45, 29, String(SPD) + " Km/h");
        if (RxInfo.charAt(12) == 0x7d)
        {
            display.drawString(45, 45, String(CRS) + "° " + String(intAlt) + "m");
        }
        else
        {
            display.drawString(45, 45, String(CRS) + "°");
        }
#endif

        Show_direction(bearing, DXKM);
        Last_KM = DXKM;
        display.display();
        if (!Digied and RxCall.charAt(0) != '}' and !Inet and int(DXKM) > Dx_km and DXKM < 6000)
        {
#ifdef NTP_Serv
            if (String(year()).indexOf("1970") < 0)
            {
                Dx_Call = NTP_DATETIME + " " + String(RxCall) + BUF_via;
                Dx_km = DXKM;
                Dx_br = bearing;
                Dx_Callx = String(RxCall) + BUF_via;
                Dx_km = DXKM;
                Dx_br = bearing;
            } //17.45   8/03/19   HS6TUX
            else
            {
                Dx_Call = String(RxCall) + BUF_via;
                Dx_km = DXKM;
                Dx_br = bearing;
                Dx_Callx = String(RxCall) + BUF_via;
                Dx_km = DXKM;
                Dx_br = bearing;
            } //17.45   8/03/19   HS6TUX
#else
            Dx_Call = String(RxCall) + BUF_via;
            Dx_km = DXKM;
            Dx_br = bearing;
            Dx_Callx = String(RxCall) + BUF_via;
            Dx_km = DXKM;
            Dx_br = bearing;
#endif
#ifdef LINE_Notify
#ifdef LINE_DX_NEW
            if (Read_PWR == 0)
            {
                Serial.print("> Line notify [" + String(MyCallSign) + "] DX:" + Dx_Call + String(Dx_km) + "km" + String(Dx_br) + "°");
                Line_Notify("[" + String(MyCallSign) + "] DX:" + Dx_Call + String(Dx_km) + "km" + String(Dx_br) + "°");
            }
#endif
#endif
        } //16.55 28/10/18 HS3LSE บันทึกDx Station
        if (CMD_debug == true)
        {
            serverClients[0].println(String(bearing) + "° " + String(DXKM));
        }
    }

    /////-----------------------------------------------------
    /////------------------กรณีสถานีประจำที่ เต็ม--------------------
    else if (RxInfo.charAt(4) == '.' and RxInfo.charAt(14) == '.') // 1506.45N/10328.54E>Hybrid Tracker
    {

        float flat1, flon1, flat2, flon2;
        char LatTrack[8], LonTrack[9];
        float LatDDint, LatMMint, LatSSint, LonDDint, LonMMint, LonSSint, synt;
        RxInfo.toCharArray(LatTrack, 8);

        //Beacon[21] = "!1455.03N/10329.84E&";
        //--------------------------POSITION 2---------------------------------//

        char Latcdn = Beacon[8];
        char Loncdn = Beacon[18];

        LatDDint = ((Beacon[1] - '0') * 10.0) + (Beacon[2] - '0');
        LatMMint = ((Beacon[3] - '0') * 10.0) + (Beacon[4] - '0');
        LatSSint = ((Beacon[6] - '0') * 10.0) + (Beacon[7] - '0');
        LatSSint = (LatSSint / 100.0) * 60.0;
        LonDDint = ((Beacon[10] - '0') * 100.0) + ((Beacon[11] - '0') * 10.0) + (Beacon[12] - '0');
        LonMMint = ((Beacon[13] - '0') * 10.0) + (Beacon[14] - '0');
        LonSSint = ((Beacon[16] - '0') * 10.0) + (Beacon[17] - '0');
        LonSSint = (LonSSint / 100.0) * 60.0;
        if (Latcdn == 'N')
        {
            synt = 1.0;
        }
        else
        {
            synt = -1.0;
        }
        flat2 = (LatDDint + (LatMMint / 60.0) + (LatSSint / 3600.0)) * synt;
        if (Loncdn == 'E')
        {
            synt = 1.0;
        }
        else
        {
            synt = -1.0;
        }
        flon2 = (LonDDint + (LonMMint / 60.0) + (LonSSint / 3600.0)) * synt;
        //--------------------------POSITION 1---------------------------------//

        LatDDint = 0;
        LatMMint = 0;
        LatSSint = 0;
        LonDDint = 0;
        LonMMint = 0;
        LonSSint = 0;
        synt = 0;
        String LN = RxInfo.substring(9, 17);
        LN.toCharArray(LonTrack, 9);
        //--------------------------POSITION 1---------------------------------//
        LN = RxInfo.substring(9, 17);
        LN.toCharArray(LonTrack, 9);
        Latcdn = RxInfo.charAt(7);
        Loncdn = RxInfo.charAt(17);

        LatDDint = ((LatTrack[0] - '0') * 10.0) + (LatTrack[1] - '0');
        LatMMint = ((LatTrack[2] - '0') * 10.0) + (LatTrack[3] - '0');
        LatSSint = ((LatTrack[5] - '0') * 10.0) + (LatTrack[6] - '0');
        LatSSint = (LatSSint / 100.0) * 60.0;
        LonDDint = ((LonTrack[0] - '0') * 100.0) + ((LonTrack[1] - '0') * 10.0) + (LonTrack[2] - '0');
        LonMMint = ((LonTrack[3] - '0') * 10.0) + (LonTrack[4] - '0');
        LonSSint = ((LonTrack[6] - '0') * 10.0) + (LonTrack[7] - '0');
        LonSSint = (LonSSint / 100.0) * 60.0;
        if (Latcdn == 'N')
        {
            synt = 1.0;
        }
        else
        {
            synt = -1.0;
        }
        flat1 = (LatDDint + (LatMMint / 60.0) + (LatSSint / 3600.0)) * synt;
        if (Loncdn == 'E')
        {
            synt = 1.0;
        }
        else
        {
            synt = -1.0;
        }
        flon1 = (LonDDint + (LonMMint / 60.0) + (LonSSint / 3600.0)) * synt;
        //--------------------------POSITION 2---------------------------------//

        //----------------------convert  Position---------------
        float DXKM = ConvPos(flat1, flon1, flat2, flon2);
        int bearing = calc_bearing(flat1, flon1, flat2, flon2);
//-------------------------------------------------------------
#ifdef Two_color //****************จอ 2 สี*******************//
        display.setFont(ArialMT_Plain_10);
        display.drawString(0, 14, String(Time_RX)); //17.45   8/03/19   HS6TUX
        if (DigiP)
        {
            display.drawString(50, 14, "Digi >" + String(Dg_cnt));
        }
        else if (Inet)
        {
            display.drawString(50, 14, "TX >" + String(Tx_cnt));
        }
        else
        {
            display.drawString(50, 14, "RX >" + String(Rx_cnt));
        }
#else //*****************************************//
        display.setFont(ArialMT_Plain_10);
        if (DigiP)
        {
            display.drawString(33, 0, "Digi >" + String(Dg_cnt));
        }
        else if (Inet)
        {
            display.drawString(33, 0, "TX >" + String(Tx_cnt));
        }
        else
        {
            display.drawString(33, 0, "RX >" + String(Rx_cnt));
        }
#endif
#ifdef Two_color //****************จอ 2 สี*******************//
        display.drawHorizontalLine(X_hor, Y_hor, 100);
        display.drawVerticalLine(X_ver, Y_ver, 39);
        display.setFont(ArialMT_Plain_10);
        // display.drawStringMaxWidth(45, 27, 83,RxInfo.substring(19,len)); //HS6TUX****//
        display.drawString(37, 25, RxInfo.substring(19, 29));
        display.drawString(37, 35, RxInfo.substring(29, 39));
        display.drawString(37, 45, RxInfo.substring(39, 55));
        display.drawString(37, 54, RxInfo.substring(55, 71)); //HS6TUX-20.35-11.10.18//

#else //********************************************//
        display.drawHorizontalLine(X_hor, Y_hor, 128);
        display.drawVerticalLine(X_ver, Y_ver, 39);
        display.setFont(ArialMT_Plain_10);
        display.drawStringMaxWidth(45, 27, 83, RxInfo.substring(19, len));
#endif

        Show_direction(bearing, DXKM);
        Last_KM = DXKM;
        display.display();
        if (!Digied and RxCall.charAt(0) != '}' and !Inet and int(DXKM) > Dx_km and DXKM < 6000)
        {
#ifdef NTP_Serv
            if (String(year()).indexOf("1970") < 0)
            {
                Dx_Call = NTP_DATETIME + " " + String(RxCall) + BUF_via;
                Dx_km = DXKM;
                Dx_br = bearing;
                Dx_Callx = String(RxCall) + BUF_via;
                Dx_km = DXKM;
                Dx_br = bearing;
            } //17.45   8/03/19   HS6TUX
            else
            {
                Dx_Call = String(RxCall) + BUF_via;
                Dx_km = DXKM;
                Dx_br = bearing;
                Dx_Callx = String(RxCall) + BUF_via;
                Dx_km = DXKM;
                Dx_br = bearing;
            } //17.45   8/03/19   HS6TUX
#else
            Dx_Call = String(RxCall) + BUF_via;
            Dx_km = DXKM;
            Dx_br = bearing;
            Dx_Callx = String(RxCall) + BUF_via;
            Dx_km = DXKM;
            Dx_br = bearing;
#endif

#ifdef LINE_Notify
#ifdef LINE_DX_NEW
            if (Read_PWR == 0)
            {
                Serial.print("> Line notify [" + String(MyCallSign) + "] DX:" + Dx_Call + String(Dx_km) + "km" + String(Dx_br) + "°");
                Line_Notify("[" + String(MyCallSign) + "] DX:" + Dx_Call + String(Dx_km) + "km" + String(Dx_br) + "°");
            }
#endif
#endif
        } //16.55 28/10/18 HS3LSE บันทึกDx Station
        if (CMD_debug == true)
        {
            serverClients[0].println(String(bearing) + "° " + String(DXKM));
        }
    }

    else
    { //กรณีไม่เข้ากับทุกๆกรณี ให้แสดงเป็น Raw

#ifdef Two_color //****************จอ 2 สี*******************//
        display.drawHorizontalLine(X_hor, Y_hor, 100);
        display.setFont(ArialMT_Plain_10);
        display.drawString(0, 14, String(Time_RX)); //17.45   8/03/19   HS6TUX
        if (DigiP)
        {
            display.drawString(50, 14, "Digi >" + String(Dg_cnt));
        }
        else if (Inet)
        {
            display.drawString(50, 14, "TX >" + String(Tx_cnt));
        }
        else
        {
            display.drawString(50, 14, "RX >" + String(Rx_cnt));
        }
#else //******************************************//
        display.drawHorizontalLine(X_hor, Y_hor, 128);
        display.setFont(ArialMT_Plain_10);
        if (DigiP)
        {
            display.drawString(33, 0, "Digi >" + String(Dg_cnt));
        }
        else if (Inet)
        {
            display.drawString(33, 0, "TX >" + String(Tx_cnt));
        }
        else
        {
            display.drawString(33, 0, "RX >" + String(Rx_cnt));
        }
#endif

//------------------------------
#ifdef Two_color //****************จอ 2 สี*******************//
        display.setFont(ArialMT_Plain_10);
        int lenB = RxInfo.length();
        display.drawHorizontalLine(X_hor, Y_hor, 100);
        // display.drawStringMaxWidth(0, 25, 128,RxInfo);  //HS6TUX 00.18 12/10/18
        display.drawString(0, 25, RxInfo.substring(0, 20));
        if (lenB > 20)
        {
            display.drawString(0, 35, RxInfo.substring(20, 40));
        }
        if (lenB > 39)
        {
            display.drawString(0, 45, RxInfo.substring(40, 60));
        }
        if (lenB > 59)
        {
            display.drawString(0, 54, RxInfo.substring(60, 83)); //HS6TUX-20.35-11.10.18//
        }
#else //**************************************//
        int lenB = RxInfo.length();
        display.drawHorizontalLine(X_hor, Y_hor, 128);
        display.setFont(ArialMT_Plain_10);
        display.drawString(0, 26, RxInfo.substring(0, 20));
        if (lenB > 20)
        {
            display.drawString(0, 37, RxInfo.substring(20, 40));
        }
        if (lenB > 39)
        {
            display.drawString(0, 48, RxInfo.substring(40, 59));
        }
#endif

        //------------------------------
        display.display();
    }

#ifdef ANS_MSG
    String com_mycall = String(MyCallSign) + "         ";
    int len_ANS_call = RxCall.length();
    String ANS_call = "";
    com_mycall = com_mycall.substring(0, 9);
    com_mycall = "::" + com_mycall;

    if (RxCall.charAt(0) == '}')
    {
        ANS_call = RxCall.substring(1, len_ANS_call);
    }
    else
    {
        ANS_call = RxCall;
    }
    String ANS_Info = "RRR";
    if ((RxInfo.indexOf(com_mycall) == 0))
    {

        String income = NTP_DATETIME + "" + String(ANS_call) + " > " + RxInfo;
#ifdef LINE_Notify
#ifdef LINE_MSG_RCV
        if (Read_PWR == 0)
        {
            Serial.print("> Line notify " + income);
            Line_Notify(income);
        }
#endif
#endif
        if (incomeMSG[0].indexOf(income) != 0)
        {
            My_MSG++;
            incomeMSG[4] = incomeMSG[3];
            incomeMSG[3] = incomeMSG[2];
            incomeMSG[2] = incomeMSG[1];
            incomeMSG[1] = incomeMSG[0];
            incomeMSG[0] = income;
        }

        ANS_call = ANS_call + "         ";
        ANS_call = ANS_call.substring(0, 9);
        ANS_call = ":" + ANS_call;

        String Code_ack = ":ack" + Num_ack;
        char ACK_code[10];
        Code_ack.toCharArray(ACK_code, 10);
        int len_ack = ANS_call.length();

        ANS_call.toCharArray(Answer, len_ack + 1);
        if ((RxInfo.indexOf(":ack") < 1) && CHK_MSG)
        {
            CHK_MSG = false;
            if (Num_ack != "")
            {
                delay(3000);
                BUFF = String(Answer) + String(ACK_code);
                SendAPRS(String(VIAPATH));
                String BUFF_NET = String(MyCallSign) + ">" + FW_destNET + ":" + String(Answer) + String(ACK_code);
                client.println(BUFF_NET);
                //client.printf("%s>APESPG:%s%s\r\n",MyCallSign,Answer,ACK_code);
            }
        }
        int in_Num_ack = Num_ack.toInt();
        if (Old_ack == in_Num_ack)
        {
        }
        else
        {
            int len_Info = RxInfo.length();
            String Remote_cmd = RxInfo.substring(RxInfo.indexOf('?'), Pos_ack);
            CMD_REMOTE = true;
            CMD(Remote_cmd, false);
            CMD_REMOTE = false;
        }
        Old_ack = in_Num_ack;
    }
    RxInfo = "";
#endif

    DigiP = false;
}
#endif
/*"(---------------------------------------------------------------------)"*/
/*"(-                            S_WiFi                                  -)"*/
/*"(---------------------------------------------------------------------)"*/
#ifdef OLED
void S_wifi()
{
#ifdef Two_color //****************จอ 2 สี*******************// ย้ายการแสดงรูปWiFiไปไว้ มุมบน ขวาแทน ที่ 96px เลขความแรงสัญญาณที่ 112 px
    if (WiFi.RSSI() > -67)
    {
        display.drawXbm(98, 0, 15, 10, S_wifi3);
    }
    else if (WiFi.RSSI() > -80)
    {
        display.drawXbm(98, 0, 15, 10, S_wifi2);
    }
    else
    {
        display.drawXbm(98, 0, 15, 10, S_wifi1);
    }
    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.setFont(ArialMT_Plain_10);
    display.drawString(128, 0, String(WiFi.RSSI())); //HS3LSE 11.32 27/09/18  display.drawString(115, 0, String(WiFi.RSSI())); //HS6TUX-30092561-0045//
#else                                                //*******************************************//
    if (WiFi.RSSI() > -67)
    {
        display.drawXbm(0, 0, 15, 10, S_wifi3);
    }
    else if (WiFi.RSSI() > -80)
    {
        display.drawXbm(0, 0, 15, 10, S_wifi2);
    }
    else
    {
        display.drawXbm(0, 0, 15, 10, S_wifi1);
    }
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.setFont(ArialMT_Plain_10);
    display.drawString(15, 0, String(WiFi.RSSI()));
#endif
}

#endif

/*"(---------------------------------------------------------------------)"*/
/*"(-      Get The Distance of 2 Point                                  -)"*/
/*"(---------------------------------------------------------------------)"*/

float ConvPos(float Lt1, float Ln1, float Lt2, float Ln2)
{
    float dist_calc = 0.0;
    float dist_calc2 = 0.0;
    float diflat = 0.0;
    float diflon = 0.0;
    diflat = radians(Lt2 - Lt1);
    Lt1 = radians(Lt1);
    Lt2 = radians(Lt2);
    diflon = radians((Ln2) - (Ln1));
    dist_calc = (sin(diflat / 2.0) * sin(diflat / 2.0));
    dist_calc2 = cos(Lt1);
    dist_calc2 *= cos(Lt2);
    dist_calc2 *= sin(diflon / 2.0);
    dist_calc2 *= sin(diflon / 2.0);
    dist_calc += dist_calc2;
    dist_calc = (2 * atan2(sqrt(dist_calc), sqrt(1.0 - dist_calc)));
    dist_calc *= 6371.0; //Converting to Kilometers
    return dist_calc;
}
/*"(---------------------------------------------------------------------)"*/
/*"(-          Get The Bearing of 2 Point                               -)"*/
/*"(---------------------------------------------------------------------)"*/
int calc_bearing(float flt2, float fln2, float flt1, float fln1)
{
    float calc;
    float bear_calc;

    float x = 69.1 * (flt2 - flt1);
    float y = 69.1 * (fln2 - fln1) * cos(flt1 / 57.3);
    calc = atan2(y, x);
    bear_calc = degrees(calc);
    if (bear_calc <= 1)
    {
        bear_calc = 360 + bear_calc;
    }
    return bear_calc;
}

void Screen_page()
{
    Page++;
    if (Page > 10)
    {
        Page = 0;
    }
    if (Page == 1)
    {
        Serial.println("> Page#2 RawPacket");
        display.clear();
        display.setTextAlignment(TEXT_ALIGN_CENTER);
        display.setFont(ArialMT_Plain_16);
        display.drawString(64, 0, "DISPLAY #2");
        display.drawString(64, 30, "RAW Packet");
        display.display();
        delay(1000);
        Show_frm_Srv();
    }
    if (Page == 2)
    {
        Serial.println("> Page#3 ONLY RX");
        display.clear();
        display.setTextAlignment(TEXT_ALIGN_CENTER);
        display.setFont(ArialMT_Plain_16);
        display.drawString(64, 0, "DISPLAY #3");
        display.drawString(64, 30, "ONLY Rx");
        display.display();
        delay(1000);
        Show_frm_Srv();
    }
    if (Page == 3)
    {
        Serial.println("> Page#4 Statistics");
        display.clear();
        display.setTextAlignment(TEXT_ALIGN_CENTER);
        display.setFont(ArialMT_Plain_16);
        display.drawString(64, 0, "DISPLAY #4");
        display.drawString(64, 30, "Statistics");
        display.display();
        delay(1000);
        TxRx_totle();
    }
    if (Page == 4)
    {
        Serial.println("> Page#5 Network Setup");
        display.clear();
        display.setTextAlignment(TEXT_ALIGN_CENTER);
        display.setFont(ArialMT_Plain_16);
        display.drawString(64, 0, "DISPLAY #5");
        display.drawString(64, 30, "Network Setup");
        display.display();
        delay(1000);
        MY_INFO1();
    }
    if (Page == 5)
    {
        Serial.println("> Page#6 Station Setup");
        display.clear();
        display.setTextAlignment(TEXT_ALIGN_CENTER);
        display.setFont(ArialMT_Plain_16);
        display.drawString(64, 0, "DISPLAY #6");
        display.drawString(64, 30, "SETUP Station");
        display.display();
        delay(1000);
        MY_INFO2();
    }
    if (Page == 6)
    {
        Serial.println("> Page#7 SETUP INFO");
        display.clear();
        display.setTextAlignment(TEXT_ALIGN_CENTER);
        display.setFont(ArialMT_Plain_16);
        display.drawString(64, 0, "DISPLAY #7");
        display.drawString(64, 30, "SETUP INFO");
        display.display();
        delay(1000);
        MY_INFO3();
    }
    if (Page == 7)
    {
        Serial.println("> Page#8 WiFi Scan");
        display.clear();
        display.setTextAlignment(TEXT_ALIGN_CENTER);
        display.setFont(ArialMT_Plain_16);
        display.drawString(64, 0, "DISPLAY #8");
        display.drawString(64, 30, "WiFi Scan");
        display.display();
        delay(1000);
        SCAN_WIFI();
    }
    if (Page == 8)
    {
        Serial.println("> Page#9 Tx Packet Test");
        display.clear();
        display.setTextAlignment(TEXT_ALIGN_CENTER);
        display.setFont(ArialMT_Plain_16);
        display.drawString(64, 0, "DISPLAY #8");
        display.drawString(64, 30, "Tx Packet TEST");
        display.display();
        delay(1000);
        Tx_TEST();
    }
    if (Page == 0)
    {
        Serial.println("> Page#1 Decode Packet");
        display.clear();
        display.setTextAlignment(TEXT_ALIGN_CENTER);
        display.setFont(ArialMT_Plain_16);
        display.drawString(64, 0, "DISPLAY #1");
        display.drawString(64, 30, "DecodePacket");
        display.display();
        delay(1000);

        Show_frm_Srv();
    }
    if (Page == 9)
    {
        if (Read_PWR == 0)
        { //ถ้า Read_PWR==0
            Serial.printf("> AP OFF / Please press Button for switch ON \r\n");
            display.clear();
            display.setFont(ArialMT_Plain_24);
            display.setTextAlignment(TEXT_ALIGN_CENTER);
            display.drawString(64, 20, "AP OFF"); //ให้แสดงผลที่ OLED ว่า OFF
            display.display();
        }
        else
        { //ถ้า Read_PWR==1
            Serial.printf("> AP ON / Please press Button for switch OFF\r\n");
            display.clear();
            display.setFont(ArialMT_Plain_24);
            display.setTextAlignment(TEXT_ALIGN_CENTER);
            display.drawString(64, 20, "AP ON"); //ให้แสดงผลที่ OLED ว่า ON
            display.display();
        }
    }
    if (Page == 10)
    {
        State_IGATE = EEPROM.read(511); //อ่านการตั้งค่าในEEPROM แอดเดรส 511 ว่าเคยตั้งค่าเป็น IGATE ON หรือ OFF
        if (State_IGATE == 0)
        { //ถ้า ==0
            // IGOFF_Beep(Buzzer,frq,timeOn);      ////************HS6TUX-16.00-23/07/19
            Serial.printf("> IGATE OFF / Please press Button for switch ON\r\n");
            display.clear();
            display.setFont(ArialMT_Plain_24);
            display.setTextAlignment(TEXT_ALIGN_CENTER);
            display.drawString(64, 20, "IGATEOFF"); //ให้แสดงผลที่ OLED ว่า OFF
            display.display();
            IGATE_ON = false;
            //delay(2000);
        }
        else
        { //ถ้า ==1
            //  IGON_Beep(Buzzer,frq,timeOn);      ////************HS6TUX-16.00-23/07/19
            Serial.printf("> IGATE ON / Please press Button for switch OFF\r\n");
            IGATE_ON = true;
            display.clear();
            display.setFont(ArialMT_Plain_24);
            display.setTextAlignment(TEXT_ALIGN_CENTER);
            display.drawString(64, 20, "IGATE ON"); //ให้แสดงผลที่ OLED ว่า ON
            display.display();
            // delay(2000);
        }
    }
}

void TxRx_totle()
{
    display.clear();
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.setFont(ArialMT_Plain_16);
    display.drawString(64, 0, "HISTORY");
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    long UPT = millis();
    int UPTIME_H = UPT / 3600000;
    int UPTIME_M = (UPT % 3600000) / 60000;
    String U_HH = String(UPTIME_H);
    String U_MM = String(UPTIME_M);
    if (UPTIME_H < 10)
    {
        U_HH = '0' + String(UPTIME_H);
    }
    if (UPTIME_M < 10)
    {
        U_MM = '0' + String(UPTIME_M);
    }
#ifdef Two_color //****************จอ 2 สี*******************//
    /*    display.drawString(0, 11, "IS > RF: " + String(Tx_cnt)); //HS6TUX***************************
display.drawString(0, 21, "RF >IS: " + String(Rx_cnt));
display.drawString(0, 31, "DIGI: " + String(Dg_cnt));
display.drawString(0, 41, "UPTime: " + String(U_HH)+':'+String(U_MM));
display.drawString(0, 51, "FW Ver: " + String(FW_V)); */
    display.drawString(0, 14, "RF > IS: " + String(Rx_cnt));
    display.drawString(0, 24, "IS > RF: " + String(Tx_cnt));
    display.drawString(0, 34, "DIGI: " + String(Dg_cnt));
    display.drawString(0, 44, "UPTime: " + String(U_HH) + ':' + String(U_MM));
    display.drawString(0, 54, "FW Ver: " + String(FW_V)); //HS6TUX-20.35-11.10.18//
#else                                                     //****************************************//
    display.drawString(0, 14, "IS > RF: " + String(Tx_cnt));
    display.drawString(0, 24, "RF >IS: " + String(Rx_cnt));
    display.drawString(0, 34, "DIGI: " + String(Dg_cnt));
    display.drawString(0, 44, "UPTime: " + String(U_HH) + ':' + String(U_MM));
    display.drawString(0, 54, "FW Ver: " + String(FW_V));
#endif

    display.display();
    Serial.println("RF > IS: " + String(Rx_cnt));
    Serial.println("IS > RF: " + String(Tx_cnt));
    Serial.println("DIGI: " + String(Dg_cnt));
    Serial.println("UPTime: " + String(U_HH) + ':' + String(U_MM));
    Serial.println("FW Ver: " + String(FW_V));
}

void SCAN_WIFI()
{
    display.clear();
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.setFont(ArialMT_Plain_10);
    //bool Target_wifi=false;
    String s = "";
    int n = WiFi.scanNetworks();
    if (n == 0)
    {
        s += "no networks found";
        display.drawString(0, 0, s);
        Serial.println(s);
    }
    else
    {
        for (int i = 0; i < n; ++i)
        {
            s = String(i + 1);
            s += ": ";
            s += String(WiFi.SSID(i));
            s += " (";
            s += String(WiFi.RSSI(i));
            s += ")";
            s += String((WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*");
            //s += "<br>";
            if (String(WiFi.SSID(i)) == String(ssid))
            {
                Target_wifi_Start = true;
            }
            display.drawString(0, 13 * (i + 1), s);
            Serial.println(s);
            delay(10);
        }
        if (Target_wifi_Start)
        {
            display.drawString(0, 0, "[" + String(n) + "]" + String(ssid) + " >Found");
            Serial.println("[" + String(n) + "]" + String(ssid) + " >Found");
        }
        else
        {
            display.drawString(0, 0, "[" + String(n) + "]" + String(ssid) + " >Not Found");
            Serial.println("[" + String(n) + "]" + String(ssid) + " >Not Found");
        }
    }
    display.display();
}

void Tx_TEST()
{
    display.clear();
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.setFont(ArialMT_Plain_16);
    display.drawString(0, 30, "Tx Packet TEST");
    unsigned int num_Tx = 0, round_Tx = 0;
    while (digitalRead(Tx_Now) == 1)
    {
        if (millis() - timerecount > 1000)
        {
            num_Tx++;
            display.clear();
            display.setTextAlignment(TEXT_ALIGN_CENTER);
            display.setFont(ArialMT_Plain_16);
            display.drawString(64, 0, "Tx Packet TEST");
            display.drawString(64, 30, String(num_Tx));
            Serial.println(String(num_Tx));
            display.display();
            timerecount = millis();
        }
        if (num_Tx > 9)
        {
            round_Tx++;
            display.clear();
            display.setTextAlignment(TEXT_ALIGN_CENTER);
            display.setFont(ArialMT_Plain_16);
            display.drawString(64, 30, "RF TX > " + String(round_Tx));
            Serial.print("RF TX > " + String(round_Tx));
            display.display();
            //SendKISSRF();TNC_Serial.printf(">RF-Tx TEST %d",round_Tx);TNC_Serial.write(0xC0);TNC_Serial.println("");
            //SendKISSRF();TNC_Serial.printf("%s%s >RF-Tx TEST %d",Beacon,comment,round_Tx);TNC_Serial.write(0xC0);TNC_Serial.println(""); //*3.1d
            BUFF = String(Beacon) + String(comment) + ">RF-Tx TEST " + String(round_Tx);
            Serial.println("> RF Tx | " + BUFF);
            SendAPRS(String(VIAPATH));
            timerecount = millis();
            num_Tx = 0;
        }
    }
}

void MY_INFO1()
{
    display.clear();
#ifdef Two_color //****************จอ 2 สี*******************//
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.setFont(ArialMT_Plain_10);
    /* display.setTextAlignment(TEXT_ALIGN_LEFT);   //HS6TUX*****************
display.setFont(ArialMT_Plain_10);
display.drawString(0, 0, "IP: " + Display_read(0));
display.drawString(0, 11, "Subnet: " + Display_read(21));
display.drawString(0, 21, "Gateway: " + Display_read(41));
display.drawString(0, 31, "DNS: " + Display_read(61));
display.drawString(0, 41, "SSID: " + String(ssid));
display.drawString(0, 51, "Pass: " + String(password)); */
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.setFont(ArialMT_Plain_10);
    display.drawString(0, 4, "IP: " + WiFi.localIP().toString());
    display.drawString(0, 14, "Subnet: " + WiFi.subnetMask().toString());
    display.drawString(0, 24, "Gateway: " + WiFi.gatewayIP().toString());
    display.drawString(0, 34, "DNS: " + WiFi.dnsIP().toString());
    display.drawString(0, 44, "SSID: " + String(ssid));
    display.drawString(0, 54, "Pass: " + String(password)); //HS6TUX-20.35-11.10.18//
#else                                                       //*******************************************//
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.setFont(ArialMT_Plain_10);
    /*
display.drawString(0, 0, "IP: " + Display_read(0));
display.drawString(0, 11, "Subnet: " + Display_read(21));
display.drawString(0, 21, "Gateway: " + Display_read(41));
display.drawString(0, 31, "DNS: " + Display_read(61)); */
    display.drawString(0, 0, "IP: " + WiFi.localIP().toString());
    display.drawString(0, 11, "Subnet: " + WiFi.subnetMask().toString());
    display.drawString(0, 21, "Gateway: " + WiFi.gatewayIP().toString());
    display.drawString(0, 31, "DNS: " + WiFi.dnsIP().toString());
    display.drawString(0, 41, "SSID: " + String(ssid));
    display.drawString(0, 51, "Pass: " + String(password));
#endif
    display.display();
    Serial.println("IP: " + WiFi.localIP().toString());
    Serial.println("Subnet: " + WiFi.subnetMask().toString());
    Serial.println("Gateway: " + WiFi.gatewayIP().toString());
    Serial.println("DNS: " + WiFi.dnsIP().toString());
    Serial.println("SSID: " + String(ssid));
    Serial.println("Pass: " + String(password));
}

void MY_INFO2()
{
    display.clear();
#ifdef Two_color //****************จอ 2 สี*******************//
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.setFont(ArialMT_Plain_10);
    display.drawStringMaxWidth(0, 4, 127, "IS: " + String(APRSIS));
    //display.drawString(0, 0, "IS: " + String(APRSIS));
    display.drawString(0, 14, "Port: " + String(IS_port));
    display.drawString(0, 24, "Passcode: " + String(is_passcode));
    display.drawString(0, 34, "Fil: " + String(JavaFilter));
    display.drawString(0, 44, "TNC Baud Rate: " + String(Brate)); //HS6TUX-20.35-11.10.18//
#else                                                             //******************************************//
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.setFont(ArialMT_Plain_10);
    display.drawStringMaxWidth(0, 0, 127, "IS: " + String(APRSIS));
    //display.drawString(0, 0, "IS: " + String(APRSIS));
    display.drawString(0, 21, "Port: " + String(IS_port));
    display.drawString(0, 31, "Passcode: " + String(is_passcode));
    display.drawString(0, 41, "Fil: " + String(JavaFilter));
    display.drawString(0, 51, "TNC Baud Rate: " + String(Brate));
#endif
    display.display();
    Serial.println("IS: " + String(APRSIS));
    Serial.println("Port: " + String(IS_port));
    Serial.println("Passcode: " + String(is_passcode));
    Serial.println("JAVA Filter: " + String(JavaFilter));
    Serial.println("TNC Baud Rate: " + String(Brate));
}

void MY_INFO3()
{
    display.clear();
#ifdef Two_color //****************จอ 2 สี*******************//
    /* display.setTextAlignment(TEXT_ALIGN_LEFT);  //HS6TUX***************************
display.setFont(ArialMT_Plain_10);
display.drawString(0, 0, "CALL: " +String(MyCallSign));
display.drawString(0, 11, "Path: " + String(VIAPATH));
display.drawString(0, 21,"Bcn: " + String(Beacon));
display.drawString(0, 31, "TXT: " + String(comment));
display.drawString(0, 41, "Every: " + String(interval)+ " min");
display.drawString(0, 51, "Digi Delay: " + String(DigiDelay) + " sec"); */
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.setFont(ArialMT_Plain_16);
    display.drawString(64, 0, String(MyCallSign));
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.setFont(ArialMT_Plain_10);
    display.drawString(0, 14, "Path:" + String(VIAPATH));
    display.drawString(0, 24, "Bn:" + String(Beacon));
    display.drawString(0, 34, "Txt:" + String(comment));
    display.drawString(0, 44, "Every: " + String(interval) + " min");
    display.drawString(0, 54, "Digi Delay: " + String(DigiDelay) + " sec"); //HS6TUX-20.35-11.10.18//
#else                                                                       //******************************************//
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.setFont(ArialMT_Plain_10);
    display.drawString(0, 0, "CALL: " + String(MyCallSign));
    display.drawString(0, 11, "Path: " + String(VIAPATH));
    display.drawString(0, 21, "Bcn: " + String(Beacon));
    display.drawString(0, 31, "TXT: " + String(comment));
    display.drawString(0, 41, "Every: " + String(interval) + " min");
    display.drawString(0, 51, "Digi Delay: " + String(DigiDelay) + " sec");
#endif
    display.display();
    Serial.println("CALL: " + String(MyCallSign));
    Serial.println("Path: " + String(VIAPATH));
    Serial.println("Beacon: " + String(Beacon));
    Serial.println("TXT: " + String(comment));
    Serial.println("Every: " + String(interval) + " min");
    Serial.println("Digi Delay: " + String(DigiDelay) + " sec");
}

void Show_direction(int bearing, float DXKM)
{                //********* แสดงเข็มทิศ และระยะห่างระหว่างสถานี ของทุกๆ การแสดงผล *************//
#ifdef Two_color //****************จอ 2 สี*******************//
    if (bearing < 24)
    {
        display.drawXbm(5, 29, 25, 25, S_compass0);
    } //HS6TUX_23092561-1400//
    else if (bearing < 69)
    {
        display.drawXbm(5, 29, 25, 25, S_compass45);
    }
    else if (bearing < 114)
    {
        display.drawXbm(5, 29, 25, 25, S_compass90);
    }
    else if (bearing < 159)
    {
        display.drawXbm(5, 29, 25, 25, S_compass135);
    }
    else if (bearing < 204)
    {
        display.drawXbm(5, 29, 25, 25, S_compass180);
    }
    else if (bearing < 249)
    {
        display.drawXbm(5, 29, 25, 25, S_compass225);
    }
    else if (bearing < 294)
    {
        display.drawXbm(5, 29, 25, 25, S_compass270);
    }
    else if (bearing < 339)
    {
        display.drawXbm(5, 29, 25, 25, S_compass315);
    }
    else
    {
        display.drawXbm(5, 29, 25, 25, S_compass0);
    }
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.setFont(ArialMT_Plain_10);
    if (DXKM > 10)
    { //HS3LSE 13.53  03/11/18
        int DXKM_int = DXKM;
        display.drawString(17, 54, String(DXKM_int)); //แสดงระยะทาง ระยะห่าง กิโลเมตร   //HS6TUX_11.10.18-2018-เวลาแก้ไขล่าสุด//
    }
    else
    {
        display.drawString(17, 54, String(DXKM));
    }
#else //*********************************************//
    if (bearing < 24)
    {
        display.drawXbm(0, 29, 25, 25, S_compass0);
    }
    else if (bearing < 69)
    {
        display.drawXbm(0, 29, 25, 25, S_compass45);
    }
    else if (bearing < 114)
    {
        display.drawXbm(0, 29, 25, 25, S_compass90);
    }
    else if (bearing < 159)
    {
        display.drawXbm(0, 29, 25, 25, S_compass135);
    }
    else if (bearing < 204)
    {
        display.drawXbm(0, 29, 25, 25, S_compass180);
    }
    else if (bearing < 249)
    {
        display.drawXbm(0, 29, 25, 25, S_compass225);
    }
    else if (bearing < 294)
    {
        display.drawXbm(0, 29, 25, 25, S_compass270);
    }
    else if (bearing < 339)
    {
        display.drawXbm(0, 29, 25, 25, S_compass315);
    }
    else
    {
        display.drawXbm(0, 29, 25, 25, S_compass0);
    }
    display.setFont(ArialMT_Plain_10);
    if (DXKM > 10)
    { //HS3LSE 13.53  03/11/18
        int DXKM_int = DXKM;
        display.drawString(0, 54, String(DXKM_int)); //แสดงระยะทาง ระยะห่าง กิโลเมตร
    }
    else
    {
        display.drawString(0, 54, String(DXKM));
    }
#endif
}

String urlencode(String str)
{
    String encodedString = "";
    char c;
    char code0;
    char code1;
    char code2;
    for (int i = 0; i < str.length(); i++)
    {
        c = str.charAt(i);
        if (c == ' ')
        {
            encodedString += '+';
        }
        else if (isalnum(c))
        {
            encodedString += c;
        }
        else
        {
            code1 = (c & 0xf) + '0';
            if ((c & 0xf) > 9)
            {
                code1 = (c & 0xf) - 10 + 'A';
            }
            c = (c >> 4) & 0xf;
            code0 = c + '0';
            if (c > 9)
            {
                code0 = c - 10 + 'A';
            }
            code2 = '\0';
            encodedString += '%';
            encodedString += code0;
            encodedString += code1;
            //encodedString+=code2;
        }
        yield();
    }
    return encodedString;
}

unsigned char h2int(char c)
{
    if (c >= '0' && c <= '9')
    {
        return ((unsigned char)c - '0');
    }
    if (c >= 'a' && c <= 'f')
    {
        return ((unsigned char)c - 'a' + 10);
    }
    if (c >= 'A' && c <= 'F')
    {
        return ((unsigned char)c - 'A' + 10);
    }
    return (0);
}

String urldecode(String str)
{

    String encodedString = "";
    char c;
    char code0;
    char code1;
    for (int i = 0; i < str.length(); i++)
    {
        c = str.charAt(i);
        if (c == '+')
        {
            encodedString += ' ';
        }
        else if (c == '%')
        {
            i++;
            code0 = str.charAt(i);
            i++;
            code1 = str.charAt(i);
            c = (h2int(code0) << 4) | h2int(code1);
            encodedString += c;
        }
        else
        {

            encodedString += c;
        }

        yield();
    }

    return encodedString;
}

#ifdef LINE_Notify
void Line_Notify(String message)
{
    if (LINE_TOKEN.length() < 40)
        return;
    WiFiClientSecure Lclient;

    if (!Lclient.connect("notify-api.line.me", 443))
    {
        if (CMD_debug)
        {
            Serial.println("connection failed");
            //serverClients[0].println("connection failed");
        }
        return;
    }

    String reqq = "";
    reqq += "POST /api/notify HTTP/1.1\r\n";
    reqq += "Host: notify-api.line.me\r\n";
    reqq += "Authorization: Bearer " + String(LINE_TOKEN) + "\r\n";
    reqq += "Cache-Control: no-cache\r\n";
    reqq += "User-Agent: ESP8266\r\n";
    reqq += "Connection: close\r\n";
    reqq += "Content-Type: application/x-www-form-urlencoded\r\n";
    reqq += "Content-Length: " + String(String("message=" + message).length()) + "\r\n";
    reqq += "\r\n";
    reqq += "message=" + message;
    // Serial.println(reqq);
    Lclient.print(reqq);

    delay(200);
    Lclient.stop();
    reqq = "";
    message = "";
    /*
// Serial.println("-------------");
while(Lclient.connected()) {
String line = Lclient.readStringUntil('\n');
if (line == "\r") {
break;
}
//Serial.println(line);
}
// Serial.println("-------------");
*/
}
#endif

void handleRoot()
{ //************************************************************home page*******************************************//
    if (IGATE_ON == true && (WiFi.status() != WL_CONNECTED))
    {
        Config_Interrupt = true;
        //IGATE_ON=false;
        WiFi.disconnect();
        Serial.printf("> IGATE OFF for Web Config \r\n");
    }
    InitEEPROM();

    int Rain_Value = 1;
#ifdef Rain_s
    Rain_Value = digitalRead(Rain_Pin);
#endif
    // Prepare the response
    String s_ip = String(WiFi.localIP()[0]) + "." + String(WiFi.localIP()[1]) + "." + String(WiFi.localIP()[2]) + "." + String(WiFi.localIP()[3]);

    String s = "<html>\r\n";
    s += "<head>";
    s += "<meta charset=\"UTF-8\">\r\n<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n";
    s += "</head><body>";
    s += "<p style=\"font-family:Arial; color: #5e9ca0;\"><a href=\"../\">SETUP</a>&nbsp;|&nbsp;<a href=\"http://" + s_ip + "/update\">" + s_ip + "/update</a>&nbsp; |&nbsp;<a href=\"http://192.168.4.1/update\">192.168.4.1/update</a> &nbsp;|&nbsp;";
    s += "<a href=\"call\">Message</a>&nbsp; | &nbsp;<a href=\"status\">Status</a> &nbsp;|</p>";
    s += "<h1 style=\"font-family:Arial; color: #5e9ca0;\">" + String(MyCallSign) + "<br>Indy WiFiIGATE V." + FW_V + "</h1>";
    //s += "<h3 style=\"font-family:Arial; color: #2e6c80;\">Firmware UPDATE> <a href=\"http://" + s_ip +":81/update\">"  + s_ip +":81/update</a> or<a href=\"http://192.168.4.1:81/update\">192.168.4.1:81/update</a></h3>";
    //s += "<h3 style=\"font-family:Arial; color: #2e6c80;\">Page > <a href=\"\?call\">MSG</a></h3>";
    s += "<h2 style=\"font-family:Arial; color: #2e6c80;\">Your Config:</h2><h3 style=\"font-family:Arial; color: #2e6c80;\">";
    if (IGATE_ON)
    {
        s += "IGATE Status = ON : Automatic reconnect WiFi and APRS Server <br>";
    }
    else
    {
        s += "IGATE Status = OFF <br>";
    }
    s += "Your IP = " + s_ip + "<br>";
    s += "Your Subnet = " + String(WiFi.subnetMask()[0]) + "." + String(WiFi.subnetMask()[1]) + "." + String(WiFi.subnetMask()[2]) + "." + String(WiFi.subnetMask()[3]) + "<br>";
    s += "Your Gateway = " + String(WiFi.gatewayIP()[0]) + "." + String(WiFi.gatewayIP()[1]) + "." + String(WiFi.gatewayIP()[2]) + "." + String(WiFi.gatewayIP()[3]) + "<br>";
    s += "Your DNS = " + String(WiFi.dnsIP()[0]) + "." + String(WiFi.dnsIP()[1]) + "." + String(WiFi.dnsIP()[2]) + "." + String(WiFi.dnsIP()[3]) + "<br>";
    if ((WiFi.status() != WL_CONNECTED))
    {
        s += "SSID :" + String(ssid) + " >Fail!! กรุณาตวจสอบ [SSID] หรือ [Password]<br>";
    }
    else
    {
        s += "SSID :" + String(ssid) + " >Connected OK<br>";
    }
    if (!client.connected())
    {
        s += "Server: " + String(APRSIS) + " >Fail!!<br>";
    }
    else
    {
        s += "Server: " + String(APRSIS) + " >Connected OK<br>";
    }

    long UPT = millis();
    unsigned int UPTIME_H = UPT / 3600000;
    unsigned int UPTIME_M = (UPT % 3600000) / 60000;
    String U_HH = String(UPTIME_H);
    String U_MM = String(UPTIME_M);
    if (UPTIME_H < 10)
    {
        U_HH = '0' + String(UPTIME_H);
    }
    if (UPTIME_M < 10)
    {
        U_MM = '0' + String(UPTIME_M);
    }
    s += "UP Time = " + U_HH + ":" + U_MM + "<br>";
    s += "WiFi loss " + String(wifi_loss) + " , APRS-IS loss " + String(Inet_loss) + "<br>";
    s += "RF Rx = " + String(Rx_cnt) + " , Tx =" + String(Tx_cnt) + "<br>";
    if (Show_t != 0)
    {
        s += "<br>Wx Temp = " + String(Show_t) + " °C";
    }
    if (Show_h != 0)
    {
        s += "<br>Wx Humedity = " + String(Show_h) + " %";
    }
    if (Show_p != 0)
    {
        s += "<br>Wx Pressure = " + String(Show_p) + " mbar";
    }
#ifdef LDR
    int V_LDR = LDR_Read();
    s += "<br>LDR Value = " + String(V_LDR);
    if (V_LDR > change_symbol_LDR)
    {
        s += "&nbsp;Sunny";
    }
    else if (V_LDR > 5)
    {
        s += "&nbsp;Cloudy";
    }
    else
    {
        s += "&nbsp;Night";
    }

#endif
    s += "<br>Rain Value = " + String(Rain_Value);
    if (Rain_Value == 1)
    {
        s += "&nbsp;Normal";
    }
    else
    {
        s += "&nbsp;Rainy";
    }
    s += "<br><form action=\"config\" method=\"get\"><p align=\"left\"><button name=\"600\" type=\"submit\" value=\"1\" >Restart</button></p>";
    s += "<form action=\"config\" method=\"get\">";
    s += "<input type=\"radio\" name=\"511\" id=\"ON\" value=\"1\"";
    if (IGATE_ON)
    {
        s += " checked=\"checked\"";
    }
    s += "> IGATE ON (For IGATE and Auto reconnect)<br>";
    s += "<input type=\"radio\" name=\"511\" id=\"OFF\" value=\"0\"";
    if (!IGATE_ON)
    {
        s += " checked=\"checked\"";
    }
    s += "> IGATE OFF (For Digi only or no WiFi Connection)<br>";
    s += "</h3><p><form action=\"config\" method=\"get\"><p>[IP IGATE] ex:192.168.1.200";
    String s_buff = String(local_ip[0]) + "." + String(local_ip[1]) + "." + String(local_ip[2]) + "." + String(local_ip[3]);
    s += "&nbsp;<br><input type=\"text\" name=\"wa\" value=\"" + s_buff + "\" maxlength=\"15\"><p>[Subnet mask] ex:255.255.255.0";
    s_buff = String(subnet[0]) + "." + String(subnet[1]) + "." + String(subnet[2]) + "." + String(subnet[3]);
    //s_buff = String(WiFi.subnetMask()[0]) + "." + String(WiFi.subnetMask()[1]) + "." +  String(WiFi.subnetMask()[2]) + "." +  String(WiFi.subnetMask()[3]);//s += EP_read_Config(21);
    s += "&nbsp;<br> <input type=\"text\" name=\"wb\" value=\"" + s_buff + "\" maxlength=\"15\"><p>[Gateway] ex:192.168.1.1";
    s_buff = String(gateway[0]) + "." + String(gateway[1]) + "." + String(gateway[2]) + "." + String(gateway[3]);
    //s_buff = String(WiFi.gatewayIP()[0]) + "." + String(WiFi.gatewayIP()[1]) + "." +  String(WiFi.gatewayIP()[2]) + "." +  String(WiFi.gatewayIP()[3]);//s += EP_read_Config(41);
    s += "&nbsp;<br> <input type=\"text\" name=\"wc\" value=\"" + s_buff + "\" maxlength=\"15\"><p>[DNS] ex:192.168.1.1";
    s_buff = String(dnsip[0]) + "." + String(dnsip[1]) + "." + String(dnsip[2]) + "." + String(dnsip[3]);
    //s_buff = String(WiFi.dnsIP()[0]) + "." + String(WiFi.dnsIP()[1]) + "." +  String(WiFi.dnsIP()[2]) + "." +  String(WiFi.dnsIP()[3]);//s += EP_read_Config(61);    //HS6TUX_23092561-1053//
    s += "&nbsp;<br>";
    s += "<input type=\"text\" name=\"wd\" value=\"" + s_buff + "\" maxlength=\"15\"><p>[SSID WiFi] ex:MYHOME";
    s_buff = String(ssid);
    s += "&nbsp;<br> <input type=\"text\" name=\"we\" value=\"" + s_buff + "\" maxlength=\"19\"><p>[Password WiFi] ex:123456789";
    s_buff = String(password);
    s += "&nbsp;<br><input type=\"text\" name=\"wf\" value=\"" + s_buff + "\" maxlength=\"19\"><p>[APRS Server] ex:aprsth.nakhonthai.net";
    s_buff = String(APRSIS);
    s += "&nbsp;<br> <input type=\"text\" name=\"wg\" value=\"" + s_buff + "\" maxlength=\"29\"><p>[Server Port] ex:14580";
    s_buff = String(IS_port);
    s += "&nbsp;<br> <input type=\"text\" name=\"wh\" value=\"" + s_buff + "\" maxlength=\"6\"><p>[MyCall IGATE] ex:HS3LSE";
    s_buff = String(MyCallSign);
    s += "&nbsp;<br> <input type=\"text\" name=\"wi\" value=\"" + s_buff + "\" maxlength=\"19\">"; //<p>[Passcode] ex:000000" ;
    //s_buff=String(is_passcode);
    //s +="&nbsp;<br><input type=\"text\" name=\"wj\" value=\""+ s_buff +"\" maxlength=\"5\">
    s += "<p>[JAVA Filter] ex:g/HS*/E2*";
    s_buff = String(JavaFilter);
    s += "&nbsp;<br><input type=\"text\" name=\"wk\" value=\"" + s_buff + "\" maxlength=\"69\"><p>[Beacon] ex:=1452.98N/10329.60E#";
    s_buff = String(Beacon);
    s += "&nbsp;<br><input type=\"text\" name=\"wl\" value=\"" + s_buff + "\" maxlength=\"20\"><p>[Comment Text] ex:MY APRS IGATE";
    s_buff = String(comment);
    s += "&nbsp;<br><input type=\"text\" name=\"wm\" value=\"" + s_buff + "\" maxlength=\"29\"><p>[Interval Beacon] ex:30";
    s_buff = String(interval);
    s += "&nbsp;<br><input type=\"text\" name=\"wn\" value=\"" + s_buff + "\" maxlength=\"2\"><p>[TNC Baud Rate] ex:19200";
    s_buff = String(Brate);
    s += "&nbsp;<br><input type=\"text\" name=\"wo\" value=\"" + s_buff + "\" maxlength=\"5\"><p>[Send RF Via Path] ex:WIDE1-1";
    s_buff = String(VIAPATH);
    s += "&nbsp;<br><input type=\"text\" name=\"wp\" value=\"" + s_buff + "\" maxlength=\"19\"><p>[DIGI Delay [0=OFF] ex:0";
    s_buff = String(DigiDelay);
    s += "&nbsp;<br><input type=\"text\" name=\"wq\" value=\"" + s_buff + "\" maxlength=\"1\"><p>[AUTO Restart every [0-255 Hour] ex:0";
    s_buff = Read_ResetOnTime;
    s += "&nbsp;<br><input type=\"text\" name=\"wr\" value=\"" + s_buff + "\" maxlength=\"3\">";
    s += "<p>[LINE Token] ex:";
    s_buff = LINE_TOKEN;
    s += "&nbsp;<br><input type=\"text\" name=\"wt\" value=\"" + s_buff + "\" maxlength=\"45\">";
    s += "<p><input type=\"submit\" value=\"SAVE All\"></form>";
    s += "</body></html>\n";
    httpServer.send(200, "text/html", s);
    Config_Interrupt = false;
}

void handleConfigArg()
{ //************************************************************config page*******************************************//
    if (IGATE_ON == true && (WiFi.status() != WL_CONNECTED))
    {
        Config_Interrupt = true;
        //IGATE_ON=false;
        WiFi.disconnect();
        Serial.printf("> IGATE OFF for Web Config \r\n");
    }
    //https://techtutorialsx.com/2016/10/22/esp8266-webserver-getting-query-parameters/
    //String s = httpServer.argName(0) + " = " + httpServer.arg(0)  ;
    // web_config(httpServer.argName(0).toInt(),httpServer.arg(0));

    String Command_config; //= httpServer.argName(0);
    String Value_config;   //= httpServer.arg(0);
    String Raw_Value_config;
    int num_arg = httpServer.args();

    for (int i = 0; i < num_arg; i++)
    {
        Command_config = httpServer.argName(i);
        Raw_Value_config = httpServer.arg(i);
        Value_config = urldecode(Raw_Value_config);
        if (Command_config == "wa")
        {
            CMD("?wa?" + Value_config, CMD_debug);
        }
        else if (Command_config == "wb")
        {
            CMD("?wb?" + Value_config, CMD_debug);
        }
        else if (Command_config == "wc")
        {
            CMD("?wc?" + Value_config, CMD_debug);
        }
        else if (Command_config == "wd")
        {
            CMD("?wd?" + Value_config, CMD_debug);
        }
        else if (Command_config == "we")
        {
            CMD("?we?" + Value_config, CMD_debug);
        }
        else if (Command_config == "wf")
        {
            CMD("?wf?" + Value_config, CMD_debug);
        }
        else if (Command_config == "wg")
        {
            CMD("?wg?" + Value_config, CMD_debug);
        }
        else if (Command_config == "wh")
        {
            CMD("?wh?" + Value_config, CMD_debug);
        }
        else if (Command_config == "wi")
        {
            CMD("?wi?" + Value_config, CMD_debug);
        }
        else if (Command_config == "wj")
        {
            CMD("?wj?" + Value_config, CMD_debug);
        }
        else if (Command_config == "wk")
        {
            CMD("?wk?" + Value_config, CMD_debug);
        }
        else if (Command_config == "wl")
        {
            CMD("?wl?" + Value_config, CMD_debug);
        }
        else if (Command_config == "wm")
        {
            CMD("?wm?" + Value_config, CMD_debug);
        }
        else if (Command_config == "wn")
        {
            CMD("?wn?" + Value_config, CMD_debug);
        }
        else if (Command_config == "wo")
        {
            CMD("?wo?" + Value_config, CMD_debug);
        }
        else if (Command_config == "wp")
        {
            CMD("?wp?" + Value_config, CMD_debug);
        }
        else if (Command_config == "wq")
        {
            CMD("?wq?" + Value_config, CMD_debug);
        }
        else if (Command_config == "wr")
        {
            CMD("?wr?" + Value_config, CMD_debug);
        }
        else if (Command_config == "ws")
        {
            CMD("?ws?" + Value_config, CMD_debug);
        }
        else if (Command_config == "wt")
        {
            CMD("?wt?" + Value_config, CMD_debug);
        }
        else if (Command_config == "wu")
        {
            CMD("?wu?" + Value_config, CMD_debug);
        }
        else if (Command_config == "wv")
        {
            CMD("?wv?" + Value_config, CMD_debug);
        }
        else if (Command_config == "ww")
        {
            CMD("?ww?" + Value_config, CMD_debug);
        }
        else if (Command_config == "wx")
        {
            CMD("?wx?" + Value_config, CMD_debug);
        }
        else if (Command_config == "wy")
        {
            CMD("?wy?" + Value_config, CMD_debug);
        }
        else if (Command_config == "wz")
        {
            CMD("?wz?" + Value_config, CMD_debug);
        }
        else
        {
            if (isDigit(Command_config.charAt(0)))
            {
                web_config(Command_config.toInt(), Raw_Value_config);
            }
        }
    }

    String s = "<html>\r\n";
    s += "<head>";
    s += "<meta charset=\"UTF-8\">\r\n<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n";
    s += "</head><body>";
    s += "<h1 style=\"text-align: center;\"><span style=\"color: #5e9ca0;\">SAVE OK</span></h1><br><form action=\"../\"><p align=\"center\"><input type=\"submit\" value=\"BACK\"></p></form>";
    s += "</body></html>\n";
    httpServer.send(200, "text/html", s);
    Config_Interrupt = false;
}

void web_config(int eep, String url_config)
{ //*******************************************************************************************************//

    String Org_value = url_config = urldecode(url_config);
    url_config = "????" + url_config; //มี ???? เพื่อ ใช้เว้นคำสั่งเดียวกันกับ ?wa? 4อักษร
    int len = url_config.length();
    char web_val[50];
    url_config.toCharArray(web_val, len);
    if (eep == 506)
    {
        int V_restart = Org_value.toInt();
        EEPROM.write(506, V_restart);
        EEPROM.commit();
        delay(20);
    }
    else if (eep == 505)
    {
        int V_wifi = Org_value.toInt();
        EEPROM.write(505, V_wifi);
        EEPROM.commit();
        delay(20);
    }
    else if (eep == 508)
    {
        int V_KM = Org_value.toInt();
        EEPROM.write(508, V_KM);
        EEPROM.commit();
        delay(20);
    }
    else if (eep == 507)
    {
        int V_Vin = Org_value.toInt();
        if (V_Vin == 1)
        {
            EEPROM.write(507, 1);
            EEPROM.commit();
            delay(20);
        }
        else
        {
            EEPROM.write(507, 0);
            EEPROM.commit();
            delay(20);
        }
    }
    else if (eep == 509)
    {
        int8_t offset = 0;
        if (Org_value.charAt(0) == '-')
        {
            Org_value.remove(0, 1);
            offset = 0 - Org_value.toInt();
        }
        else
        {
            offset = Org_value.toInt();
        }
        EEPROM.write(509, offset);
        EEPROM.commit();
        delay(20);
    }
    else if (eep == 510)
    {
        int W_PWR = Org_value.toInt();
        EEPROM.write(510, W_PWR);
        EEPROM.commit();
        delay(20);
    }
    else if (eep == 511)
    {
        int ST_WIFI = Org_value.toInt();
        EEPROM.write(511, ST_WIFI);
        EEPROM.commit();
        delay(20);
    }
    else if (eep == 600)
    {
        ESP.restart();
    }
    else
    {
        EP_write_char(eep, url_config);
    }
}

void handleStatusArg()
{ //************************************************************status page*******************************************//
    if (IGATE_ON == true && (WiFi.status() != WL_CONNECTED))
    {
        Config_Interrupt = true;
        //IGATE_ON=false;
        WiFi.disconnect();
        Serial.printf("> IGATE OFF for Web Config \r\n");
    }
    String s_ip = String(WiFi.localIP()[0]) + "." + String(WiFi.localIP()[1]) + "." + String(WiFi.localIP()[2]) + "." + String(WiFi.localIP()[3]);
    String s = "<html>\r\n";
    s += "<head>";
    s += "<meta charset=\"UTF-8\">\r\n<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"><meta http-equiv=\"refresh\" content=\"20\">\r\n";
    s += "</head><body>";
    s += "<p style=\"font-family:Arial; color: #5e9ca0;\"><a href=\"../\">SETUP</a>&nbsp;|&nbsp;<a href=\"http://" + s_ip + "/update\">" + s_ip + "/update</a>&nbsp; |&nbsp;<a href=\"http://192.168.4.1/update\">192.168.4.1/update</a> &nbsp;|&nbsp;";
    s += "<a href=\"call\">Message</a>&nbsp; | &nbsp;<a href=\"status\">Status</a> &nbsp;|</p>";
    s += "<h2 style=\"font-family:Arial; color: #2e6c80;\">Status</h2><h3 style=\"font-family:Arial; color: #2e6c80;\">";

    int n = WiFi.scanNetworks();
    if (n == 0)
    {
        s += "no networks found <br>";
    }
    else
    {
        s += String(n) + " networks found <br>";
    }
    for (int i = 0; i < n; ++i)
    {

        s += String(i + 1);
        s += ": ";
        s += String(WiFi.SSID(i));
        s += " (";
        s += String(WiFi.RSSI(i));
        s += ")";
        s += String((WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*");
        s += "<br>";
        delay(10);
    }

    if (IGATE_ON)
    {
        s += "<br>IGATE Status = ON : Automatic reconnect WiFi and APRS Server <br>";
    }
    else
    {
        s += "IGATE Status = OFF <br>";
    }
    s += "Your IP = " + s_ip + "<br>";
    s += "Your Subnet = " + String(WiFi.subnetMask()[0]) + "." + String(WiFi.subnetMask()[1]) + "." + String(WiFi.subnetMask()[2]) + "." + String(WiFi.subnetMask()[3]) + "<br>";
    s += "Your Gateway = " + String(WiFi.gatewayIP()[0]) + "." + String(WiFi.gatewayIP()[1]) + "." + String(WiFi.gatewayIP()[2]) + "." + String(WiFi.gatewayIP()[3]) + "<br>";
    s += "Your DNS = " + String(WiFi.dnsIP()[0]) + "." + String(WiFi.dnsIP()[1]) + "." + String(WiFi.dnsIP()[2]) + "." + String(WiFi.dnsIP()[3]) + "<br>";

    if ((WiFi.status() != WL_CONNECTED))
    {
        s += "SSID :" + String(ssid) + " >Fail!! กรุณาตวจสอบ [SSID] หรือ [Password]<br>";
    }
    else
    {
        s += "SSID :" + String(ssid) + " >Connected OK<br>";
    }
    if (!client.connected())
    {
        s += "Server: " + String(APRSIS) + " >Fail!!<br>";
    }
    else
    {
        s += "Server: " + String(APRSIS) + " >Connected OK<br>";
    }

    long UPT = millis();
    unsigned int UPTIME_H = UPT / 3600000;
    unsigned int UPTIME_M = (UPT % 3600000) / 60000;
    String U_HH = String(UPTIME_H);
    String U_MM = String(UPTIME_M);
    if (UPTIME_H < 10)
    {
        U_HH = '0' + String(UPTIME_H);
    }
    if (UPTIME_M < 10)
    {
        U_MM = '0' + String(UPTIME_M);
    }
    s += "UP Time = " + U_HH + ":" + U_MM + "<br>";
    s += "WiFi loss " + String(wifi_loss) + " , APRS-IS loss " + String(Inet_loss) + "<br>";
    s += "RF Rx = " + String(Rx_cnt) + " , Tx =" + String(Tx_cnt) + "<br>";
    s += "--------------Last " + String(interval) + " Minute--------------<br>";
    if (Show_t != 0)
    {
        s += "<br>Wx Temp = " + String(Show_t) + " °C";
    }
    if (Show_h != 0)
    {
        s += "<br>Wx Humedity = " + String(Show_h) + " %";
    }
    if (Show_p != 0)
    {
        s += "<br>Wx Pressure = " + String(Show_p) + " mbar";
    }
#ifdef LDR
    int V_LDR = LDR_Read();
    s += "<br><br>--------------Realtime--------------<br>";
    s += "<br>LDR Value = " + String(V_LDR);
    if (V_LDR > change_symbol_LDR)
    {
        s += "&nbsp;Sunny";
    }
    else if (V_LDR > 5)
    {
        s += "&nbsp;Cloudy";
    }
    else
    {
        s += "&nbsp;Night";
    }

#endif
#ifdef Rain_s
    int Rain_Value = 1;
    Rain_Value = digitalRead(Rain_Pin);

    s += "<br>Rain Value = " + String(Rain_Value);
    if (Rain_Value == 1)
    {
        s += "&nbsp;Normal";
    }
    else
    {
        s += "&nbsp;Rainy";
    }
#endif
    s += "</h3>";
    s += "<br><p style=\"font-family:Arial; color: #5e9ca0;\">--------------Last Packet on RF--------------<br>";
    s += Last_Packet[0] + "<br>";
    s += Last_Packet[1] + "<br>";
    s += Last_Packet[2] + "<br>";
    s += Last_Packet[3] + "<br>";
    s += Last_Packet[4] + "<br>";
    s += "</p><br><p style=\"font-family:Arial; color: #5e9ca0;\">--------------DX Station--------------<br>";
    String DX_print = " DX:" + Dx_Call + String(Dx_km) + "km " + String(Dx_br) + "°";
    s += DX_print;
    s += "</p><br><p style=\"font-family:Arial; color: #5e9ca0;\">--------------Free RAM--------------<br>";
    s += String(ESP.getFreeHeap());
    s += "</p><p style=\"font-family:Arial; color: #5e9ca0;\">Auto refresh every 20sec</p></body></html>\n";
    httpServer.send(200, "text/html", s);
    Config_Interrupt = false;
}

void handleCallArg()
{ //************************************************************call page*******************************************//
    if (IGATE_ON == true && (WiFi.status() != WL_CONNECTED))
    {
        Config_Interrupt = true;
        //IGATE_ON=false;
        WiFi.disconnect();
        Serial.printf("> IGATE OFF for Web Config \r\n");
    }
    //String s = httpServer.argName(0) + " = " + httpServer.arg(0)  ;
    //http://192.168.1.36/?call=E24SAT-8&msg=test{1&via=0
    char Buff_web_raw = httpServer.arg(0).charAt(0);
    String Buff_web_call = httpServer.arg(1);
    String Buff_web_msg = httpServer.arg(2);
    char Buff_web_via = httpServer.arg(3).charAt(0);
    String Raw_msg = urldecode(Buff_web_msg);
    int lenC = Buff_web_call.length();
    int lenM = Raw_msg.length();
    if (Buff_web_raw == '0')
    {
        if (Buff_web_via == '0')
        {
            BUFF = Raw_msg;
            SendAPRS(String(VIAPATH));
        }

        if (Buff_web_via == '1')
        {
            BUFF = String(MyCallSign) + ">" + FW_destNET + ":" + Raw_msg;
            client.println(BUFF);
        }
        if (Buff_web_via == '2')
        {
            BUFF = String(MyCallSign) + ">" + FW_destNET + ":" + Raw_msg;
            client.println(BUFF);
            BUFF = Raw_msg;
            SendAPRS(String(VIAPATH));
        }
    }
    else
    {
        if (lenC > 3 && lenM > 1)
        {
            /* if(lenC<9)
{
for(int t=lenC;t<9;t++)
{
Buff_web_call = Buff_web_call + ' ';
}
}*/
            Buff_web_call = Buff_web_call + "         ";
            Buff_web_call = Buff_web_call.substring(0, 9);
            char post_c[12];
            Buff_web_call.toCharArray(post_c, 10);
            char post_m[80];
            Raw_msg.toCharArray(post_m, lenM + 1);
            if (Buff_web_via == '0')
            {
                BUFF = ":" + Buff_web_call + ":" + Raw_msg;
                SendAPRS(String(VIAPATH));
            }

            if (Buff_web_via == '1')
            {
                BUFF = String(MyCallSign) + ">" + FW_destNET + "::" + Buff_web_call + ":" + Raw_msg;
                client.println(BUFF);
                //client.printf("%s>APESPG::%s:%s\r\n",MyCallSign,post_c,post_m);
            }
            if (Buff_web_via == '2')
            {
                BUFF = String(MyCallSign) + ">" + FW_destNET + "::" + Buff_web_call + ":" + Raw_msg;
                client.println(BUFF);
                //client.printf("%s>APESPG::%s:%s\r\n",MyCallSign,post_c,post_m);
                BUFF = ":" + Buff_web_call + ":" + Raw_msg;
                SendAPRS(String(VIAPATH));
            }
        }
    }
    Buff_web_call.trim();

    String s_ip = String(WiFi.localIP()[0]) + "." + String(WiFi.localIP()[1]) + "." + String(WiFi.localIP()[2]) + "." + String(WiFi.localIP()[3]);
    String s = "<html>\r\n";
    s += "<head>";
    s += "<meta charset=\"UTF-8\">\r\n<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n";
    s += "</head><body>";
    s += "<p style=\"font-family:Arial; color: #5e9ca0;\"><a href=\"../\">SETUP</a>&nbsp;|&nbsp;<a href=\"http://" + s_ip + "/update\">" + s_ip + "/update</a>&nbsp; |&nbsp;<a href=\"http://192.168.4.1/update\">192.168.4.1/update</a> &nbsp;|&nbsp;";
    s += "<a href=\"call\">Message</a>&nbsp; | &nbsp;<a href=\"status\">Status</a> &nbsp;|</p>";
    s += "<h1 style=\"font-family:Arial; text-align: center;\"><span style=\"font-family:Arial; color: #5e9ca0;\">MSG Page</span></h1><br><center>หน้า MSG<center><br>";
    s += "<p style=\"font-family:Arial; text-align: center; color: #5e9ca0;\">";
    s += "<br>" + incomeMSG[0] + "<br>" + incomeMSG[1] + "<br>" + incomeMSG[2] + "<br>" + incomeMSG[3] + "<br>" + incomeMSG[4] + "<br>";
    s += "<p style=\"font-family:Arial; color: #5e9ca0; align=\"center\"><form action=\"\" method=\"get\">";
    s += "<input type=\"hidden\" name=\"raw\" value=\"" + String(Buff_web_raw) + "\" >";
    s += "<input type=\"hidden\" name=\"call\" value=\"" + Buff_web_call + "\" >";
    s += "<input type=\"hidden\" name=\"msg\" value=\"" + Raw_msg + "\" > ";
    s += "<input type=\"hidden\" name=\"via\" value=\"3\" > ";
    s += "<input type=\"submit\" value=\"Refresh\"></form>";
    s += "&nbsp;<br><form action=\"\" method=\"get\">";
    //**************************Check RAW Packet or MSG*******************************//
    s += "<p style=\"font-family:Arial; text-align: center; color: #5e9ca0;\"><br><input type=\"radio\" name=\"raw\" id=\"Message\" value=\"1\"";

    if (Buff_web_raw == '1' or (Buff_web_raw != '0' and Buff_web_raw != '1'))
    {
        s += "checked=\"checked\"";
    }

    s += "> Send Message<br>";
    s += "<input type=\"radio\" name=\"raw\" id=\"RAW\" value=\"0\"";
    if (Buff_web_raw == '0')
    {
        s += "checked=\"checked\"";
    }
    s += "> Send RAW Packet<p>";
    //**************************Text BOX  *******************************//
    if (Raw_msg != "")
    {
        s += "<input type=\"text\" name=\"call\" value=\"" + Buff_web_call + "\">&nbsp;<input type=\"text\" name=\"msg\" value=\"" + Raw_msg + "\">&nbsp;";
    }
    else
    {
        s += "<input type=\"text\" name=\"call\" value=\"To CALL\">&nbsp;<input type=\"text\" name=\"msg\" value=\"Message\">&nbsp;";
    }
    //**************************Check VIA *******************************//
    s += "<p style=\"font-family:Arial; text-align: center; color: #5e9ca0;\"><br><input type=\"radio\" name=\"via\" id=\"RF\" value=\"0\"";

    if (Buff_web_via == '0' or (Buff_web_via != '0' and Buff_web_via != '1' and Buff_web_via != '2'))
    {
        s += "checked=\"checked\"";
    }

    s += "> Send Via RF<br>";
    s += "<input type=\"radio\" name=\"via\" id=\"INET\" value=\"1\"";
    if (Buff_web_via == '1')
    {
        s += "checked=\"checked\"";
    }
    s += "> Send Via Internet<br>";
    s += "<input type=\"radio\" name=\"via\" id=\"Both\" value=\"2\"";

    if (Buff_web_via == '2')
    {
        s += "checked=\"checked\"";
    }

    s += "> Send Both Rf & Internet<br>";
    s += "<br><input type=\"submit\" value=\"SEND\">";
    s += "</p></form></p>";
    s += "<p style=\"font-family:Arial; color: #5e9ca0; align=\"center\"><form action=\"\"><input type=\"submit\" value=\"BACK\"></form>";
    if (Buff_web_raw == '1')
    {
        s += "SEND MSG To " + Buff_web_call + "<br>MSG > " + Raw_msg;
    }
    else
    {
        s += "SEND RawPacket  " + String(MyCallSign) + ">" + FW_destNET + ":" + Raw_msg;
    }
    s += "</p><br></body></html>\n";
    httpServer.send(200, "text/html", s);
    Config_Interrupt = false;
}

void aprspass()
{
    String MYCALL = String(MyCallSign);

    if (MYCALL.indexOf("-" > 0))
    {
        MYCALL = MYCALL.substring(0, MYCALL.indexOf("-"));
    }
    // initialize hash
    unsigned int hash = 29666; //0x73e2;
    int i = 0;
    int len = MYCALL.length();
    // hash callsign two bytes at a time
    while (i < len)
    {
        hash ^= MYCALL.charAt(i) << 8;
        hash ^= MYCALL.charAt(i + 1);
        i += 2;
    }
    // mask off the high bit so number is always positive
    //Serial.println(hash & 0x7fff,DEC);
    String(hash & 0x7fff).toCharArray(is_passcode, 6);
    //Serial.println(is_passcode);
    //  return hash & 0x7fff;
}
void factory_reset()
{
    Serial.println("> Factory Reset()");
    display.clear();
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.setFont(ArialMT_Plain_16);
    display.drawString(64, 0, "Factory");
    display.drawString(64, 30, "Reset");
    display.display();
    delay(1000);

    CMD("?wa?0.0.0.0", CMD_debug);
    CMD("?wb?255.255.255.0", CMD_debug);
    CMD("?wc?192.168.1.1", CMD_debug);
    CMD("?wd?8.8.8.8", CMD_debug);
    CMD("?we?MyWIFI", CMD_debug);
    CMD("?wf?", CMD_debug);
    CMD("?wg?aprsth.nakhonthai.net", CMD_debug);
    CMD("?wh?14580", CMD_debug);
    CMD("?wi?NOCALL-1", CMD_debug);
    CMD("?wj?000000", CMD_debug);
    CMD("?wk?g/HS*/E2*", CMD_debug);
    CMD("?wl?=1519.24N/10340.41E#", CMD_debug);
    CMD("?wm?Indy Igate", CMD_debug);
    CMD("?wn?15", CMD_debug);
    CMD("?wo?19200", CMD_debug);
    CMD("?wp?WIDE1-1", CMD_debug);
    CMD("?wq?0", CMD_debug);
    CMD("?wr?0", CMD_debug);
    CMD("?ws?0", CMD_debug);
    CMD("?wt?0", CMD_debug);
    CMD("?aa?NOCALL", CMD_debug);
    CMD("?bb?/>Indy Tracker", CMD_debug);
    CMD("?cc?P>Indy Tracker", CMD_debug);
    CMD("?dd?WIDE1-1", CMD_debug);
    CMD("?ee?70", CMD_debug);
    CMD("?ff?45", CMD_debug);
    CMD("?gg?3", CMD_debug);
    CMD("?hh?600", CMD_debug);
    CMD("?ii?OFF", CMD_debug);
    CMD("?jj?19200", CMD_debug);
    CMD("?kk?9600", CMD_debug);

    EEPROM.write(505, 1);
    EEPROM.commit();
    delay(20);
    EEPROM.write(506, 0);
    EEPROM.commit();
    delay(20);
    EEPROM.write(507, 0);
    EEPROM.commit();
    delay(20); //show V in
    EEPROM.write(508, 0);
    EEPROM.commit();
    delay(20);
    EEPROM.write(509, 0);
    EEPROM.commit();
    delay(20);
    EEPROM.write(510, 82);
    EEPROM.commit();
    delay(20);
    EEPROM.write(511, 0);
    EEPROM.commit();
    delay(20);
}
